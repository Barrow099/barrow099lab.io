## 1. előadás

### Klasszikus geometriák

??? question "Sorold fel a tanult klasszikus geometriákat, és hasonlítsd össze őket!"
	- 4 tanult geometria van
		- ??? note "Euklidészi síkgeometria"
			- Egy egyeneshez egy rajta kívül fekvő ponton át **egy** nem metsző egyenes húzható &rarr; párhuzamosság
			- háromszög szögösszege 180 fok
			- pithagorasz tétel &rarr; a^2^ + b^2^ = c^2^
			- kör területe &rarr; πR^2^
		- ??? note "Gömbi geometria"
			- állandó **pozitív** görbületű (*bármely irányba körbenézve olyan, mintha egy dombtetőn állnánk*)
			- két egyenes **mindig** metszi egymást egy pontban &rarr; átmérő
			- egyenes = főkör = legrövidebb út
			- az egyenesek **mindig metszők**
			- háromszög szögösszege **több** mint 180 fok
			- pithagorasz tétel &rarr; a^2^ + b^2^ **>** c^2^
			- kör területe &rarr; **több** mint πR^2^
		- ??? note "Hiperbolikus geometria"
			- állandó **negatív** görbületű (*vannak olyan irányok, ahonnan nézve a domb tetején vagyunk, és van, ahonnan a völgyben*)
			- Egy egyeneshez egy rajta kívül fekvő ponton át **több** nem metsző egyenes húzható &rarr; több párhuzamos van
			- egyenes = legrövidebb út
			- háromszög szögösszege **kevesebb** mint 180 fok
			- pithagorasz tétel &rarr; a^2^ + b^2^ **<** c^2^
			- kör területe &rarr; **kevesebb** mint πR^2^
		- ??? note "Projektív geometria"
			- lényeg: a "végtelen" is része a síknak!
			- Két egyenes pontosan egy pontban metszi egymást &rarr; a végtelenben = ideális pontban
			- nincsenek párhuzamosok (mert mindenegyik egyenes mindegyiket metszi)
			- **programozási előny** &rarr; nincs szingularitás és speciális eset, mivel nincsenek párhuzamosok


??? question "Mi az a Mercantor térkép?"
	- olyan térkép, ami **szögtertó**, de **torzítja a távolságot és a területet**
	- <img src="1_ea.assets/image-20210114135745662.png" alt="image-20210114135745662" style="zoom:40%;" />
	- *A gömböt befoglaljuk egy hengerbe, majd annak egy pontját rávetítjük a henger falára, és a hengert (mivel 0 görbületű), már ki lehet teríteni torzulás nélkül a síkba. Ezen kívül függőlegesen torzítjuk, hogy a szögek megfeleljenek.*
		- torzítás: x = λ és y = log(tan( π/4 + φ/2))

??? question "Szedd össze a tesszellációról tanultakat!"
	- a tesszelláció jelentése: szabályos, egybevágó sokszögekkel való csempézés
	- Euklidészei geometria esetén csak (3, 6), (4, 4) és (6, 3) csempézés létezik
	- Hiperbolikus geometria esetén végtelen sok, mivel a sokszögek belső szöge bármilyen értéket felvehet
		- <img src="1_ea.assets/image-20210114141108715.png" alt="image-20210114141108715" style="zoom:33%;" />

??? question "Hasonlítsd össze a tanult klasszikus geometriákat a metsző egyenesek és a görbületük szempontjából!"
	| Euklideszi                        | Gömbi                | Hiperbolikus                   | Projektív                       |
	| --------------------------------- | -------------------- | ------------------------------ | ------------------------------- |
	| 1 nem metsző egyenes (párhuzamos) | 0 nem metsző egyenes | egynél több nem metsző egyenes | 0 nem metsző egyenes            |
	| zérus görbületű                   | pozitív görbületű    | negatív görbületű              | nem metrikus: végtelen is része |

### Vektoralgebra

??? question "Mi a vektor?"
	- a vektor
		- írány és távolság
		- eltolás
	- két pont viszonyát jellemzi

??? question "Mutasd be a pontok és valós számok közötti tanult kapcsolatokat!"
	- mérések &rarr; pontok és vektorok számszerűsítése (PONTOK &rarr; VALÓS SZÁMOK)
		- távolság rendelése pontpárhoz, hossz rendelése a vektorhoz és szög rendelése két vektorhoz
	- pontok megadása függvénnyel (VALÓS SZÁMOK &rarr; PONTOK)
		- r(t), ahol t például az idő paraméter &rarr; egy görbe
	- pontok leképzése valós számokká &rarr; pl minden térpontban megmérjük a hőt
	- transzformációk &rarr; (PONT &rarr; PONT)

??? question "Sorold fel a tanult vektorműveleteket!"
	- ??? note "összeadás és kivonás"
		- összeadás &rarr; v = v~1~ + v~2~
			- <img src="1_ea.assets/image-20210131191738733.png" alt="image-20210131191738733" style="zoom:25%;" />
		- kivonás &rarr; v = v~1~ - v~2~
			- <img src="1_ea.assets/image-20210131191751345.png" alt="image-20210131191751345" style="zoom:25%;" />
		- kommutatív és asszociatív
	- ??? note "skálázás"
		- skalárral való szorzás &rarr; v~1~ = α\*v
		- <img src="1_ea.assets/image-20210131191838217.png" alt="image-20210131191838217" style="zoom:25%;" />
		- disztributív
	- ??? note "skalár szorzat (**dot**)"
		- v~1~ \* v~2~ = |v~1~|*|v~2~|*cos(α)
		- jelentése: *egyik vektor vetülete a másikra \* másik hossza*
			- <img src="1_ea.assets/image-20210131192321155.png" alt="image-20210131192321155" style="zoom:33%;" />
		- tulajdonságok: **nem asszociatív**, de **kommutatív** és **disztributív az összeadásra**
		- vektor **hosszának** kiszámítása &rarr; saját magával vett skaláris szorzata &rarr; v \* v = |v|^2^
		- egységbektor képzése &rarr; vektor elosztva az abszolútértékével
			- <img src="1_ea.assets/image-20210131192649971.png" alt="image-20210131192649971" style="zoom:25%;" />
			- (*vektor elosztva a saját magával vett skaláris szorzatának négyzetgyökével*)
		- 2 vektor bezárt szögének koszinusza
			- <img src="1_ea.assets/image-20210131192214858.png" alt="image-20210131192214858" style="zoom:25%;" />
		- két vektor **merőleges**, ha **skalár szorzatuk nulla**
	- ??? note "vektor szorzat (**cross**)"
		- |v~1~ × v~2~| = |v~1~|\*|v~2~|\*sin(α)  &rarr; *merőleges a két operandus által meghatározott síkra, majd ebből a jobbkézszabály szerinti*
			- <img src="1_ea.assets/image-20210131193532172.png" alt="image-20210131193532172" style="zoom:33%;" />
		- jelentése:
			- operandusok által kijelölt terület
			- operandusokra merőleges vektor
			- (egyik vektor vetülete a másikra merőleges síkra + 90°-os elforgatás) \* másik hossza
		- tulajdonságok: **nem asszociatív**, **antiszimmetrikus** (a × b = −b × a), **disztributív az összeadásra**

??? question "Mi az iránymenti derivált, és a gradiens"
	- kiindulás: *geometriánk minden pontjához egy skalár értéket rendelünk (pl pontban levő hőmérséklet), azonos hőmérsékletű pontokból szintvonalak lesznek*
	- **iránymenti derivált**: <img src="1_ea.assets/image-20210201110756142.png" alt="image-20210201110756142" style="zoom:25%;" />
		- (*egy v~0~ irányba Δs-el arrébb menve*)
	- **gradiens**: *amerre ez az iránymenti derivált maximális* &rarr; <img src="1_ea.assets/image-20210201110934692.png" alt="image-20210201110934692" style="zoom:25%;" />
		- (*a szintvonalra merőleges irányba lesz a gradiens*)
	- <img src="1_ea.assets/image-20210201110628262.png" alt="image-20210201110628262" style="zoom:33%;" />

??? question "Mutasd be a paraméter szerinti deriválást"
	- <img src="1_ea.assets/image-20210201111259851.png" alt="image-20210201111259851" style="zoom:33%;" />
	- ==ide kellene még valami?==

??? question "Mutasd be a kontravariáns és a kovariáns kordinátarendszerek közti különbségeket!"
	- koordinátarendszer = geometriai referencia rendszer + hozzá tartozó mérési utasítás
	- ??? note "Kontravariáns koordinátarendszer"
		- az *a* és *b* vektort hányszor kell venni, hogy eljussunk az *r* pontba &rarr; **r**(*x*, *y*) = *x***a** + *y***b**
			- **a** = **r**'~x~ és **b** = **r**'~y~
			- **kontravariáns** = ha megdublázzuk a bázisvektorokat (**a**, **b**), és szintén **r**-t akarjuk kifejezni, akkor a koordinátákat feleznünk kell (*x*, *y*)
		- <img src="1_ea.assets/image-20210201112623824.png" alt="image-20210201112623824" style="zoom:40%;" />
	- ??? note "Kovariáns koordinátarendszer"
		- vetítjük a pontot az egyenesekre
		- paraméter vonalak merőlegesek a koordináta tengelyekre (*az adott koordináta tengelyre merőleges egyenesen lesznek az azonos koordinátájú pontok*)
		- X = **a** \* **r** és Y = **b** q* **r**
		- **a** = ∇X, **b** = ∇Y
			- **kovariáns** = egy irányba változik X és Y a bázisvektorokkal
		- <img src="1_ea.assets/image-20210201113135299.png" alt="image-20210201113135299" style="zoom:40%;" />

??? question "Mutasd be a Descartes koordináta rendszert!"
	- alaptulajdonsága: bázisvektorai egység hosszúak és egymásra merőlegesek &rarr; a kontravariáns és kovariáns koordináták megegyeznek
		- **r**(*x*, *y*) = *x***i** + *y***j**
		- *x* = **i** \* **r** és *y* = **j** \* **r**
		- **r**'~x~ = **i** és **r**'~y~ = **j**
		- **i** = ∇*x* és **j** = ∇*y*
	- <img src="1_ea.assets/image-20210201114351932.png" alt="image-20210201114351932" style="zoom:45%;" />
	- ??? note "műveletek Descartes koordinátákkal:"
		- <img src="1_ea.assets/image-20210201114555207.png" alt="image-20210201114555207" style="zoom:30%;" />
	- ??? note "vektoriális szorzat 2-3-4D-ben"
		- <img src="1_ea.assets/image-20210201114739044.png" alt="image-20210201114739044" style="zoom:25%;" />
		- <img src="1_ea.assets/image-20210201114758266.png" alt="image-20210201114758266" style="zoom:25%;" />
		- <img src="1_ea.assets/image-20210201114811275.png" alt="image-20210201114811275" style="zoom:25%;" />

### Differenciálgeometria

??? question "Mutasd be egy síkgörbe érintőjét és normálvektorát!"
	- <img src="1_ea.assets/image-20210202105634629.png" alt="image-20210202105634629" style="zoom:33%;" />
	- görbe = mozgás &rarr; mivel a dinamika nem érdekes, a *t* nem feltétlenül idő
	- **érintő iránya** (**v**) &rarr; sebességvektor, ami a pálya idő szerinti  <u>első deriváltja</u>
	- **normálvektor** &rarr; érintőre merőleges &rarr; (**N**~x~, **N**~y~) = (-**T**~y~, **T**~x~)
	- gyorsulás &rarr; a pálya idő szerinti <u>második deriváltja</u>
		- <u>érintő irányú komponens</u> (tangenciális) &rarr; pályamenti <u>sebességet</u> gyorsítja vagy lassítja
		- <u>normál vektor irnyú komponens</u> &rarr; sebesség <u>irányát</u> módosítja
		- sebesség **állandó** = érintő irányú gyorsulás nincsen (*csak centripetális, ami a pontot a görbülő pályán tartja*)

??? question "Mi a görbület?"
	- görbület jele: κ
	- ??? note "4 definíciót tanultuk a görbülethez:"
		- **egysebességű mozgás centripetális gyorsulása** &rarr; a~cp~ = v^2^/ R
			- *az a gyorsulás, amit el kell szenvednünk, ha végigautózunk ezen a görbén, mégpedig állandó egységsebességgel*
		- **másodrendben simulókör sugarának reciproka** &rarr; 
		- **érintő elfordulása kis lépésnél** &rarr; Δφ ≈ sin(Δφ) = Δs / R
			- <img src="1_ea.assets/image-20210118102441050.png" alt="image-20210118102441050" style="zoom:33%;" />
		- **az az eltávolodás, ami Δs kicsiny távolság megtételekor megfigyelhető**
			- <img src="1_ea.assets/image-20210202184321326.png" alt="image-20210202184321326" style="zoom:25%;" />
		- ezekből adódóan &rarr; <img src="1_ea.assets/image-20210202184417671.png" alt="image-20210202184417671" style="zoom:25%;" />

??? question "Hogy számíthatunk görbületet?"
	- a következő képletet fogjuk használni: <img src="1_ea.assets/image-20210202185828970.png" alt="image-20210202185828970" style="zoom:7%;" />
		- ahol r'^2^ az első fundamentális forma, r'' pedig a második fundamentális forma
	- kezdjük a Δs^2^ (lépéshossz négyzete) kiszámolásával
		- másnével **metrika** &rarr; *ha a koordinátát egy picikét megváltoztatjuk, akkor ténylegesen a térben mekkora távolságot teszünk meg*
		- <img src="1_ea.assets/image-20210202190216107.png" alt="image-20210202190216107" style="zoom:33%;" />
		- az itt megjelenő r'^2^ = **metrikus tenzor** (1. fundamentális forma) = *görbéknél, a paraméter változás négyzete és a megtett út négyzete közötti kapcsolatot a görbe deriváltjának négyzete (=sebesség négyzete) teremti meg*
	- majd számítsuk ki Δh-t &rarr; merőleges eltávolodás az érintőtől
		- <img src="1_ea.assets/image-20210202190642170.png" alt="image-20210202190642170" style="zoom:33%;" />

### Analitikus euklideszi geometria

### Nemeuklideszi geometria

### Automatikus deriválás

## 2. előadás

## 3. előadás

## 4. előadás
















