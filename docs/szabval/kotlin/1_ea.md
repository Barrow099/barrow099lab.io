# Kotlin alapú szoftverfejlesztés - 1. előadás

<a class="md-button" style="text-align: center" href="../1_ea.pdf" download>Letöltés PDF-ként</a>

## Bevezetés

- JDK 8 ajánlott
- Kotlin &rarr; modern nyelvi elemeket ezen keresztül fogjuk megtanulni
- sokféle helyen használható
	- fut JVM felett &rarr; dektop, android
	- webfejlesztésre (js-re is fordul), react alkalmazás is írható
	- ios-en és androidon futó kód is írható

### Jellemzők

- **tömör nyelv**
	- nem kell felesleges dolgokat írni
	- átláthatóbb
- **biztonságos nyelv**
	- null pointer exception-t nehéz benne csinálni
- **interoperable**
	- bármelyik platformon futtatjuk, könnyen működik együtt az ottani natív kóddal
- **Tool-friendly**
	- jó fejlesztőkörnyezetekben lehet használni

### Történelme

- 2010 &rarr; JetLang néven elkezdi fejleszteni az IntelliJ
- 2016 &rarr; első stabil verzió
- azóta évente jön ki új verzió
- Kotlin szigetről van elnevezve (sziget Szentpéterváron)

### Build tool-ok

- pl.: Gradle, Maven
- projektépítő eszköz

### Fordítás

- forráskód &rarr; fordító &rarr; bytecode
- Kotlin Sources (.kt) &rarr; kotlinc &rarr; .class fájlok
- ezeket adjuk a JVM-nek
- <img src="/src/kotlin/image-20200910125030118.png" alt="image-20200910125030118" style="zoom:44%;" />

## Ismerkedés

- buildgradle fájl
	- verzió
	- milyen java-val van használva
	- dependencies rész (függőségek megadása)
		- kotlin standard library
		- junit (teszteléshez)

## Hello Word program

```kotlin
fun main() {
	println("Hello word")
}
```

- <kbd>ctrl</kbd> + <kbd>shift</kbd> + <kbd>a</kbd> (Action kereső) &rarr; skb &rarr; show kotlin byte code
- *decompile* gomb &rarr; java kódot csinál a kotlin-ból
	- fontos: ez a kód nem mindig helyes!
- Java-ból kotlinba
	- Code &rarr; Convert Java from Kotlin
- alkalmazás belépő pontja &rarr; main függvény

### Scratch fájlok &rarr; scriptelésre

- Action keresőből &rarr; Scratch
	- kis külön álló fájlok, modult kiválasztva tudjuk használni függvényeket, példányosítani osztályokat

### REPL

- parancssor jellegű, kotlin kód írható, soronként fordítódik

## Kotlin szintaktika

### Változók - var vs val

```kotlin
var x: Int = 0
//var név: Típus = érték
//nincs pontosvessző
```

- `val` &rarr; mintha `final` lenne, nem változtatható az értéke
- `var` &rarr; változtatható az értéke
- FONTOS: ami csak lehet, az `val` legyen!
- nem kell pontos vessző

```kotlin
val z = 2
//fix értékű változó (mivel val), kitalálja magától, hogy ez Int
```

- <kbd>ctrl</kbd> + <kbd>q</kbd> &rarr; dokumentáció
- <kbd>ctrl</kbd> + <kbd>p</kbd> &rarr; type info

```kotlin
//java verzió:
//final FileInputStream fis = new FileInputStream("")
val fis = FileInputStream("")
//nem kell kiírni a new-t
```

- Változó típusának megadása postfix-ekkel

```kotlin
val myLong = 1L
val myFloat = 1f
val myDouble = 1.0
```

### Mi is az az Int

- választ a fordító, hogy `int` (primitív) vagy `Integer` (osztály) kell e

### String-ek

```kotlin
val name = "Sarah"
val x = 3, y = 5, z = 7
val name = "$x + $y = ${x + y}" //változók értékének belefűzésbe string-be
```

- e mögött java-s `string` összefűzés van
- <kbd>Alt</kbd> + <kbd>Enter</kbd> &rarr; minden fontos funkció

### Függvények

```kotlin
fun addSima(x: Int, y: Int): Int {
    return x + y
}
fun addRovid(x: Int, y: Int) = x + y
```

- `void` visszatérési érték

```kotlin
fun noReturnValue(): Unit { //nem kötelező kiírni a Unit-ot
	println("Semmi vagyok")
}
//tudunk ilyet is csinálni:
val result: Unit = noReturnValue()
println(result)
```

- ennek előnye, hogy nincs megkülönböztetés a `void`-os függvények közt
	- így együtt kezelhetően


```kotlin
//több féle paraméterlistával is szeretném
fun register(
    username: String,
    password: String = "123456",
    email: String = ""
) {
    println("Registered $username, $password, $email")
}
register("alma", "alma123", "alma@alma.hu")
register("korte", "korte123") //2 paraméterrel is meghívható
register("narancs")
register("tigtis", email = "tigris@tigris.hu")
```

## Irányítási struktúrák

- Sima `if`

```kotlin
val age = 15
val drinkingAge = 18

if(age < drinkingAge) {
    println("Nope")
} else {
    println("Have a beer")
}
```

- értékadás `if`-fel (?: operátor helyett)

```kotlin
kedvezmeny = if (age < drinkingAge) {
    val diff = drinkingAge - age
    100 - diff * 5
} else {
    0
}

val max = if (a > b) a else b // cserébe ?: operátor nincs!
```

- `switch` alternatíva &rarr; when

```kotlin
val greade: Int = 2
val description: String = when (greade) {
1 -> { "Elégtelen" }
2 -> { "Adequate" }
3 -> { "Adequate" }
4 -> { "Adequate" }
5 -> { "Adequate" }
else -> "Érvénytelen"
}
```


```kotlin
when (val rating = calculateRating()) {
0 -> println("Katasztóvális") //ha 1 dolgot akarunk, nem kell {}
1, 2, 3 -> println("Rossz") //tudunk több értéket megadni
in 4..6 -> println("Átlagos") //teljes tartomány (4, 5, 6)
in 7 until 10 -> println("Jó") //jobbról nyitott tartomány (7, 8, 9)
10 -> println("Kiváló")
}
```

```kotlin
val x = 10
val y = 2
when { //if-else-if-else ág, az első igaz lesz kiértékelve!
x <40 -> println("x is too small")
y in 0..50 -> println("í is in invalid range")
}
```

## Kivételek

- mint java-ban &rarr; `try - catch - (finally)` blokk
- értékadás is lehet vele

```kotlin
val input: String = readUserInput()
val value: Double = try {
input.toDouble()
} catch (e: NumberFormatException) {
0.0
}
```

- semmit nem kötelező elkapni kotlin-ban

