# Kotlin alapú szoftverfejlesztés - 2. előadás

<a class="md-button" style="text-align: center" href="../2_ea.pdf" download>Letöltés PDF-ként</a>

## Loop-ok

* `while`

	```kotlin
	while(a > b) {
		...
	}
	```

* `do-while`

	```kotlin
	do {
	
	} while(a > b)
	```

* `for` loop &rarr; nem a pontosvesszős C-s verzió van

	* listán végigmenetel

		```kotlin
		val myNumbers = listOf(1, 2, 5, 14, 42)
		for(number in myNumbers) {
			... //lefut minden értékre a listából
		}
		```

	* adott számig menetel &rarr; `range`-el

		```kotlin
		//NÖVEKVŐ range-ek
		for(i in 0..10) { //zárt, azaz 10 IS benne van
			println(i)
		}
		for(i in 0 until 10) { //nyílt, azaz 10 már NINCS benne
		    println(i)
		}
		//csökkenő range
		for(i in 10 downTo 0) {
		    println(i)
		}
		```

	* többesével lépkedés

		```kotlin
		for(i in 0..100 step 5) {
			println(i)
		}
		```

## Osztályok

* osztályok létrehozása

	```kotlin
	class Person //törzs nélkül is hagyható
	```

* osztály konstruktor paraméterekkel

	```kotlin
	class Person(name: String, age: Int) //zárójelben a konstruktor paramétere vannak
	fun main() {
	    val person = Person("Steve", 33)
	    println(person.name)
	    println(person.age)
	}
	```

* osztály property-kkel (field-nél több)

	```kotlin
	class Person(name: String, age: Int) {
		val name: String = name//azaz a neve nem változhat majd
		var age: Int = age
	}
	```

	* kötelező alapértelmezett értékeket adni a property-knek

* rövidebb verziók

	```kotlin
	class Person(val name: String, var age: Int)
	fun main() {
	    val person = Person("Steve", 33)
	    println(person.name)
	    person.age = 34
	}
	```

	* property mögött keletkezik egy **privát field**,
	* hozzá egy **getter** *val* és *var* esetén, és egy **setter** is *var* esetén

* saját getter/setter &rarr; ki kell venni a fejlécből a property-t

	```kotlin
	class Person(val name: String, age: Int) {
		var age: Int = age
			get() {
				return field //ez a field kulcsszó a property mögötti field-et hivatkozza meg
			}
	    	set(value) {
	            field = value
	        }
	}
	```

	* pl csak nagyobb érték elfogadása

		```kotlin
		set(value) {
			if(value > field) {
				field = value
			}
		}
		```

* lazy delegate &rarr; lusta viselkedés

  * ha null akkor számoljuk csak ki, különben a régebben kiszámolt verziót adjuk vissza

  * így csak először tart sokáig a feldolgozás

  * megoldás &rarr; property delegation

  * 
  	
  	```kotlin
  	var pi: Double by lazy { //alapból szálbiztos
  		var sum = (1..50_000).sumByDouble { 1.0 / it / it}
  	    sqrt(sum * 6.0)
  	}
  	//szálbiztos mentes mód
  	var pi: Double by lazy(mode = LazyThreadSafelyMode.NONE) { ... }
  	```

* Observable delegate &rarr; ha változik az adott érték, lefut ez a delegate

	```kotlin
	var name: String by Delegates.observable("Megan") { property, oldValue, newValue ->
		println("Name changed from $oldValue to $newValue")	
	}
	```

* Vetoable delegate &rarr; ha változk az adott érték, akkor fut le, új érték véglegesítését is meg tudjuk akadályozni

	```kotlin
	//új érték véglegesítését meg tudjuk akadályozni
	var age: Int by Delegates.vetoable(20) { property, oldValue, newValue ->
		newValue > oldValue
	}
	```

	

## Konstruktorok

* minden property-t inicializálni kell, vagy absztrakttá tenni!

* primary constructor &rarr; a zárójeles rész, és utána levő property-k

	```kotlin
	class Car(model: String, age: Int) {
		val model: String = model
		var age: Int
		var miles: Double
		
		init { //inicializálás init blokkal is lehetséges
			this.age = age
			this.miles = miles
		}
	}
	```

* értékadás fentről lefele sorrendben történik

	```kotlin
	class Car(model: String, age: Int) {
		val model: String = model
		var age: Int = age
		var miles: Double = miles
		val description: String = "$model ($age age, $miles miles)"
	    //felette levőknem már van értéke, ezért használhatjuk a többi property-t az értékadásánál
	}
	```

* másodlagos konstruktorok létrehozása

	```kotlin
	class Car(model: String, year: Int) {
		val model: String = model
		var year: Int = year
		var miles: Double = miles
	    
	    constructor(
	    	model: String
	        year: String
	        mileage: String
	    ): this(model, year.toInt()) { //secondary constructor első sora
	        this.miles = mileage.toDouble()
	    }
	    
	    constructor(data: Array<String>) : this(
	    	model = data[1]
	    	year = data[3]
	    	mileage = data[7]
	    )
	}
	```

	* a secondary constructor-nak át <u>kell</u> hívnia a primary constructor-ra

* primary constructor nélküli megoldás

	```kotlin
	class Car {
	    val model: String
	    val year: Int
	    var miles: Double = 0.0
	    val age: Int
	    constructor(model: String, year: Int) {
	        this.model = model
	        this.year = year
	        age = getCurrentYear() - year
	    }
	    constructor(model: String, year: String, mileage: String) {
	        this.model = model
	        this.year = year.toInt()
	        this.miles = mileage.toDouble()
	        age = getCurrentYear() - this.year
	    }
	}
	```

	* ilyenkor nem tudunk property-kből property-t létrehozi &rarr; max duplikálással tudjuk megoldani

## Data class

* data class létrehozása

	```kotlin
	data class Person(val name: String, var age: Int)
	```

	* automatikusan legenerálja a Person osztályhoz a következőket:

		* **toString, hashCode, equals**
		* újrafordításkor újragenerálódik

	* destructuring declarations

		```kotlin
		data class Person(val name: String, var age: Int)
		fun main() {
			val emma = Person("Emma", 19)
			val (name, age) = emma //destructuring declerations
			//val name = emma.name
			//val age = emma.age
		    //fontos, hogy nem neveket, hanem SORRENDET figyel
		    val (age, name) = emma //itt pont fordítva lenne!!!
		}
		```

	* copy függvény

		```kotlin
		data class Person(val name: String, var age: Int)
		fun main() {
			val emma = Person("Emma", 19)
			val olderEmma = emma.copy(age = 21) //copy függvény
		}
		```

		* elnevetett paraméterrel megadható az a néhány paraméter, amin változtatni akarunk
		* a többi lemásolásra kerül
		* <u>***val***-lal is működik</u>, azzal érdemes használni!

## Objektum orientált megközelítés támogatása

* **Singleton osztály** = **object**

	```kotlin
	object Logger { //mivel objektum, nem lehet belőle példányt létrehozni
	    var isEnable = true
	    
		fun log(message: String) {
	        if(isEnable) {
	            println(message)
	        }
	    }
	}
	fun main() {
	    val logger: Logger = Logger //hozzárendelhetjük egy val-hoz
	    Logger.log("hello") //így is elérhető
	    Logger.isEnable = false
	    Logger.log("world") //ez már nem kerül kiírásra
	}
	```

* Statikus dolgok pótlása

	```kotlin
	class Document {
		object Counter { //egy lehet belőle => olyan mint valami static attribútum
	        var count = 0
	    }
	    companion object { //ha companion, mivel 1 lehet belőle, nem kell neki nevet adni
	        var count2 = 0
	    }
		
	    val id = Counter.count++
	    val id2 = count2++ //már így is elérhetjük
	}
	fun main() {
	    repeat(5) {
	        Document()
	    }
	    println(Document.Counter.count) //így lehet elérni, mert a Document-en belül van a Counter
	    println(Document.count2) //ha companion is előtte van, hivatkozhatunk rá így is
	}
	```
	
* egy osztályon belül <u>több **object**</u> is lehet
	* egy osztályon belül egyetlen <u>egy object</u> lehet **companion**
		* ennek <u>nem kötelező nevet adni</u> &rarr; statikus blokként fog viselkedni
		* ilyenkor Companion nevet kap alapértelmezett módon (*nem fontos*)
	
* Egymásba ágyazott osztályok

	```kotlin
	class Outer {
		var outerValue = 0
	    class Inner { //sima class
	        init {
	            println(outerValue) //ez NEM érhető el
	        }
	    }
	    inner class Inner2 { //az inner kulcsszóval thetjük ezt elérhetővé
	        init {
	            println(outerValue) //így már ELÉRHETŐ
	        }
	    }
	}
	```

	* **belső osztályból** kotlinban <u>nem érhető el</u> **külső osztály**!
