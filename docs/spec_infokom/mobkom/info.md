# Mobil kommunikációs hálózatok (VIHIAC00)

**Honlap:** http://www.mcl.hu/mobil_bsc/

## Adatok

- Kredit: 4
- Tanszék: HIT
- ZH: 1db
- Vizsga: van

## Tárgy teljesítése

- Vizsgás tárgy, vizsgára csak aláírással rendelkezők jelentkezhetnek!
- 1 db. ZH, minimum 40%

## Előadás

| Hét  | Dátum      | Előadás                                                      | Jelenlét | Tudom |
| ---- | ---------- | ------------------------------------------------------------ | -------- | ----- |
| 1    | 2020.09.10 | Bevezetés, mobil hálózati alapismeretek |          |       |
| 2    | 2020.09.17 | Mobilitásmenedzsment és csatorna alapismeretek |          |       |
| 3    | 2020.09.24 | 3G rendszerek |          |       |
| 4    | 2020.10.01 | 3G rendszerek 2.       |          |       |
| 5    | 2020.10.08 | LTE rendszerek |          |       |
| 6    | 2020.10.15 | LTE rendszerek működése |          |       |
| 7    | 2020.10.22 | Hálózat felügyelet, hálózat menedzsment csomagkapcsolt mobil hálózatban |          |       |
| 8    | 2020.10.29 |  802.11 WiFi hálózatok |          |       |
| 9    | 2020.11.05 |  802.11 hálózati megoldások|          |       |
| 10   | 2020.11.12 | Bluetooth és Bluetooth Low Energy hálózatok |          |       |
| 11   | 2020.11.19 | Ad-hoc hálózatok, csoportosítás, használati lehetőségek |          |       |
| 12   | 2020.11.26 | Speciális célú hálózati megoldások különleges környezetben |          |       |
| 13   | 2020.12.03 | Speciális célú hálózati megoldások különleges környezetben |          |       |
| 14   | 2020.12.10 |  A mobil/vezetéknélküli hálózatok jövője    |          |       |

## Gyakorlatok

2020\. őszi félév

- 7 gyakorlat tartozik a tárgyhoz
- második oktatási héten kezdődnek
- **kéthetente** lesznek
- Gyakorlatok sorrendje:
  1. Mobilitás-menedzsment és rádiós erőforrás-kezelési folyamatok leírása valós hálózati példákkal: paging, location update és handover.
  2. Konkrét megvalósítási módok, példa hálózati felépítés a 3G megvalósításához (a szabványban adott logikai architektúrán túl); a hálózati architektúra változása a 3G evolúcióban, néhány gyártói megoldás ismertetése. Tipikus folyamatok a 3G rendszerben.
  3. Működés, tipikus folyamatok az LTE rendszerben: handover folyamat, mobilitás menedzsment eljárások működése, session menedzsment. Példa számítások LTE teljesítőképességre.
  4. Mért minőség jellemzők a mobil hálózatokban valós példákon. Teljesítményők számolása és származtatása a mért jellemzőkből.
  5. NFC és RFID rövid áttekintés, alkalmazási és működési példák.
  6. Bluetooth LE fejlesztési példa.
  7. Edge computing és NFV allokációs mintapéldák, nyereség számítása. 


## Zárthelyi

1 darab ZH és 1 darab pótlási lehetőség a félév során

- zh: 2020.11.03. kedd 8.15
- pzh: 2020.11.20. péntek 14.15

## Vizsga

## Távoktatás

Ki tudja mi lesz

## A tantárgy célkitűzése

A tárgy célja, hogy gyakorlati megközelítésben a hallgatók számára bemutassa a napjainkban legelterjedtedtebb mobil- és vezetéknélküli  hálózatok és rendszerek működését, valamint az üzemeltetésükhöz, tervezésükhöz szükséges alapvető ismereteket. A tárgy célja továbbá a konkrét mobil hálózati példákon keresztül alapvető rádiós és vezetéknélküli kommmunikációs megoldások és ezek lehetőségeinek, használati módjainak bemutatása. 