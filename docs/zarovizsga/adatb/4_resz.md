!!! note ""
    - Relációs adatmodell
    - Az adatok strukturálása
    - Relációalgebra, illesztések


### Adatok strukturálása
- **adatmodell**: formalizált jelölésrendszer adatok és adatkapcsolatok leírására + műveletek az adatokon
- **reláció**: halmazok Descartes-szorzatának részhalmaza
    - Descartes-szorzat = minden mindennek
- **relációs széma**: A relációhoz tartozó attribútumokat tárolja pl: R(A1, A2, ..., An)
    - *reláció foka*: oszlopok száma
    - *reláció számossága*: sorok száma

### Műveletek relációkon
- **unió, egyesítés** -> attribútumok száma meg kell hogy egyezzen!
- **különbségképzés** -> attribútumok száma meg kell hogy egyezzen!
- **Descartes-szorzat** -> oszlopok száma: r+s, sorok száma: r*s
- **kiválasztás, szelekció** ($\sigma$) -> részhalmaz képzése a feltétel kiértékelése alapján (feltétel kvantormentes)
- **vetítés, projekció** ($\pi$) -> egyes attribútumok megtartása, többi törlése
- **természetes illesztés** -> ($r \Join s$) = ($\pi_{R \cup S} \sigma_{(R.x_1 = S.x_1) \wedge (R.x_n = S.x_n)} (r \times s)$)
- **theta-illesztés** -> theta kvantormentes ($r \underset{\Theta}{\Join} s$) = ($\sigma_{\Theta}(r \times s)$)
- **hányados** ($r \div s$ ) = (s-sel alkotott Descartes-szorzata a lehető legbővebb részhalmaza r-nek)
