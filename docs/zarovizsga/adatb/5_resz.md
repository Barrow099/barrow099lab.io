!!! note ""
    - Relációs sématervezés ER diagramból

- leképzés:
    - egyedhalmazokból -> olyan relációs sémák lesznek, amik tartalmazzá az egyedhalmaz összes attribútumát
    - kapcsolattípusokból -> olyan relációs sémák lesznek, amik attribútumai közt az összes kapcsolatban résztvevő egyedhalmaz összes kulcsa szerepel attribútumként
- **univerzális séma**: egy tábla írja le az egész rendszert
    - felhasználónak könnyű
    - tárolásnak NEM jó, nehézkes és ellentmondást okozhat
- **redundáns reláció**: ha a relációban valamely attribútum értékét a relációban található másik attributom(ok) értékéből ki tudjuk következtetni valamely ismert következtetési szabály segítségével
