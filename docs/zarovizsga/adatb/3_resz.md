!!! note ""
    - ER modell és diagram
        - Egyedhalmazok
        - Tulajdonsághalmazok
        - Specializáció/általánosítás
        - Gyenge egyedhalmazok

### ER modell
- **ER modell** (egyed-kapcsolat modell): egy majdnem-adatmodell
    - elemei: *egyedtípusok*, *attribútumtípusok*, *kapcsolattípusok*
- **egyed**, **entitás**: valós világban létező, saját léttel rendelkező dolog, amiről adatot tárolunk (*megkülönböztethetőnek kell lenniük*)
- **tulajdonság**: entitásokat jellemzi, ezeken keresztül különböztethetők meg az entitások
- **egyedhalmaz**: azonos attribútumtípusokkal jellemzett egyedek összessége
- **kapcsolat**: entitások névvel ellátott viszonya
    - **kardinalitás**:
        - *egy-egy kapcsolat*
        - *több-egy kapcsolat*
        - *több-több kapcsolat*
- **kulcs**: attribútumoknak az a halmaza, amely az entitás példányait egyértelműen azonosítja (*attribútumok teljes halmaza mindig kulcs*)

### ER és EER diagram
- **ER diagram**: az ER modell grafikus ábrázolása
- jelölések:
    - egyedhalmaz - téglapal
    - attribútum - karika
    - kapcsolattípus - trapéz
- **Extended ER (EER)**: bővített ER diagram
    - **Specializáció** / **általánosítás**: egy entitáshalmaz minden eleme rendelkezik egy másik (*általánosabb*) entitáshalmaz attribútumaival, de azokon kívül még továbbiakkal is (*specializáció*)
    - jelölése: háromszög ISA felirattal
    - **Gyenge egyedhalmaz**: olyan entitáshalmaz, aminek nem tudunk kulcsot meghatározni, csak egy vagy több kapcsolódó egyed segítségével
        - egyediséget a *tulajdonos egyedhalmaz* biztosítja, ez a gyenge egyedhalmazzal több-egy kapcsolatban áll (**determináló kapcsolat**)
        - példa: a KURZUS egy gyenge egyedhalmaz (több évben is indul), egyediséget csak az INDUL determináló kapcsolata biztosítja a TÁRGY entitással
        - jelölés: dupla keretes rombuszba írt kapcsolat
