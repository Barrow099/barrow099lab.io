# Objektumorientált szoftvertervezés - 4. előadás</br>Clean code

## Mi a clean code?

* rossz kód: nehéz olvasni, megérteni, karbantartani
	* hátránya: csökkenti a produktivitást
* kódot többet olvassuk mint írjuk
* tulajdonságok: könnyű olvasni, megérteni, elegáns, hatékony, minden hibát kezel, könnyen lehet dolgozni vele

## Szabályok

### Osztályok elnevezései

1. az osztálynevek főnév jellegűek legyenek (ige max Visitor vagy Strategy minta esetén legyen)
2. a függvénynevek ige jellegűek legyenek
3. használjuk az alkalmazási terület elnevezéseit
4. használjunk beszédes neveket
5. változó nevébe kódoljuk a jelentését és mértékegységét is (*elapsedTimeInDay*)
6. kerüljük a félreinformálás
7. használjunk megkülönböztető neveket
8. használjunk kimondható neveket
9. használjunk kereshető neveket
10. kerüljük a névkódolást
11. használjuk a hatókörnek megfelelő hosszúságú nevet
12. adott dologra mindig ugyanazt a nevet használjuk
13. biztosítsunk értelmes kontextust (hogy egyértelmű legyen a név jelentése)
14. hagyjuk el az értelmetlen kontextust (pl prefixeket hagyjuk)

### Függvényekre vonatkozó szabályok

1. a függvények legyenek kicsik (2-4sor)
2. a blokkok belseje egyetlen sor legyen (különben külön fv legyen, blokkok ne legyenek egymásba ágyazva)
3. egy függvény egyszerre csak egy dolgot csináljon
4. egy függvény egyszerre csak egy absztrakciós szinten dolgozzon
5. a függvények lépcsőzetesen kövessék egymást (az absztrakciós szinteket folyamatosan kibontva)
6. egy függvénynek minél kevesebb paramétere legyen (0-1-2)
7. kerüljük a mellékhatásokat (erre nem számít a hívó!)
8. válasszuk szét a parancs és a lekérdezés jellegű függvényeket

### Kommentek írása és elhagyása

#### Nem ajánlott kommentek:

1. motyogás (tartalmatlan információ)
2. redundáns komment (olyan komment, ami a kódból is kiolvasható)
3. kötelező kommentek (ha kötelező mindent kommentelni :( szóval ezt ne tegyük kötelezővé!)
4. idegességből hagyott komment
5. komment függvény vagy változó helyett (inkább kifejező kódot írjunk!)
6. feltűnő banner jellegű kommentek (/---------------------/ na ilyet ne)
7. blokkzáró kommentek (itt a while vége)
8. kikommentezett kód (ilyet se hagyjunk, csak gyűlni fognak! használjunk verziókövetést)
9. napló jellegű komment (helyette verziókövető rendszert használjunk)
10. szerzőt hitelesítő komment (szintén verziókövetővel pótoljuk)
11. HTML kommentek (dokumentációgenerátor csinálja inkább)
12. nemlokális információ (inkább hivatkozzuk az adott helyet)
13. túl sok információ (ne teljes szabványt másoljunk be!)
14. hiányzó kapcsolat (legyen nyilvánvaló a kapcsolat)

#### Ajánlott kommentek:

1. informatív kommentek
2. szándék illetve tervezői döntés magyarázata (ez fontos!)
3. bonyolultabb szintaxissal rendelkező kódot magyaráznak (ne felejtsük őket szinkronizálni)
4. figyelmeztetés a következményekre (pl valami nem szálbiztos)
5. TODO kommentek
6. hangsúlyozó kommentek (mit ne töröljünk ki)
7. publikus API dokumentálása (sokmindent szépen írjunk bele!)

### Kivételek definiálása és kezelése

1. hibakódok helyett használjunk kivételeket (alkalmazáslogika külön függvénybe)
2. biztosítsunk kontextust a kivételhez (hol keletkezett, miért, hogy elkerülhető)
3. használjunk unchecked (runtime) kivételeket (ne hárítsuk a kliensre a lekezelést)
4. a kivételeket mindig a hívó szemszögéből definiáljuk (ne kényszerítsük, hogy a kliens megértse mi van a szervernél)
5. ne térjünk vissza null értékkel (dobjunk kivételt, vagy NullObject tervezési mintát használjunk)
6. ne adjunk át paraméterként null értéket

## Objektum orientált fejlesztés vs procedurális fejlesztés

| Procedurális kód                                             | Objektumorientált kód                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| fókuszba: **adat**                                           | fókuszban: **viselkedés**                                    |
| viselkedés külön függvényekben                               | adatreprezentáció elrejtve                                   |
| könnyű új függvényt adni az adatstruktúra változtatása nélkül | könnyű megváltoztatni a belső adatreprezentációt a külső interfész megváltoztatása nélkül |
| nehéz megváltoztatni az adatstruktúrát, mert az minden függvényre hatással lehet | nehéz megváltoztatni a külső interfészt, mert az minden hívóra és leszármazottra hatással lehet |
| stabil viselkedés: **ORM**, **DTO**                          | stabil viselkedés: **OCP**                                   |

