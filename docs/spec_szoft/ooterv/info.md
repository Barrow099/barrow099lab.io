# Objektumorientált szoftvertervezés (VIIIAC00)

**Honlap:** https://www.iit.bme.hu/targyak/BMEVIIIAC00

!!! warning "Távoktatás"
	Távoktatás során a gyakorlat helyett kvíz-ek lesznek, amit a moodle felületen kell kitölteni! Minden héten az adott heti kvízt minimum 70%-ra kell kitölteni, de akárhányszor lehet próbálkozni, a legjobb próbálkozás lesz figyelembe véve.

## Adatok

* Kredit: 4
* Tanszék: IIT
* kisZH: 5db
* ZH: nincs
* Vizsga: van

## Tárgy teljesítése

* Az 5 kisZh-ból a 3 legjobbnak legalább elégségesnek kell lennie
* Vizsgás tárgy, vizsgára csak aláírással rendelkezők jelentkezhetnek!
* Félév végi jegy = Vizsga pont
      * 0 - 0 pont: **1**-es
      * 0 - 0 pont: **2**-es
      * 0 - 0 pont: **3**-as
      * 0 - 0 pont: **4**-es
      * 0 - 0 pont: **5**-ös

## Előadás

| Hét  | Dátum      | Előadás                                    | Jelenlét | Tudom |
| ---- | ---------- | ------------------------------------------ | -------- | ----- |
| 1    | 2020.09.09 | OO tervezési elvek                         |          |       |
| 2    | 2020.09.16 | OO tervezési heurisztikák                  |          |       |
| 3    | 2020.09.23 | OO tervezési heurisztikák                  |          |       |
| 4    | 2020.09.30 | OO tervezési heurisztikák                  |          |       |
| 5    | 2020.10.07 | OO tervezési heurisztikák                  |          |       |
| 6    | 2020.10.14 | Refaktorálás                               |          |       |
| 7    | 2020.10.21 | Clean-code elvek                           |          |       |
| 8    | 2020.10.28 | Clean-code elvek                           |          |       |
| 9    | 2020.11.04 | API tervezési elvek                        |          |       |
| 10   | 2020.11.11 | Elosztott objektumorientáltság, SOAP, REST |          |       |
| 11   | 2020.11.18 | Konkurens és párhuzamos minták             |          |       |
| 12   | 2020.11.25 | Konkurens és párhuzamos minták             |          |       |
| 13   | 2020.12.02 | Konkurens és párhuzamos minták             |          |       |
| 14   | 2020.12.09 | Immutable objektumok                       |          |       |

## Gyakorlat

1. OO tervezési elvek, OO tervezési heurisztikák
2. OO tervezési heurisztikák
3.  Refaktorálás
4. Példák hibásan megtervezett API-kra
   Csomagkezelő és fordító eszközök (ANT, Maven, Gradle, ...)
5. Verziókezelés (Git)
6. Példák konkurencia problémákra

## Kis ZH

* KisZh-k nem pótolgatók
* a 3 legjobbnak legalább elégségesnek kell lennie
	1. *2020.00.00*	**OO tervezési elvek és heurisztikák**
	2. *2020.00.00*	**OO tervezési heurisztikák**
	3. *2020.00.00*	**Refaktorálás, clean-code**
	4. *2020.00.00*	**Clean-code, API tervezési elvek, Csomagkezelő és fordító eszközök**
	5. *2020.00.00*	**Elosztott objektumorientáltság, konkurens és párhuzamos minták**

## Vizsga

!!! warning
	még nincs róla információ