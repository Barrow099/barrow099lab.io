# Objektumorientált szoftvertervezés - 5. előadás</br>Elosztott OO

* hálózati kommunikáció esetén felmerülő problémák és megoldások

## Helyi hívás vs távoli hívás

### Helyi hívás

* kliens és szerver azonos memória-térben vannak
* kliens objektumnak közvetlen mutatója van a szerver interfészére
* hívás közvetlenül történik
* <img src="/src/ooterv/image-20201117102648069.png" alt="image-20201117102648069" style="zoom:33%;" />

### Távoli hívás

* kliens és szerver objektum külön memóriatérben vannak
* két objektumnak hálózati kapcsolaton keresztül kommunikál
* cél: kliens olyan interfészt lásson, mintha helyi hívást intézne
	* ezt egy proxy objektum biztosítja &rarr; kliens hívásából hálózati üzenet = sorosítás
	* ezt egy adapter fogadja &rarr; visszasorosítja, majd továbbítja a szerver interfészen keresztül a szervernek
* <img src="/src/ooterv/image-20201117103116848.png" alt="image-20201117103116848" style="zoom:30%;" />

## Kérdések:

* hogyan definiáljuk a szerver interfészt?
	* ugyanazon a programnyelven? &rarr; programnyelv interfészét használhatjuk
	* különböző programnyelven? &rarr; programnyelvektől független interfész leírókkal
* hogyan találjuk meg a szervert?
	* kliens tudja hol van &rarr; kliensnek config fájljában van
	* logikai név alapján &rarr; ezt a fordítást egy névszerver végzi
	* interfész alapján &rarr; treading service interfész specifikációból fizikai címet készít (pl időjáros cucc interfész kell)
* hogyan implementáljuk a proxy-t?
	* feladata: szerver megkeresése, kapcsolat felépítése, paraméterek és eredmények sorosítása
	* megvalósítás: proxy + adapter tervezési minta
* hogyan implementáljuk az adaptert?
	* feladata: kliens kapcsolat fogadása, szerver objektum példányosítása, kliens kérdések továbbítása függvényhívásként a szerver objektumhoz, paraméterek és eredmények sorosítása, több kérdés kezelése párhuzamosan
	* megvalósítás: adapter tervezési minta
* hogyan sorosítsuk az adatokat?
	* memóriában lévő adat bájtfolyammá és vissza
	* teljes hierarchiát kell leírni majd azt rekonstruálni a másik oldalon
	* 2 fajta:
		1. **bináris sorosítás** &rarr; gyors, hatékony, kevés memóriát használ, DE bináris kompatibilitást feltételez a kliens és szerver közt, pl ha ugyanazon programnyelven készültek
		2. szöveges sorosítás &rarr; emberileg is olvasható (XML, JSON), lassabb, kevésbé hatékony, több memóriát eszik, DE programnyelvek közt kompatibilis, és időtálló
* hogyan kezeljük a memóriát?
	* ki foglalja le és ki szabadítja fel a bemenő és kimenő paramétereket
	* modern nyelvekben: GC
	* régebbi nyelvekben: objektum tulajdonosának egyértelműsítésével
		* pl eredményt: proxy foglalja, kliens szabadítja
* hogyan szolgálunk ki több klienst?
	* egy szálú szerver &rarr;  nincs konkurencia probléma
		* blokkoló &rarr; újabb kéréseknek várakozni kell, míg a jelenlegi kiszolgálásra nem kerül (ilyet nem szoktunk csinálni &rarr; nem hatékony)
		* nem blokkoló &rarr; pl nodejs, hatékonyabb de komplex callback programozáson alapuló modellt igényel
	* több szálú szerver &rarr; védeni kell a több szál által használt adatokat
		* kliensként külön szál &rarr; könnyen túlterheli a szervert 
		* thread pool &rarr; amíg van szabad szál, addig tud kiszolgálni, jól kihasználja a szerver erőforrásait
* hány objektumpéldány kell a szerverből?
	* egy szerver példány &rarr; singleton, jó ha nincs kliens függő állapot
	* kliensenként egy &rarr; kliens specifikus állapotot is tud tárolni, DE nem skálázható (újrainduláskor elveszhet), lezáró szükséges
	* object pool &rarr; kiszolgáló szerver objktumokhoz rendeljük a klienseket, így kérések szeparálva futnak, de nincs kliens specifikus állapot. Ez jól skálázható
* hogyan őrizzük meg az állapotot hívások között?
	* szerver memóriájában &rarr; minden kliens saját szerver objektum példánnyal rendelkezzen
	* minden hívásban átküldjük &rarr; működik, ha nem túl nagy ez az állapot (pl böngészú cookie-k)
	* adatbázisban &rarr; jól skálázódik, de a kliensnek azonosítani kell tudnia magát (kell seccion id)
* hogyan kommunikáljunk, ha valamelyik oldal nem elérhető?
	* szinkron hívásnál (kliensnek azonnal kell válasz) &rarr; nem tudunk kommunikálni
	* aszinkron hívás (ráér a válasz)
		* ha nincs megbízható köztes szereplő
		* ha van megbízható köztes szereplő &rarr; megoldható a szétcsatolt kommunikáció (köztes szereplő vigyáz addig az adatra). Nehézség a visszaérkező válaszok párosítása a hozzájuk tartozó kérdésekkel
* hogyan kezeljük a szinkron hívásokat?
	* általában folyamatosan fennálló kapcsolat kell
	* kliens elküldi a kérdést, blokkolva vár a válaszra
	* szerver fogadja a kérdést, és kiszolgálja:
		* szinkron módon
		* aszinkron módon
* hogyan kezeljük az aszinkron hívásokat?
	* ha nincs megbízható köztes szereplő
		* call-back
		* periodikus pollozás
	* ha van megbízható köztes szereplő
		* üzenetek: sok forrás (kliens), egy cél (szerver)
			* kliensek egy sorba küldik a kérdéseket, a szeverek meg ebből veszik ki
			* <img src="/src/ooterv/image-20201117141936514.png" alt="image-20201117141936514" style="zoom:26%;" />
			* egy üzenetet csak egy szerverpéldány dolgozhat fel (más nem kaphatja meg)
		* események: egy forrás (publisher), sok cél (subscriber)
			* <img src="/src/ooterv/image-20201117142037486.png" alt="image-20201117142037486" style="zoom:28%;" />
			* minden eseményt minden feliratkozó megkap, és általában a fogadók nem küldenek választ

## Technológiák elosztott kommunikáció megvalósításához

* Konkurens és elosztott minták
	* szálak közti szinkronizáció, használt erőforrásokra a kölcsönös kizárást, kliensektől beérkező kérések lekezelése
	* <img src="/src/ooterv/image-20201117142342639.png" alt="image-20201117142342639" style="zoom:33%;" />
* Sorosítás
	* <img src="/src/ooterv/image-20201117142420504.png" alt="image-20201117142420504" style="zoom:33%;" />
	* bináris: java / .net serialization
	* szöveges: XML, JSON
* objektum-relációs leképzés
	* <img src="/src/ooterv/image-20201117142542722.png" alt="image-20201117142542722" style="zoom:33%;" />
	* javaban JPA, .net-ben EntityFramework
* kommunikációs technológia és keretrendszer
	* <img src="/src/ooterv/image-20201117142644350.png" alt="image-20201117142644350" style="zoom:33%;" />
	* programnyelvfüggő megoldás &rarr; java / .net RMI
	* SOAP web services, XML-re épül
	* REST services, főleg JSON-ra esetleg XML-re épül

## SOAP webszolgátlatások

* XML-re épülő, programnyelvektől és oprendszertől független kommunikációs szabvány
* típusos api-ja van mind .net mind java-hoz
* a cserlégetett XML-ek szabványát adja csak meg
* <img src="/src/ooterv/image-20201117142943029.png" alt="image-20201117142943029" style="zoom:33%;" />
* XML előállítása és feldolgozása NEM feladata!
* <img src="/src/ooterv/image-20201117143209793.png" alt="image-20201117143209793" style="zoom:43%;" />
	* WSDL interfész leíró &rarr; megadja, hogy rajta milyen függvények milyen paraméterekkel hívható, ezek milyen kivételeket dobhatnak
* <img src="/src/ooterv/image-20201117143543360.png" alt="image-20201117143543360" style="zoom:30%;" />
	* import &rarr; kisebb részekre feldaraboláshoz
	* types &rarr; saját összetett típusok megadása
	* message &rarr; szolgáltatás által elfogadott és visszaküldött üzenetek definiálása
	* portType &rarr; szolgáltatás interfésze, azaz a rajta meghívható üzenetek
	* binding &rarr; protokoll konfigurálása
	* service &rarr; konkrét URL-t tartalmazza, ahol a szolgáltatás meghívható

## REST szolgáltatások

* HTTP protokoll kibővítése, hálózati üzenetekért felelős
* bármely HTTP protokollal való kommunikációra képes alkalmazás tud REST-el kommunikálni
* OpenAPI-t érdemes használni (REST interfész leírás)
* <img src="/src/ooterv/image-20201118192005203.png" alt="image-20201118192005203" style="zoom:33%;" />
* <img src="/src/ooterv/image-20201118192028686.png" alt="image-20201118192028686" style="zoom:33%;" />
* GET kérés
	* <img src="/src/ooterv/image-20201118192106234.png" alt="image-20201118192106234" style="zoom:20%;" />
* POST kérés
	* <img src="/src/ooterv/image-20201118192130725.png" alt="image-20201118192130725" style="zoom:20%;" />
	* felhasználónév-jelszót csak POST-al ajánlott küldeni
	* újraküldésnél vigyázni kell (ne fizessünk duplán)
* előnye: böngészőből is egyszerűen tesztelhető
* példák
	* GET /api/movies &rarr; összes filmet adja vissza
	* GET /api/movies/12 &rarr; adott azonosítójú filmet kapunk vissza
	* GET /api/movies?orderby=title &rarr; összes film cím alapján rendezve
* PUT &rarr; erőforrás frissítése, vagy ha nincs ilyen még akkor létrehoz egy ilyet
* DELERE &rarr; erőforrás törlése, gyűjteményeken általában nem működik
* eredmény elvárt formátumát megadhatja a szerver &rarr; HTTP "Accept" fejléccel
* választ a HTTP törzsben kapjuk
	* típus a Content-Type fejlécben található
* 