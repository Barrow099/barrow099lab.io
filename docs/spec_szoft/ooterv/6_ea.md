# Objektumorientált szoftvertervezés - 6. előadás</br>API tervezési elvek

## API

* API = Application Programming Interface &rarr; felhasznált programozási könyvtárak interfésze
* cél: más is jól tudja használni a modulunk

## Jól megtervezett API tulajdonságai

1. Könnyű megtanulni és memorizálni &rarr; átlátható, konzisztens
2. Olvasható kódot eredményez
3. Nehéz rosszul használni
4. Könnyű kiterjeszteni
5. Teljes: lefedi az összes felhasználói igényt
6. Jól dokumentált

## API fejlesztési folyamatok

1. Gyűjtsük össze a követelményeket
2. Írjunk use-case-eket
3. Vegyünk példát hasonló API megoldásokról
4. Definiáljuk az API-t
5. Ellenőriztessük másokkal
6. Írjunk sok-sok példát
7. Készüljünk fel a kiterjesztésekre
8. Implementáljuk
9. Ha kételkedünk, hagyjuk ki
10. Ne változtassunk rajta

## API tervezési elvek

1. Válasszunk magától értetődő neveket és szignatúrákat
2. Válasszunk egyfajta nevet az összetartozó dolgokra
3. Kerüljük a hamis konzisztenciát
4. Kerüljük a rövidítéseket
5. Általános nevek helyett használjunk specifikus neveket
6. Használjuk a helyi terminológiát
7. Ne legyünk az alattunk lévő API elnevezésének fogjai
8. Válasszunk megfeleől alapértelmezett értékeket és működést
9. Az API-t ne tegyük túlságosan okossá
10. Gondoljunk az API tervezési döntéseinek teljesítménybeli következményeire
11. Figyeljünk a szélső esetekre
12. Minimalizáljuk a módosíthatóságot
13. Vagy öröklésre tervezzünk, és dokumentáljuk is, vagy tiltsuk meg az öröklődést
14. Legyünk óvatosak, ha virtuális API-t definiálunk
15. Egy GUI-hoz készülő API tulajdonságokra épüljön
16. Próbáljuk meg előre látni a testreszabhatósági lehetőségeket
17. Kerüljük a hosszú paraméterlistákat
18. Használjunk kényelmi függvényeket
19. Induljunk el 3 sorból (inicializálás, alapvető konfiguráció, futtatás)
20. A mágia rendben van, a számok azonban nem (nevesített konstansok támogatása)
21. Jelezzük a hibát, amilyen hamar csak lehet
22. Ne használjunk checked exception-öket
23. A kivétel jelezze, hogyan lehet őt elkerülni
24. Vigyázzunk a függvények túlterhelésével
25. Teszteljük ki a belét is (teszteljünk mindent na)
26. Dokumentuljuk az API-t











