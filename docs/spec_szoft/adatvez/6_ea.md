# Adatvezérelt rendszerek

## MongoDB

* ez egy <u>nem relációs adatbázis</u> = NoSQL

### Alap koncepciók

* motiváció, azaz a relációs adatbázis hátrányok
	* fejlődő alkalmazás &rarr; bonyolult séma &rarr; nő az adatbázis (nehéz átlátni és karbantartani :( )
	* folyamatos séma változtatás
	* migráció
	* teljesítmény problémák (konzisztenciából fakadóan)
	* ötlet: hagyjuk el a szigorú sémát &rarr; NoSQL
* NoSQL-nek semmi köze az SQL-hez

### MongoDB

* rendszer architektúra (kliens-szerver)

	* <img src="/src/adatvez/image-20201101152424097.png" alt="image-20201101152424097" style="zoom:45%;" />

* logikai felépítés: klaszter > szerver > adatbázis > gyűjtemény > dokumentum

	* klaszterezéssel osztjuk szét több gépen

* **dokumentum**

	```json
	{
		name: "sue",
		age: 26,
		status: "A",
		groups: ["news", "sports"]
	}
	```

	* JSON vagy BSON
	* elemi tárolás egysége
	* kulcs-érték párokat tárol
		* kulcs: szabad szöveg lehet, nem kezdődhet $-el, _id implicit mező, caseSensitive
	* objektum-orientált világban: objektum lesz a dokumentum
	* <u>méret limit</u>: 16MB

* **gyűjtemény**

	* relációs adatbázis tábla analógiája
	* nincs sémája, definiálni se kell, csak dobáljuk bele a dokumentumokat
	* gyűjtemény = hasonló dokumentumok gyűjteménye (logikai szervezés, de nem kötelező)
	* indexek definiálhatóak rá &rarr; gyorsabb keresés elérése miatt
	* nincs tartomány integritás kritérium
		* azaz lehet egyik dokumentumban több kulcs-érték pár mint a többiben
		* és lehet az egyikben az age szám, míg a másikban string!
		* DE minket ez utóbbi zavar majd :(

* **adatbázis**

	* alkalmazás adatainak összefogása
	* jogosultságokat adatbázis szinten adhatóak
	* caseSensitive ez is!

* | Relációs séma                  | MongoDB                          |
	| ------------------------------ | -------------------------------- |
	| tábla                          | gyűjtemény                       |
	| rekord                         | dokumentum                       |
	| oszlop (skalár)                | mező (skalár & összetett)        |
	| VANNAK integritási kritériumok | NINCSENEK integritás kritériumok |
	| kulcs                          | ObjectId, unique index           |
	| külső kulcsok                  | hivatkozás _id alpaján           |
	| join                           | beágyazás, töbök                 |
	| tranzakciók                    | ~tranzakciók (van, de más cucc)  |

* **kulcs**

	* minden dokumentumban: _id (ezen kívül nem lehet saját kulcs!)
		* 12 bájtos (időbélyeg, random bájt, számláló)
		* globálisan egyedi
	* egyedi azonosításra használjuk &rarr; _id
	* egyedi biztosításra használjuk &rarr; index (szól ha ütközés van)
	* összetett kulcs NINCS, DE összetett index van

* **hivatkozás más dokumentumra**

	* beágyazás tömbként vagy hivatkozás &rarr; 1-több kapcsolat
		<img src="/src/adatvez/image-20201101204321511.png" alt="image-20201101204321511" style="zoom:33%;" />
	* normalizálás (beágyazás), join helyett &rarr; 1-1 kapcsolat
		<img src="/src/adatvez/image-20201101204335048.png" alt="image-20201101204335048" style="zoom:33%;" />

* **tranzakciók**

	* alapvetően nem támogatott ebben a világban
	* atomicitás van
	* izoláció helyett &rarr; read/write concern

### CRUD műveletek, lekérdezések

* "wire protocol": TCP/IP alapú bináris
* kérés-válasz egy JSON dokumentum
	* C# elfedi majd nekünk
* mongoDB.Driver package-et kell hozzáadni

### Használat .NET-ben

* ezt a jegyzetből kell kiolvasni

### Séma tervezés

* gyűjtemények &rarr; általában egyértelműek
* beágyazást preferáljuk (ahol lehet)
* tranzakciók csak végszükség esetén