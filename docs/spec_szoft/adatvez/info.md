# Adatvezérelt rendszerek (VIAUAC01)

**Honlap:** https://www.aut.bme.hu/Course/adatvezerelt

!!! tip
	A tárgy jegyzete az alábbi oldalon található: https://bmeviauac01.github.io/adatvezerelt/

!!! warning "Távoktatás"
	Távoktatás során az előadások valós időben Teams-en lesznek megtartva, de felvételről visszanézhetők lesznek. A gyakorlatokhoz videók készülnek, óra időpontjában konzultációra is lesz lehetőség, illetve a moodle felületen screenshoz-okkal kell bizonyítani a feladatok elvégzését. Az 5 db opcionális kisházira és a megajánlott jegy megszerzésére is van lehetőség.

## Adatok

* Kredit: 4
* Tanszék: AUT
* ZH: 1db
* Házi: szorgalmi
* Vizsga: van

## Tárgy teljesítése

* Vizsgás tárgy, vizsgára csak aláírással rendelkezők jelentkezhetnek!
* jó zh-ért plusz pont (2020. ősz)
    * 80% feletti zh: +3 pont
    * 90% feletti zh: +5 pont
* Félév végi jegy = Vizsga pont + félév közi pluszpontok
    * 0 - 19 pont: **1**-es
    * 20 - 27 pont: **2**-es
    * 28 - 34 pont: **3**-as
    * 35 - 41 pont: **4**-es
    * 42 - 50 pont: **5**-ös
* Megajánlott jegy szerezhető (2020)
    * megajánlott 5-ös: minden gyakorlat teljesítése ÉS (ZH pontszám + HF pontszám) >= 44
    * megajánlott 4-es: maximum 1 gyakorlat nemteljesítése ÉS (ZH pontszám + HF pontszám) >= 40

## Előadás

| Hét  | Dátum      | Előadás                                                      | Jelenlét | Tudom |
| ---- | ---------- | ------------------------------------------------------------ | -------- | ----- |
| 1    | 2020.09.09 | Bevezetés, adatvezérelt rendszerek architektúrái (többrétegű architektúra). Tranzakciókezelés adatbázis-kezelő rendszerekben. |          |       |
| 2    | 2020.09.16 | Microsoft SQL Server platform és sajátosságai. Haladó SQL nyelvi elemek, Microsoft SQL Server platform, T-SQL nyelv, tárolt eljárások és triggerek. |          |       |
| 3    | 2020.09.23 | -                                                            |          |       |
| 4    | 2020.09.30 | Adat szótárak, félig strukturált adatok kezelése, Linq       |          |       |
| 5    | 2020.10.07 | Adatelérési réteg, objektum-relációs leképzés, adatelérési rétegben használt tervezési minták (repository). |          |       |
| 6    | 2020.10.14 | Objektum relációs keretrendszerek .NET környezetben, ADO.NET, Entity Framework és Linq to Entity Framework. |          |       |
| 7    | 2020.10.21 | NoSQL adatbázisok, dokumentum-alapú adatbázisok működése. MongoDB platform, egyszerű lekérdezések MongoDB-ben. |          |       |
| 8    | 2020.10.28 | Lekérdezések végrehajtása és hatékonysága Microsoft SQL Server platformon. Végrehajtási tervek és lekérdezés-optimalizálás. |          |       |
| 9    | 2020.11.04 | Objektum relációs keretrendszerek Java környezetben: JPA, QueryDSL |          |       |
| 10   | 2020.11.11 | Üzleti logikai réteg megvalósítása Java környezetben: JavaEE, Spring, SpringData |          |       |
| 11   | 2020.11.18 | Üzleti logikai réteg megvalósítása .NET környezetben. ASP.NET Core platform alapjai, middleware szolgáltatások, WebAPI készítése. |          |       |
| 12   | 2020.11.25 | Üzleti logikai réteg megvalósítása .NET környezetben. ASP.NET Core platform alapjai, middleware szolgáltatások, WebAPI készítése. |          |       |
| 13   | 2020.12.02 | VIDEÓS, OTTHONI óra: Modern webalkalmazás készítése: minta alkalmazás elkészítése a félév során tanult módszerek áttekintése, demonstrációs céllal. |          |       |
| 14   | 2020.12.09 | *tartalék óra elmaradó előadás esetén*                       |          |       |

## Gyakorlatok

2020\. őszi félév

* 6 gyakorlat tartozik a tárgyhoz
* második oktatási héten kezdődnek
* **kéthetente** lesznek
* elmaradó gyakorlat NEM lesz pótolva
* Gyakorlatok sorrendje:
    1. Transzakciókezelés
    2. MSSQL platform
    3. Entity Framework
    4. MongoDB
    5. JPA & Spring Data
    6. REST API & ASP.NET WebApi

## Zárthelyi

1 darab ZH és 1 darab pótlási lehetőség a félév során

* zh: 2020.11.03. kedd 8.15
* pzh: 2020.11.20. péntek 14.15

## Házi feladat (pluszpontért és IMSC pontért)

Határidő mindig hétfő 23:59!

1. *2020.10.12 hétfő*   **MSSQL**
2. *2020.10.26 hétfő*   **ADO.NET** 
3. *2020.11.09 hétfő*   **EF**
4. *2020.11.23 hétfő*   **MongoDB**
5. *2020.12.07 hétfő*   **WebAPI**

## Vizsga

!!! warning
	még nincs róla információ

## Távoktatás

* Moodle -> időbeosztás, eredmények, házik, gyakorlatok
* Teams -> élő előadások, videók
* Jegyzetek, gyakorlatok, házik -> https://bmeviauac01.github.io/adatvezerelt/
* Előadások: élőben, de felvétel is készül róla
* Gyakorlat (6db, 2hetente): teljesen online lesz, jövőhéttől, megoldás ki lesz adva, videó is lesz hozzá
    * képernyőképek kellenek a feladatok megoldásáról -> jelenlét, moodle-ön beadás
* 5db kisházi, opcionális, pluszpont szerzési lehetőség, kódot kell beadni
* lesz ZH (min 40%), írásbeli vizsga
* megajánlott jegy kapható