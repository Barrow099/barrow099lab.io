# Adatvezérelt rendszerek - 1. előadás

<a class="md-button" style="text-align: center" href="../1_ea.pdf" download>Letöltés PDF-ként</a>

## Bevezetés

* **céljuk**: Adat tárolása, és ezen adat elérhetővé tétele, adatot manipulálnak (a manipulációt befolyásolja maga az adat)
* **adatvezérelt** (data driven) -> pl: facebook, twitter, stackoverflow, neptun, booking.com, airbnb
* **folyamatvezérelt** (control driven) -> pl egy számítógépes játék
* félévben a példa &rarr; webshop
	* fő adat: egy termék, vásárlás, számla
	* megvannak a szabályok: mikor lehet rendelni, mikor megy át egy rendelés

## Háromrétegű architektúra

* <img src="/src/adatvez/image-20200909085824811.png" alt="image-20200909085824811" style="zoom:40%;" />
* **Presentation Layer** (*Megjelenési réteg*) &rarr; a UI, pl a holnap mint kezelőfelület
* **Business Layer** (*Üzleti logikai réteg*) &rarr; üzleti szabályok, pl csak oktató írhat be jegyet viszgaidőszakban
* **Data Layer** (*Adat réteg*) &rarr; perzisztens tárolás (adatbázisokban, de ebbe egyéb részek is beletartoznak)
* ezen rétegek sokszor <u>külön gépen</u> futnak, rétegek mentén szétbontva (layer - egy gépen fut, tier - több gépen fut)
* <img src="/src/adatvez/image-20200909090331953.png" alt="image-20200909090331953" style="zoom:50%;" />

### Data Layer - *Adat réteg*

* Perzisztenciáért felelnek &rarr; data source - adatforrás (pl relációs adatbázis, google drive)
* Akkor hívjuk ***Adat réteg***-nek, ha nem tartalmazza az adatbázist

### Data Access Layer - *Adatelérési réteg*

* **Data Access Layer** (*Adatelérési réteg*) &rarr; Ha az <u>adatbázist vagy adatforrást is beleértjük</u>
* pl csatoljuk hozzá ezt a fájlt az email-hez
* szolgáltatásként nyújtja az adatok manipulálásának módját

### Business Layer - *Üzletilogikai réteg*

* **Üzleti entitások** (*Business Entities*) &rarr; főbb elemek, amiket manipulálunk (megrendelés, termék, kurzus, vizsga)
* **Üzleti komponensek** (*Business Components*) &rarr; egyszerűbb műveletek (pl vizsgajegy beírása - ezzel manipuláljuk a vizsgajegyet)
* **Üzleti folyamtok** (*Business Workflow*) &rarr; több lépéses, folyamatlépés folyamat (pl rendelés véglegesítése - sok lépésből áll, entitások halmazát manipuláljuk)
* **Szolgáltatási interfész réteg** (*Services layer*) &rarr; üzleti logika funkcióit a felhasználói réteg felé szolgáltatásként biztosítja (külön réteg, mert helyzetfüggő a megvalósítási módja)

### Presentation layer - *Megjelenítési réteg*

* **UI Components** (*Felhasználói felület komponensek*) &rarr; pl weblap, asztali vagy mobil alkalmazás
* **Presentation Logic Components** (*Megjelenési logikai komponensek*) &rarr; pl keresés, szűrés
* feladatok:
	* adatok értelmes megjelenítése
	* egyéb funkcióra lehetőségek: keresés, szűrés
	* lokalizáció (dátumok, pénzek, ezek felhasználó függően kerüljön kiírásra)
* a megjelenítési réteg nem nagyon gondolkozik, azt az üzletilogikai réteg csinálja

### Cross-cutting - *Rétegfüggetlen alkalmazások*

* Minden rétegben megjelenő közös aspektusok:
	* biztonság &rarr; bejelentkezés (aki belép, mit is csinálhat)
	* operational management (üzemeltetés szolgáltatás) &rarr; naplózás, hibakeresés, audit naplózás (rögzítjük, hogy ki mit csinál a rendszerben), konfigurációkezelés, hibák naplózása
	* kommunikáció &rarr; rétegek sokszor külön gépen vannak, ezek közt kommunikálni kell
	    * lehet szinkron és aszinkron is, adatbázissal általában szinkron, felhasználói felületről néha aszinkron

### Összegzés

* backend &rarr; minden, ami a megjelenési réteg alatt van, a többi frontend
* sokszor kód szinten is megtörténik a szétválasztás

## Tranzakciók

### Konkurens adathozzáférés (az adatbázis tekintetében)

* **Def:** Egyazon adategységhez (pl egy tábla egy rekordja) egy időben <u>többen férnek hozzá</u>, és legalább <u>egyikük módosítja</u>.

#### Tranzakció fogalma

* A feldolgozás logikai egysége, olyan műveletek sorozata, melyek csak együttesen értelmesek
* Pl: rendelés véglegesítése &rarr; kosárban levő mind a 15 termékre raktárkészletet megnézni, csökkenteni az ottani számokat, elmenteni az adatbázisba, véglegesíteni
* Alaptulajdonságok:
	* **Atomicity** (*Atomi*) &rarr; oszthatatlanság, a műveletek sorozatát egyben csináljuk végig, nem lehetnek részeredmények (vagy végig csinálja, vagy semmit nem csinál vele &rarr; atomi, oszthatatlan)
	* **Consistency** (*Konzisztencia*) &rarr; konzisztens állapotból konzisztensbe megy (közben érinthet inkonzisztenst)
	* **Isolation** (*Izoláció*) &rarr; úgy tud végig menni a műveletek sorozatán, mintha a rendszerben egyedül lenne (a rendszer biztosítja, hogy az átlapolódásból ne legyen baj)
	  * T1(A = 12, C = A+2), T2(A = 15). Ha T1 közben T2 végrehajtódik, T1 értéke nem az elvárt lesz
	* **Durability** (*Tartósság*) &rarr; tranzakció végén disk-re kiírva van az adat (nem csak memóriában)

#### Izolációs alapproblémák

* sok párhuzamos tranzakció
* izoláció: "úgy kell végrehajtani, <u>mintha</u> egymás után történnének és nem párhuzamosan"
    * de párhuzamosan futnak a tranzakciók
* 4 alap probléma:
    * **piszkos olvasás** (*dirty read*) &rarr; T1 elkezd futni, de abortál, így T2 egy nem commitált tranzakció által módosított értéket olvasott ki
      * <img src="/src/adatvez/image-20200909094142268.png" alt="image-20200909094142268" style="zoom:33%;" />
    * **elveszett módosítás** (*lost update*) &rarr; T1 és T2 is módosította, majd T1 olvasná, és nem azt látta amire ő írta át
      * <img src="/src/adatvez/image-20200909094356549.png" alt="image-20200909094356549" style="zoom:33%;" />
    * **nem megismételhető olvasás** (*non-repeatable read*) &rarr; időben 2x kiadva ugyanazt a lekérdezést, más eredményt kap T2
      * <img src="/src/adatvez/image-20200909094524542.png" alt="image-20200909094524542" style="zoom:33%;" />
    * **fantom rekordok** (*phantom read*) &rarr; rekordhalmazoknál kerül elő: T2 olyan rekordot módosít, ami benne van T1 által épp olvasott rekordhalmazban
      * <img src="/src/adatvez/image-20200909094620781.png" alt="image-20200909094620781" style="zoom:33%;" />
* **Fontos**: nem csak relációs adatbázisokban, hanem minden többfelhasználós, elosztott rendszerekben megjelenhetnek!

#### Izolációs szintek SQL szabvány szerint &rarr; ezeket a rendszer garantálja, de nekem kell jeleznem!

* **Read uncommitted** &rarr; mind a 4 probléma előfordulhat
* **Read committed** &rarr; nincs piszkos olvasás (ez az alap szint)
* **Repeatable read** &rarr; nincs piszkos olvasás, se nem megismételhető olvasás
* **Serializable** &rarr; egyik probléma se fordulhat elő

#### Tranzakciók ütemezése (megoldás az izolációs alapproblémákra)

* Csak olyan műveletek engedhetők meg, melyek nem sértik a helyes ütemezést
* Ha sérülne a helyes ütemezés, akkor a tranzakció vár
* probléma: nem tudjuk előre, hogy melyik tranzakció mit akar csinálni (csak futás közben látjuk)
* Olyan ütemezés megengedett, mely <u>konfliksekvivalens egy soros ütemezéssel</u>
	* azaz van egy olyan átrendezése a tranzakcióknak, ahol soros ütemezés teljesül

#### Ütemezés biztosítása

* **Kétfázisú zárolás (2PL)** &rarr; ha egy tranzakció hozzá akar férni egy erőforráshoz, arra zárat rak, majd ha elvégezte a dolgát (commit), leveszi a zárat.
	* probléma: **holtopont** (*deadlock*)
	* <img src="/src/adatvez/image-20200909095137744.png" alt="image-20200909095137744" style="zoom:33%;" />
	* Ha **Serializable**-t használunk, akkor <u>gyakran fordul elő holtpont</u> &rarr; csökken a hatékonyság

