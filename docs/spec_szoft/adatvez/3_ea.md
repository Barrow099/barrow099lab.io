# Adatvezérelt rendszerek

## Relációs adatbázisok adatszótára

* `IF EXISTD` &rarr; ha létezik... `drop table Invoice` &rarr; Invoice tábla eldobása... majd create table

* idempontens script &rarr; <u>adatbázis állapotától függetlenül újra lefuttatható és ugyanazt az eredményt adja</u>

* **adatszótár**

	* központi helyen tárolt információ az adatról, a formátumról, kapcsolatokról &rarr; *táblák nevei, oszlopok nevei, típusai*
	* <u>adatbáziskezelő integrált része</u> (de lehet dokumentum is)
		* csak olvasható nézet
		* felhasználható DML és DDL utasításokban
	* adatszótár tartalma
		* minden séma objektum leírása (táblák, nézetek, indexek, ...)
		* integritási kritériumok
		* felhasználók, jogosultságok
		* monitoring információk &rarr; pl deadlock megtalálására
		* auditing információk &rarr; ki módosított egyes séma objektumokat

* **MS SQL adatszótár**

	* *Information Schema Views* (ISO standard, pl táblák, nézetek, oszlopok, paraméterek, ...)

	* *Catalog Views* &rarr; teljes körű információ a szerverről

	* *Dynamic Management Views* &rarr; szerver diagnosztikai információk

	* pl: `select * from sys.objects`

		```mssql
		IF EXISTS (
			SELECT * FROM sys.objects
			WHERE type = 'U' AND name = 'Product')
		DROP TABLE Product
		```

## Félig strukturált adatok kezelése

### XML

* adatbázis és txt fájl között vannak kb

* XML: Extensible Markup Language

	* adatok <u>szöveges, platformfüggetlen</u> reprezentációja
	* emberileg és géppel is jól olvasható
	* célja: egyszerű, általános használat
	* eredetileg dokumentum leírásnak készült (pl OpenXML, XHTML)
	* <u>önleíró</u>
		* sémát és adattartalmat is egyben tárol

* felépülése

	```xml
	<?xml version="1.0" XML deklaráció encoding="UTD-8"?>
	<!-- komment -->
	<elem attributum="érték">
		<tag>tartalom</tag>
		<![CDATA[ bármilyen tartalom ]]>
	</elem>
	```

* XML hátrányok

	* szöveges adat reprezentáció
		* platformon belüli használt sorosítás &rarr; nem gond
		* szabványra épülő megoldás (pl. SOAP)
		* dokumentált séma (pl. XSD)
	* nem definiált adattípusok &rarr; dátum, bool (true vagy 1?)
	* nem egyértelmű adatreprezentáció &rarr; null (üres reprezentáció, vagy oda se írom a kacsacsőröket? üres string vs null?)
	* szöveges &rarr; nagyobb méret

* XML .NET-ből

	* System.Xml.Serialization.XmlSerializer

		```c#
		[XmlElement("Cim")]
		public class Address {
			[XmlAttribute("Varos")]
			public string city;
		}
		```

* séma

	* XML dokumentum <u>jól formázott</u>
		* minden nyitó tag le is van zárva, zárójelezés szabályai szerint
		* egyetlen gyökér eleme van
	* tartalom <u>érvényessége</u> bonyolultabb
		* jó névvel vannak benne a tagek? helyes bennük a tartalom?
		* DTD vagy XSD való ennek leírására
	* <u>validálás</u>: egy adott XML dokumentum megfelel-e egy adott sémának &rarr; programozottan eldönthető

* DOM: Document Object Model
	<img src="/src/adatvez/image-20201026090841971.png" alt="image-20201026090841971" style="zoom:33%;" />

* **XPath**

	* `konyvtar/konyv` &rarr; relatív címzés
	* `/konyvtar/konyv`  &rarr; abszolút, azaz a gyökérhez képest
	* `//konyv` &rarr; hierarchiához képest bárhol lefele
	* `//@nyelv` &rarr; bárhol egy nyelv nevű attribútum
	* `/konyvtar/konyv[1]` &rarr; első talált elem (1-től indexelünk)
	* `/konyvtar/konyv[ar>5000]` &rarr; adott tulajdonságú megkeresése

### JSON

* JavaScript Object Notation, de nem csak javascript
* tulajdonságai:
	* kompakt, olvasható, szöveges reprezentáció
	* előny: kisebb méret (nincsenek felesleges záró tag-ek)
	* memóriabeli objektum = egy JSON objektum
* alapelemei:
	* objektum &rarr; kulcs-érték párok halmaza
	* tömb &rarr; értékek halmaza
	* érték &rarr; szöveg, szám, igaz/hamis, null, objektum, tömb (van null és bool :) )
* ami NINCS benne:
	* nincs komment
	* nincs egyértelmű reprezentáció &rarr; pl dátum még mindig nincs! :(
	* biztonsági kockázat &rarr; JSON eredményt JavaScript motorral végrehajthatunk (`eval()`)
* mikor használjuk?
	* *backend* &rarr; vékonykliens kommunikáció
		* tömör, rövid (kevés hálózati forgalom, mobil klienseknek előnyös)
		* javascript tudja parsolni (webes rendszerekben)
	* *REST*
	* *JSON relációs adatbázis*
		* MS SQL Server 2016, MongoDB
* JSON .NET-ben
	* OpenSource &rarr; JsonConvert.DeserializeObject...

### XML vs JSON

<img src="/src/adatvez/image-20201026091850148.png" alt="image-20201026091850148" style="zoom:40%;" />

### XML kezelés relációs adatbázisokban

* mikor?

	* létező XML formátumú adatok
	* ismeretlen, nem definiált formájú adat
	* külső rendszerből ilyen formában érkeznek, vagy külső rendszernek ilyen formában kell átadni
	* csak tárolt, nem manipulált adattartalom
	* mélyen egymásba ágyazott adatformátum (nagyon sok tábla és join kellene a reprezentálásához)

* XML-képes relációs adatbázisok &rarr; pl MS SQL, Oracle, ...

* relációs adatok <u>mellett</u> xml adatok is

	* relációs a fő tartalom!

* XML adat köthető relációhoz &rarr; termék adatai az ár és név mellett egy XML leírás

* tárolás módja:

	* nvarchar(max) &rarr; validáció nélküli szöveg, futás időben konvertálható (költséges)
	* xml &rarr; jólformázottnak kell lennie, kereshető lesz a tartalom (where-be is használható), indexelni is lehet, csatolható hozzá séma ami ellenőrzi, manipulálható (törölhető egy tag)

* 2 fajta index

	* elsődleges &rarr; teljes tartalmat indexeli
		* egy ilyen definiálható
	* másodlagos &rarr; konkrét xml elemekre definiált

* séma hozzárendelés xml oszlophoz (opcionális)

	* adat validáció automatikusan
	* lekérdezés optimalizáláshoz is

* xml-ben keresés &rarr; **XPath**-al

	```mssql
	/* példa1 - projekció */
	select Description.query('/product/num_of_packages') from Product
	/* példa2 - konkrét érték kiszedése */
	select Description.value('(/product/num_of_packages)[1]', 'int') from Product
	/* példa3 - szűrési feltétel */
	select Name from Product
	where Description.exist('/product/num_of_packages eq 2') = 1
	/* példa4 - módosítás replace */
	update Product
	set Description.modify('replace value of (/product/num_of_package/text())[1] with "2"')
	where ID=8
	/* példa5 - módosítás insert */
	update Product
	set Description.modify('insert <a>1</a> after (/product)[1]')
	where ID=8
	/* példa6 - módosítás törlés */
	update Product
	set Description.modify('delete /product/a')
	where ID=8
	```

* lekérdezés eredményét XML-ben lekérése &rarr; for xml auto

	* `select ID, Name for Customer for xml auto`

## LINQ

* motiváció - C#

	* kinek van ma szülinapja? &rarr; for mindenki, ha ma van szülinapja adja `birthDayToday` listához
	* ez SQL-lel SOKKAL könnyebb lenne! &rarr; dekleratívan könnyebb megadni (mint imperatívan = hogyan)

* speciális gyűjteményekkel megy &rarr; `IQueryable`, `IEnumarable`

* lambda kifejezése

	```c#
	bool foo(int value) {
		return value % 2 == 1;
	}
	
	value => value % 2 == 1; //lambda kifejezés
	```

	* általánosan: `(in1, in2) => { code; return something; }`

* LINQ szintaktika

  * szintaktika:

    ```c#
    from m in list
    where m.Title.StartsWith("S")
    select m.Title;
    ```
    
  * szebb szintaktika:

    ```c#
    list
    .Where( m => m.Title.StartsWith("S"))
    .Select( m => m.Title);
    ```

* LINQ to *

	* Linq to Objects &rarr; minden gyűjteményen használható
	* Linq to Entity Framework &rarr; SQL kóddá fordul
	* <u>Késleltetett kiértékelés</u> &rarr; eredményhalmaz iterálásakor keletkezik (mikor a leírón végig megyünk)
		* addig csak leíróként van meg!
	* System.Linq névtér



