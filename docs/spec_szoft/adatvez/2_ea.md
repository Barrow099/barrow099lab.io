# Adatvezérelt rendszerek - 2. előadás

<a class="md-button" style="text-align: center" href="../2_ea.pdf" download>Letöltés PDF-ként</a>

## Adatbázis

* Def: Logikailag összefüggő adatok rendezett gyűjteménye
	* adat &rarr; mérhető és rögzíthető, sokféle lehet
	* rendezett &rarr; könnyű tárolás, módosítás lekérés
	* összefüggő &rarr; szükséges adatok kellenek
* **Metaadat**
	* adatdefiníció, adatstruktúra, szabályok és korlátozások
	* adatszótár (data repositiry / data dictionary)

### Relációs adatmodell

* matematikai alap

* alap komponensek

	* **tábla** vagy **reláció**, adatok sorokban és oszlopokban
		* 2d reprezentáció, megnevezett oszlopok vannak, sorok száma korlátlan
		* sor és oszlop kereszteződése &rarr; cella, ebben max 1 érték van
		* <u>nincs két egyforma sor</u> &rarr; nem teljesül a halmaz tulajdonság
		* sorok sorrendje lényegtelen
	* **integritási kritériumok** = érvényességi szabályok (milyen hosszú lehet egy szöveg)
		* *tartományi integritás* &rarr; egy adott oszlopban milyen tartományból származó érték lehet (adattípus, hossz, értéktartomány megszabása)
		* *entitás integritás* &rarr; 1 rekord mikor érvényes (elsődleges kulcs sehol se lehet NULL)
		* *referenciális integritás* &rarr; táblák között, pl külső kulcsok (csak létezőre hivatkozhat, hivatkozott rekord nem törölhető)
		* *működési korlátozás* &rarr; üzletvitelből származó, üzleti logika feladata mert relációs modellen túlmutat (pl tárgy csak tárgyfelvételi időben vehető fel)

	* **adatmanipulációs nyelv**
		* relációs algebrából adódóan &rarr; SQL

* elnevezések

	* **attribútum** &rarr; nevezett oszlop
	* **rekord** &rarr; sor
	* **fokszám** &rarr; attribútumok száma
	* **kardinalitás** &rarr; sorok száma

* **felhasználói séma** = objektumok az adatbázisban

  * adatbázisban levő objektumok összessége
  * platform függő elemek

## Microsoft SQL Server platform - MSSQL

* stabil adatbázisrendszer
* szerver komponensek &rarr; sok van belőlük, komplex rendszer

### Felhasználói séma elemei

* **tábla**
	* oszlop
	* **computed column** = számított oszlopok
		* lehet virtuális (mindig újra számolódik), és tárolt is
* **nézetek** = lekérdezések eredményei
	* indexelhető &rarr; ilyenkor tárolódik (amúgy nem tárolódik)
* **indexek** = megmondja az adatbázisnak, hogy hogyan fogunk benne keresni
	* jobb index &rarr; gyorsabb keresés az adatbázison
* **szekvencia**
	* számláló
	* megadjuk, hogy honnan indul, hányasával lépked, stb
* **programmodul**
	* eljárás, függvény, trigger, assembly

### Adattípusok

* szöveges adattípusok
	* char(n), varchar(n)
	* nchar(n), nvarchar(n)  &rarr; ékezetes karaktereket is tud tárolni (unicode)
	* varchar(max), nvarchar(max) &rarr; ezt kerüljük
	* char és nchar &rarr; fix hosszú, többi helyre szóközt tesz
	* varchar, nvarchar &rarr; változó hosszú
* numerikus
	* int, float, numeric(p, s)
* dátumok
	* datetime &rarr; 1753. január 1-től tud számot reprezentálni
	* datetime2 &rarr; időzónát is tárol, minvalue-t is tud
* nagyméretű objektumok
	* Image, TEXT
* egyéb
	* Money, SQL_VARIANT, VARBINARY (ez jobb a nagyméretű objektumoknál), XML

### Elsődleges kulcsok generálása

* Identity kulcsszó

  ```sql
  create table Statusz(
  	ID int identity(1, 1) primary key,
  	Nev nvarchar(20))
  insert into Statusz values ('Kész')
  ```

  * lekérdezése ennek

  	* ident_current('Status'), TODO itt is hiányzik még valami
  * ha lehet <u>mindig</u> generált elsődleges kulcsot használjunk

### Tranzakciós határ

* kapcsolat szintjén létezik a tranzakció
* tranzakció kezdés módjai (beállítás függő)
	* *auto commit* &rarr; minden utasítás önálló tranzakció (alapértelmezett)
	* *explicit tranzakciók* &rarr; explicit tranzakció `begin tran`, egymásba is ágyazhatók
	* *implicit tranzakciók* &rarr; tranzakció vége jel után jön új tranzakció
* DML és DDL utasítások is tranzakciók része

### Izolációs szintek támogatottsága

* minden SQL szabvány szerinti szintet támogat
* a **Read committed** az alapértelmezett
	* olvasás: megosztott zárakat használ
	* írás: más nem olvashatja
* szabványtól eltérő &rarr; **snapshot**
	* tranzakció kezdetekor pillanatkép, az olvasható
	* adatbázis szinten engedélyezni kell
	* jó cucc, de NAGYON drága (külön engedélyezni kell adatbázis szinten)

### Adatbázisok tárolása

* adatbázis
	* adatfájl (.mdf), filegroup is lehet
	* tranzakciós napló (.ldf)
	* több sémát tartalmazhat
		* alapértelmezett séma: dbo
		* ez jó logikai csoportosításra, hozzáférés szabályozására
	* rendszer adatbázisok: Master, Model, stb

### Hozzáférés szabályozás

* rendszer szintű &rarr; adatbázis szerverhez ki férhet hozzá
* adatbázis szintű &rarr; konkrét adatbázishoz
* séma szintű &rarr; sémák csoportosítása
* objektum szintű
	* konkrét objektumhoz, esetleg objektum szinten is megadható
* tiltás is megadható
* sor szintű hozzáférés nem szabályozható

### Tranzakciós naplózás

TODO &rarr; jegyzetből elolvasni

## Adatbázis szerver oldali programozás

* Adatmodell: táblák
* Adatmanipuláció: SQL nyelv
* Vannak feladatok, amik kimutatnak a relációs adatmodellből
	* Üzleti logikai rétegben valósítjuk meg
	* Adatrétegben valósítjuk meg &rarr; **Adatbázis szerveroldali programozása**
* Szerveroldali programozás <u>előnyei</u>
	* Adatbázis felelős a konzisztenciáért (innentől már adatforrás és szolgáltatás is)
	* Adatbiztonság
	* Teljesítmény növelés (csökkenő hálózati forgalom, cache is jobb lesz)
	* Termelékenység (egyszerűbb karbantartás, több komponens által hívható modulok készítése)
* Szerveroldali programozás <u>hátrányai</u>
	* Nem szabványos dolgok (platformfüggő elemek)
	* Interpretált (azaz nem lefordított kód, teljesítménye rosszabb)
	* Növeli a szerver terhelését
	* Nem illetve nehezen skálázható

### Transact-SQL nyelv (T-SQL)

* Csak az MSSQL Server nyelve

* amik lesznek: változók, utasítás blokkok, ciklusok, strukturált hibakezelés, új konstrukciók

* utasítás blokk

	```mssql
	BEGIN
		/*TSQL utasítások*/
	END
	```

* változók

	* DECLARE-el deklaráljuk, @-al kezdődik a nevük
	* deklarálás után a változó értéke NULL

	```mssql
	DECLARE @nev nvarchar(30), @szam int = 5
	```

	```mssql
	DECLARE @szam int
	SET @SZAM = 3
	```

	```mssql
	DECLARE @szam int, @nev nvarchar(30)
	SELECT @szam = id, @nev = nev FROM termek
	WHERE ... /*ha a lekérdezés több sorral tér vissza, a változó értéke az utolsó sor értékével fog megegyezni*/
	```

* vezérlési szerkezetek

	```mssql
	IF ... ELSE
	WHILE
	/*ciklusvezérlő utasítások*/
	BREAK
	CONTINUE
	```

#### Tárolt eljárások

* tárolt, mert az adatbázis szerverben van
* eljárás, mert nincs visszatérési értéke
* Belső
	* T-SQL nyelv
	* interpretált
	* zárt környezetben van
* Külső &rarr; .NET assembly

##### Tárolt eljárás létrehozása

* `create or alter procedure`
	* létrehozza/módosítja a tárolt eljárást a sémában
	* értelmezi, ellenőrzi, eltárolja
* első futtatáskor &rarr; fordítás, optimalizálás
* stored prucedure cache &rarr; használaton kívüli tervek elöregednek

##### Tárolt eljárás újrafordítása

* ha releváns tartalom változik az adatbázisban &rarr; újra lesz fordítva
* újrafordítás kérése &rarr; sp_recomplie

##### Tárolt függvények

* van visszatérési értéke, nem változtathat az adatbázisban (csak olvashat, írni nem írhat)
* Típusai
	* Scalar-valued &rarr; értéket számol ki (min, max)
	* Table-valued &rarr; rekordhalmazzal tér vissza
	* Aggregate &rarr; saját oszlopfüggvény, .NET Assembly-ben írunk ilyet
* van jónéhány beépített (string hossza, string üsszefűzése, stbstb)

##### Tárolt eljárások kezelése

* módosítás &rarr; `ALTER PROCEDURE`
* törlés &rarr; `DROP PROCEDURE`

##### Hibakezelés

* @@Error függvény

	* minden utasítás után lekérdezhető
	* ha nincs hiba, akkor 0-t ad vissza

* struktúrált kivétel kezelés

	```mssql
	BEGIN TRY
		/*utasítások*/
	END TRY
	BEGIN CATCH
		/*utasítások*/
	END CATCH
	```

* hiba generálás

	* Raiserror
	* Throw

#### Triggerek

* eseménykezelő tárolt eljárások
* mire használható:
	* származtatott értékek karbantartása (denormalizáció)
	* naplózás
	* statisztikák gyűjtése (hányszor történt meg valami)
	* máshogy nem kifejezhető referenciális integritás
* események
	* DML események &rarr; mikor módosul a tábla, táblához kötődik
	* DDL triggerek &rarr; create, alter, drop, sémákhoz kötődnek
	* rendszeresemény &rarr; logon, logoff, syserror, ...
	* instead of triggerek &rarr; adott utasítás helyett valami más hajtódik végre
* DML triggerek &rarr; <u>utasítás szintű</u>
	* ha 10 sort módosító utasítás van, akkor egybe kapja a trigger azt a 10-et, egyszer hívódik meg
	* adatmódosítás után hajtódik végre
* **Módosított rekord elérése**
	* Napló táblákon keresztül
		* napló tábla struktúrája:
			<img src="/src/adatvez/image-20201011111246030.png" alt="image-20201011111246030" style="zoom:50%;" />
		* csak a trigger-ben értelmezett
* Triggerek egymásra hatása
	* Kaszkád triggerek &rarr; ami kivált egy következő triggert (32 mélységig)
	* Rekurzív triggerek &rarr; megengedett, de nagyon hagyjuk
	* Ugyan ahhoz az eseményhez több trigger kapcsolása
		* lehet, de <u>sorrend nem befolyásolható, nem ismert</u>
* Triggerek és tranzakciók
	* szülő tranzakció részét képzi egy trigger
* Instead of triggerek &rarr; náluk lehet nem okoz rekurziót, ha belőlük a saját táblájukra hivatkozunk

#### Kurzor

* foreach iterátor
* több rekordot visszaadó lekérdezések feldolgozására