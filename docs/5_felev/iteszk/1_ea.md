# IT eszközök technológiája - 1. előadás

<a class="md-button" style="text-align: center" href="../1_ea.pdf" download>Letöltés PDF-ként</a>

## Miről lesz szó a tárgyban?

* mi van belül és kívül &rarr; mikroprocesszorok, memóriák, segédprocesszorok, kijelzők, érzékelők
* milyen <u>komponensek</u>ből áll egy ilyen rendszer, és <u>hogyan tervezik</u> ezt?
* <u>hogyan szerelnek össze</u> egy ilyen rendszert?

## Intergrált áramkörök és összeköttetésük

* <img src="/src/iteszk/image-20200913232616917.png" alt="image-20200913232616917" style="zoom:40%;" />
* **nyomtatott huzalozású lemez** &rarr; ez köti össze a rajta levő **integrált áramköröket** és egyéb **diszkrét alkatrészeket**
* **integrált áramkörök** (IC-k) &rarr; fekete tokban, többmilliárd komponenst tartalmaznak
	* soksok **MOS tranzisztor** &rarr; ~vezérelhető kapcsoló (de nem teljesen ideális)
	* MOS tranzisztorokból &rarr; **digitális kapuk** (amikkel már tudunk logikákat csinálni)

## Emlékeztetők

### Emlékeztető digitális áramkörökről

* digitális logika &rarr; <u>áramkörökkel</u> megvalósítva
* **feszültséglogika** = tápfeszültség - igaz, 0V - hamis (távfeszültség = 5V, 3.3V, 2.5V, 1.8V)
* **művelet elvégzése** &rarr; kapukkal, amik **tranzisztorokból** (vezérelhető kapcsolókból) áll

### Mikroelektronika

* kisméretű **integrált áramkörök** tervezése és gyártása
* félvezető alapanyagból, kisméretű szilícium lapkára (chip) készülnek
* tranzisztorokból állnak
* <u>tömeggyártás</u>sal vannak előállítva az integrált áramkörök

### Moore-törvény

* 1965 &rarr; Gordon Moore: "*egy lapkára integrálható tranzisztorok száma másfél-két évente megduplázódik*"
* **jóslat továbbra is helytálló**

### Integrált áramkörök gyártási technológiája

* VLSI &rarr; very large scale integration (10 000-nél több tranzisztort tartalmaz)
* **planár technológia** &rarr; egy félvezető szeletre sík elrendezésben történik a gyártás
* rudaskban készülő **szilícium egykristály** (50-450nm átmérőjű, 0.25-0.7mm vastag szeletek)
	* egy ilyen szeleten több ezer IC készül
	* szilícium egykristály &rarr; ~35-45cm
		* szilícium dioxid = kvarc, kvarchomok
		* ezt megolvasztják, szénnel redukáltatják &rarr; szilícium és szénmonoxid
		* ezt a szilíciumot tisztítják (sósavgázzal), triklór szilánt frakcionált diszcillációval megtisztítják
		* hidrogénnel reagáltatva egy kristálymagra kiválik
		* újra megolvaasztják, majd belelógatnak egy kristály magot
		* ezt forgatják és húzzák ki, így épül fel az egykristály

### Fotolitográfia &rarr; alakzatok kialakítása

* cél: felszínen csak szelektív területeken történjen valami
* **fotorezisztert** visznek a felületre &rarr; megvilágítás hatására bizonyos oldószerekkel szemben oldhatóvá vagy nem oldhatóvá válik
* **maszkon** keresztül megvilágítják &rarr; valahol átlátszó, valahol nem
* megvilágított részt leoldják, fotorezisztet megszilárdítják

### Méretcsökkentés

* elemek számának növelése
	1. chip méretének növelésével &rarr; gyártási hiba valószínűsége egyenesen arányosan nő
		* optimális chipméret &rarr; 500mm<sup>2</sup>
	2. elemek és összeköttetések méretének csökkentésével &rarr; **méretcsökkentés**
* méretcsökkentés hatása
	* **poz**: késleltetés csökken (növelhetjük az órajelet), egy kapu fogyasztása csökken
	* **neg**: <u>felületegységnyi fogyasztás megnő</u>
* "*x nm-es*" techonlógia
	* jelentés: megvalósítható legkisebb méret *x nm-es*
	* A verzió: 2 fémvezeték bal oldalai közti távolság (fémvezeték szélességét és köztük levő távolságot is jellemzi)
	* B verzió: milyen közel lehetnek egymáshoz a poliszilícium csíkok
	* jelenleg &rarr; 7-10nm
		* szilícium rácsállandója &rarr; ~ fél nm
		* 2 atom közötti távolság &rarr; ~ 0.2 nm

### IRDS roadmap

* várható fejlődés &rarr; ~2031-re 1 nm-es elérhető

## Félvezetők

* átmenet a szigetelő és vezető anyagok között
* negatív hőmérsékleti együttható (növekvő hőmérsékletre csökken az ellenállásuk)
* legfontosabb félvezető anyagok: Si (szilícium), Ge (Germánium)
	* Si &rarr; integrált áramkörök, tranzisztorok, napelemek
* Vegyületfélvezetők: GaAs, GaAsP, GaN, SiC
	* LED, HEMT (nagyfrekvenciás analóg feldolgozás), teljesítmény félvezetők
* szerves anyagú félvezetők
	* OLED

#### Mitől lesz egy anyag vezető, félvezető, szigetelő?

* kvantummechanika
	* atomban elektronok csak meghatározott energiaállapotokat vehetnek fel
	* egy energia állapotban max 2 elektron
	* energiaminimumra törekednek
	* kristályban &rarr; megengedett sávok (állapotok helyett), tiltott sávok
	* vegyérték sáv (legfelső teljesen betöltött): az itt levő elektronok kötésbe vannak
	* vezetési sáv (legfelső csaknem üres): az itt levő elektronok könnyen elmozdíthatók
* **vezetők** (fémek) &rarr; nincs tiltott sáv a vegyérték és vezetési közt, így könnyen átkerülhetnek az elektronok
* **szigetelők** &rarr; széles tiltott sáv található, így vegyérték sávból közel 0 eséllyel kerül elektron a vezető sávba
* **félvezető** &rarr; néhány eV a tiltott sáv (Si - 1.1eV)
	* hőmérséklet nő &rarr; nagyobb valószínűséggel kerül fel atom a vezetési sávba &rarr; több töltéshordozó, jobb vezetési képesség
	* **tiszta félvezető** (*intrisic*) &rarr; ugyan annyi elektronhiány (lyuk) található mint ahány elektron a vezetési sávba felkerült
		* *intrisic* félvezető nem vezeti túl jól az áramot
* **generáció** &rarr; vegyértékből vezető sávba
* **rekombináció** &rarr; vezetőből vegyérték sávba

#### Adalékolás

* idegen atomokat rakunk a kristályrácsba (kis mennyiségűt)
* 2 fajta adalékolási mód
	* **n típusú adalék** &rarr; adalék atomok több elektronnal rendelkeznek
		* a többlet a kristály vezetési sávjába kerül
		* elektronok többségi töltéshordozók lesznek
	* **p típusú adalék** &rarr; adalék atomok kevesebb elektronnal rendelkeznek
		* befogják a kristály szabad elektronjait
		* mozgóképes elektronhiány lesz
* kb <u>annyi új töltéshordozó</u> keletkezik, <u>amennyi adalékatom</u> a kristályba kerül  &rarr; sokkal jobb vezetők!
* <img src="/src/iteszk/image-20200914094358663.png" alt="image-20200914094358663" style="zoom:50%;" />

