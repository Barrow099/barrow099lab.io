# RTL modellezés és szimuláció labor

* egyszerű logikai és szinkron szekvenciális hálózatok RTL modelljének elkészítése VHDL nyelven
* azon verifikációja HDL-alapú lineáris testbench segítségével

## jegyzet

* tervezés HDL nyelven szokott történni (VHDL, Verilog, SystemC, ...)
	* több **elvonatkoztatás** modellezésére képes
		* időzítési információtól mentes **viselkedési** leírások
		* automatizált szintézisre optimalizált **regiszter-transzfer szintű**
		* és akár **kapuszintű** modellek is
* specifikáció: magas szintű programozási nyelven, <u>algoritmus</u> formájában van meg
* feladat:
	* algoritmus egyes fázisaihoz <u>műveletvégző erőforrásokat</u> rendelni
	* azok összeköttetéseit megvalósítani, közben figyelembe venni az
		* erőforrásigény,
		* számítási teljesítmény,
		* és fogyasztás követelményeket
	* megfelelően időzített <u>vezérlőjel</u> előállításáról
		* fázisjelséma &rarr; egyes erőforrások működésének <u>szinkronizációjához</u>
		* **aszinkron órajeltartományok** &rarr; egyedi fázisjelsémát alkalmazó makrocellák

### Logikai kapuk tervezése

* 2 fő alegység
	* **egyed-deklaráció** &rarr; tervezési egység interfésze
		* **portlista** &rarr; egyed ki- és bemeneteli kapcsolatai, azok típusa és mérete
		* **generikus paraméterek listája** &rarr; egyed beültetésekor megadható paraméterek
			* egyed alapvető funkciójára <u>nincsenek hatással</u>. de a <u>kód újra-felhasználhatóságát</u> jelentősen javítják
	* **funkcionális leírás** (más néven **architektúra**)
* példa: AND, OR, XOR, NAND kapu VHDL modellje

### Összeadó áramkörök

* tanulság: a VHDL modell *architecture* részében az egyes utasítások sorrendje nem köt
	* architecture mező = konkurens utasításblokkot képez
	* azaz <u>az itt felsorolt utasítások párhuzamosan működő áramköri részeket írnak le</u>
	* fontos, hogy a process (3. rész) már sorrendi, itt számít az utasítások sorrendje
* a VHDL erősen típusos nyelv! (sokat kell konvertálgatni)
* std_logic_vector már magában tudja pl két szám összeadását

### Szinkron számláló

* 