# IT eszközök technológiája - 2. előadás

<a class="md-button" style="text-align: center" href="../1_ea.pdf" download>Letöltés PDF-ként</a>

## A félvezető dióda

* félvezető anyagokat általában adalékolják (töltéshordozó számának változtatása)
* **pn átmenet** = félvezető dióda
	* p - anód, n - katód
	* anód pozitívabb mint a katód &rarr; nagy árammal vezet &rarr; dióda kinyit
	* anód negatívabb mint a katód &rarr; kis árammal vezet &rarr; dióda lezár
* felhasználás:
	* ez fogja egymástól elszigetelni a tranzisztorokat
	* **egyenirányítás** &rarr; váltakozó feszültségből csak pozitív feszültséget csinál

## A MOS tranzisztor

* MOS: Metal-Oxide-Semiconductor
	* félvezető általában szilícium
* ma vezető technológia
	* 1957 - első MOS tranzisztor
	* 1970 - első (nagy tételben árult) MOS IC
	* 2005 - állítólag több MOS tranzisztor készült ahány rizs lett termesztve

### Kristálytan

* egykristály: hosszú távúrendezettség
* polikristály: szemcsékből áll, de ezen szemcsék változtathatják helyüket
* amorf anyag: nincs, vagy csak rövid távú rendezettség van  (pl üveg)
* <img src="/src/iteszk/image-20200921083109482.png" alt="image-20200921083109482" style="zoom:55%;" />

### MOS tranzisztor működési elve

* félvezető alapon (**szubsztrát**) létrehozunk két eletródát
* **source-ból** származó töltéshordozók és **drain-be** gyűjtődnek
* **gate** elektróda szabályozza (mintha egy szívószált összenyomnánk)

### MOS tranzisztor kialakítása

* 2 fajta &rarr; NMOS és PMOS
* nMOS &rarr; p típusú szubsztrát, n típusú source és drain
* pMOS &rarr; n típusú szubsztrát, p típusú source és drain
* <img src="/src/iteszk/image-20200921083909541.png" alt="image-20200921083909541" style="zoom:50%;" />

### MOS tranzisztor működése

* **nMos**
	* alapeset &rarr; nem vezet &rarr; source és drain közt lezárt pn átmenet
	* gate meghalad egy <u>küszöbfeszültséget</u> &rarr; **inverzió** (gate alatt elektronok jelennek emg) &rarr; elektronok el tudnak jutni a source-ból a drain-be
* pMos &rarr; pont fordítva

### Mire jó egy MOS tranzisztor

* kapcsolásra &rarr; nem teljesen ideális de jól működő
* igaz &rarr; tápfeszültség, hamis &rarr; 0V
* nMOS szubsztrát &rarr; földre kapcsolt, pMOS szubsztrát &rarr; tápfeszültségre kapcsolt
* nMOS-ra 0V &rarr; ki van nyitva a tranzisztor, nMOS-ra tápfesz &rarr; be van kapcsolva a tranzisztor
* pMOS-ra tápfesz &rarr; ki van nyitva a tranzizstor, pMOS-ra 0v &rarr; be van kapcsolva a tranzisztor
* pMOS-nál azért van kis karika, mert az nMOS inverze!

### CMOS

* 2 fajta tranzisztorunk van &rarr; egyik logikai magas szintű vezérlésre kapcsol, a másik logikai alacsony színtűre
* komplementer MOS áramörök &rarr; CMOS
* tudunk készíteni:
	* invertert
	* alapkaput
	* ebből bármit

## A digitális logika alapvető tulajdonságai

### Bool algebra áramköri megvalósítása

* értékkészlet: x ∊ {0, 1}, műveletei: negálás, és, vagy
* értékkészlet elemeihez &rarr; feszültséget rendelünk
	* 1 &rarr; V~H~ (általában tápfeszültég)
	* 0 &rarr; V~L~ (általában föld)
	* **swing** &rarr; V~H~-V~L~(távolság feszültségben)
	* **rail** &rarr; tápfeszültég és a föld különbsége

### Inverter

* V~L~ = f(V~H~) és B~H~ = f(V~L~)
* **transzfer karakterisztika**
* **komparálási feszültég** &rarr; ahol V~OUT~ = V~IN~ &rarr; efelett 1-nek ez alatt 0-nak tekintjük
	<img src="/src/iteszk/image-20200921090738187.png" alt="image-20200921090738187" style="zoom:50%;" />
	* laposan futó 2 szélső szakaszok &rarr; zaj/zavar ennél kisebb (ezeket elnyomja)
	* középső szakasz meredek &rarr; <u>kis bemeneti változásra nagy kimeneti feszültségváltozás</u> történik
* **jel-regeneráció**
	* logikai jel szintje regenerálódik &rarr; digitális logikai kapunak 2 egyensúlyi helyzete van
	* <img src="/src/iteszk/image-20200921091316836.png" alt="image-20200921091316836" style="zoom:50%;" />
* **robosztusság**
	* digitális logikai áramkör **robosztus**
	* kevéssé érzékeny
		* bemeneten levő zajra, tápfeszültség megváltozására, környezeti hőmérsékletre, alkatrészek paramétereinek változására
* **késleltetés**
	* t~pHL~ - kimenet magasról alacsony szintre vált
	* t~pLH~ - kimenet alacsonyról magas szintre vált
	* **kritikus út** &rarr; logikai hálózatban a leghosszabb késleltetésű útvonal
		* ez határozza meg a teljes hálózat sebességét
* **teljesítmény és energia**
	* **teljesítmény** = egységnyi idő alatt felvett energia
		* Watt, P = V*I
		* átlagos teljesítmény:
			* <img src="/src/iteszk/image-20200921091903716.png" alt="image-20200921091903716" style="zoom:50%;" />
		* (V~DD~ a kapu tápfeszültsége, I az árama)
	* **energia**
		* E = ∫ P(t) dt
		* Joule (vagy ha nagy: kWh = 3.6 MJ)
* **teljesítmény-késleltetés szorzat (*PDP*)**
	* egyszerre jellemzik a digitális kaput
	* **Power-Delay product** &rarr; 1bit feldolgozása mennyi energiát igényel
		* technológia mérőszáma

## A CMOS

* n és p csatornás tranzisztorokból &rarr; innen név: komplementer
* jó tulajdonságok
	* logikai szintek tiszták &rarr; **rail-to-rail** (0-tól tápfeszültségig)
	* statikus áramfelvétel alacsony
	* fel és legkapcsolási késleltetések általában megegyeznek
	* tápfeszültség-érzéketlenek (széles tartományban működnek)
	* jól integrálhatóak

### CMOS inverter

* <img src="/src/iteszk/image-20200921092951484.png" alt="image-20200921092951484" style="zoom:50%;" />
* a terhelés kapacitív
	* **összekötő hálózat kapacitása határozza meg leginkább a késleltetést**
	* azaz pl az inverterek közti vezeték

### Késleltetés

* kapacitás töltése-kisütése határozza meg
* nagyobb kapacitás &rarr; nagyobb késleltetés
* a <u>tápfeszültég növelésével a késleltetés csökken</u> (mivel nagyobb árammal töltjük a kapacitást)

### Teljesítmény

* statikus fogyasztás alacsony, oka &rarr; szivárgás (leakage)
* dinamikus fogyasztás, oka &rarr; kapcsolási eseményeknél
	* **átkapcsolás** &rarr; bemeneti jel felfutó szakaszában mindkét tranzisztor egyszerre nyitott
	* **töltéspumpálás** &rarr; 
		* a kimeneti kapacitás feltöltése vagy kisütése miatt keletkezett fogyasztás
		* ez adja a legnagyobb részét a fogyasztásnak!
		* CMOS áramkör fogyasztása <u>egyenesen arányos az órajelfrekvenciával</u> és <u>négyzetesen arányos a tápfeszültséggel</u> &rarr; **P ~ f*V~DD~^2^**

### Dynamic Voltage Frequency Scaling

* órarekvencia és tápfeszültség változtatása az igényeknek megfelelően
	* nagyobb órajelhez &rarr; nagyobb feszültség
	* kisebb órajelhez &rarr; elég kisebb feszültség is
	* teljesítmény &rarr; <u>négyzetesen</u> változik a tápfeszültséggel

### Energia

* kisebb frekvencia &rarr; több órajel kell egy taszkhoz &rarr; nem nyerünk energiát
* felhasznált energia az **órajelek számával és a tápfeszültség négyzetével** arányos

## Statikus CMOS alapkapuk

* **PUN - pull up network** &rarr; p-csatornás tranzisztorok, V~DD~ tudja felhúzni ha kell
* **PDN - pull down network** &rarr; n-csatornás tranzisztorok, föld tudja lehúzni ha kell

### CMOS kapu felépítése

* kapcsoló jellegű működés
	* ÉS &rarr; tranzisztorok soros kapcsolása
	* VAGY &rarr; tranzisztorok párhuzamos kapcsolása
* PUN és PDN **reciprok hálózatok**, azaz ellentétes módon 

#### CMOS NOR kapu

* <img src="/src/iteszk/image-20200921121600818.png" alt="image-20200921121600818" style="zoom: 33%;" />

#### CMOS NAND kapu

* <img src="/src/iteszk/image-20200921121813194.png" alt="image-20200921121813194" style="zoom:30%;" />