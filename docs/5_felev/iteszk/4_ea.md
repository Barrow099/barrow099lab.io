# IT eszközök technológiája - 4. előadás</br>Digitális rendszertervezés

<a class="md-button" style="text-align: center" href="../4_ea.pdf" download>Letöltés PDF-ként</a>

## Absztrakciós szintek

* digitális rendszerek bonyolultsága exponenciálisan növekszik
* tervezési metódusoknak is fejlődnie kell!
	* tervezés <u>magasabb absztrakciós szinten</u>
	* alacsonyabb szintek <u>automatikusan</u> készülnek el, az ember csak felügyel és <u>kényszereket</u> szab
* automatizálás negatívuma &rarr; romlik a hatékonyság

### Gajski-Kuhn Y diagram

<img src="/src/iteszk/image-20201004231044746.png" alt="image-20201004231044746" style="zoom:30%;" />

* körökből álló a ábra
* 3 vizsgálati szempont
	* **viselkedési** szempont
	* **struktúrában** hogy jelenik meg
	* **fizikailag** hogy jelenik meg

#### Kör szintek

* **rendszerszint**
	* fő cél: részekre bontás (partícionálás)
	* hardverfüggetlen, akár több különböző megvalósítás is lehet
	* eszköz: magas szintű programnyelv
* **algoritmus**
	* fő cél: funkciók viselkedés szintű modellezése
	* eszköz: c++, SystemC, mert <u>erőforrásigényes a szimuláció</u>!
* **regiszter-transzfer szint (RTL)**
	* elvonatkoztatott szint
	* *<u>regisztereket</u> és a köztük végbemenő <u>adatátvitelt</u> definiáljuk, beleértve az adatátvitel <u>összeköttetéseit</u> és az átvitel <u>időzítését</u> is.*
	* mi történjen az áramkörben az <u>órajel két aktív éle között</u>
* **logikai szint**
	* logikai kapuk és összeköttetések &rarr; hálózatlista
	* <u>eddig kb megvalósításfüggetlen</u> a tervezés
* **áramköri szint**
	* kapcsolási rajz
	* fizikai terv &rarr; fizikai tervezés

#### Kör szintek közti lépés

* szintézis &rarr; magasabbról alacsonyabb absztrakciós szintre lépés (beljebb lépés)
* minél beljebb vagyunk, annál inkább automatikus program váltja fel az embert
* **magas szintű szintézis (HLS)**
* **logikai szintézis**
* **elhelyezés és huzalozás**

##### Magas szintű szintézis

* algoritmus szintről RTL szintre
* általában emberi közreműködésével, DE egyre több jó szintézer program
	* időzítésmentes C kódból generálnak RTL szintű leírást
* probémák:
	* **Vezérlés** jellegű funkció esetén
		* <u>állapotgépek</u> konstruálása, állapotgépek és <u>kisegítő áramkörök együttműködésének</u> megszervezése
	* **Adatfeldolgozás** jellegű funkció esetén: mikroarchitektúra választás/szintézis
		* erőforrás-allokáció, ütemezés, erőforrás-vezérlési állapot összerendelés
* automatizálás előnyei: könnyebb újrafelhasználás, kényszerek magasabb szinten való alkalmazása, több architektúrából választás lehetősége
* példa: *itt volt egy példa, ezt nem másoltam be, itt a* <a href="https://edu.vik.bme.hu/pluginfile.php/110699/mod_resource/content/2/IT04.pdf#page=14">link</a>

##### Logikai szintézis

* RTL kódból logikai szint (strukturális, kapu szintű leírás)
* *input:* RTL kód, cellakönyvtár időzítési és teljesítményadatai
* *output:* struktúrális HDL (csak cellakönyvtárbeli elemeket tartalmaz)
* *kényszerek:* időzítés, terület, teljesítmény (ezek becsültek lesznek csak, mert még nem fizikailag elkészült)
* <img src="/src/iteszk/image-20201004235522272.png" alt="image-20201004235522272" style="zoom:45%;" />
* lépések:
	* HDL beolvasása, optimalizálása
	* hierarchia kifejtése, ha szükséges &rarr; flattening, mivel jobb minőségű megoldás kapható a globális optimalizálással, de sokkal erőforrásigényesebb mint modulonként optimalizálni
	* logikai kifejezések optimalizálása (generic)
		* szerkezetek (összeadó, számláló) felismerése, optimalizálja a logikai kifejezéseket
	* leképzés ASIC makrocellára
		* ha felismert template-et, azt könnyű
		* logikai kifejezést összerakja makrocellákból
			* logikai kifejezésből &rarr; fa építés, amiben csak inverter és NAND kapu van
			* megpróbálja költségfüggvények alapján lefedni

#### Tervezési út &rarr; design flow

<img src="/src/iteszk/image-20201004233151283.png" alt="image-20201004233151283" style="zoom:40%;" />

* zöld nyíl &rarr; a <u>tervezés iteratív</u>
	* CMOS-nál a késleltetés az összekötő hosszaktól függ &rarr; pontos késleltetés csak az elhelyezés és huzalozás után derül ki

## Hardver leíró nyelvek

* **VHDL** &rarr; Amerikai védelmi minisztérium megrendelésére, eredetileg ASIC-ek funkciójának formális dokumentációjára találták ki
	* erősen típusos nyelv (ADA nyelven alapul)
	* nagy projekt létrehozására is alkalmas  &rarr; könyvtárak, hierarchia támogatása
* **SystemVerilog** &rarr; eredetileg logikai szimulációra találták ki, eredeti őse a Verilog volt
* kód példa: <a href="https://edu.vik.bme.hu/pluginfile.php/110699/mod_resource/content/2/IT04.pdf#page=28">link</a>
	* utasítások <u>párhuzamosan</u> hajtódnak végre (sorrend mindegy)
	* kombinációs hálózat &rarr; milyen bemenetre milyen kimenetet adjunk
	* szinkron hálózat &rarr; érzékenységi lista, minek a változására mi történjen
* **SystemC**
	* C++ osztálykönyvtár digitális rendszertervezésre
	* ezeket kell megvalósítani: párhuzamosság, időzítés, késleltetés, port, biz pontos adattípusok
	* előny &rarr; alkalmas lesz lefordított szimulációra, mert szimulációs kernelt is tartalmaz (gyors)
	* fogalmak:
		* **modul** &rarr; egy adott funkcionalitás megvalósítására szolgál, más modulok vagy process-eket foglalhat magába (C++ osztály lesz)
		* **process** &rarr; funkció leírása (modult megvalósító osztály metódusa)
		* **port** &rarr;  logikai jel csatlakozó pontja, iránya (bool-ok)

### Terminológia

* HDL-ek <u>NEM programozási nyelvek</u>
	* hasonló szerkezetek, de a jelentés sokszor eltérő (pl for cikus &rarr; egy elemet tegyünk le x-szer)
* HDL program &rarr; <u>HDL modell</u>
* HDL kód lefordítása &rarr; <u>HDL modell elemzése</u> vagy <u>HDL modell szintézise</u>
* HDL program futtatása &rarr; <u>HDL szimuláció</u>
* FPGA programozása &rarr; <u>FPGA konfigurálása</u>

## Fizikai tervezés (*csak kis részben ZH anyag*)

* eddig: nagyjából technológia független
* kész terv megvalósítása:
	* teljesen kézzel (csak kritikus blokk esetén)
	* előre tervezett cellakönyvtár segítségével
	* programozható logikai eszköz segítségével (pl FPGA)
* **cellakönyvtár**
	* alapvető logikai áramkörök gyűjteménye (alapkapuk, komplex kapuk, flip-flop-ok)
		* különböző meghajtóképességű kapuk &rarr; fizikai méret és fogyasztás is más lesz
	* standard cella magassága rögzített, szélessége változhat
		* azonos <u>magasságú</u> sorok lesznek &rarr; *cellasorok*
		* cella tetején &rarr; táp, cella alján &rarr; föld
* **layout** &rarr; egyszerre ábrázolva az összes réteg (rétegek különböző színekkel)
	* visszafejtés levezetése &rarr; VIDEÓBAN 1:09:10
	* <img src="/src/iteszk/image-20201005094117656.png" alt="image-20201005094117656" style="zoom:50%;" />
* lépések
	* **Floorplan** &rarr; áramkört alkotó blokkok, be és kimenet elhelyezése
		* *Core*: áramköri mag
		* *Pad*: kivezetés
	* **Tápellátás**
		* szimulációval &rarr; statikus és dinamikus áramfelvétel becslése
		* átlagos és maximális fogyasztás ismeretében kell megtervezni
	* **Cellák elhelyezése**
	* **Huzalozás**
	* **Pad-ring elkészítése**
* post-layout szimuláció
	* fizikai tervezés után <u>minden kapu kimeneti terhelése pontosan ismert</u>
	* pontos késleltetési adatok keletkeznek &rarr; újra ellenőrizhető a terv
	* logikai vagy RTL újratervezhető ha nem elég jó

## Félvezető IP

* IP core vagy IP block &rarr; újrafelhasználható egység
* blokk felhasználásáért licenszdíjat kell fizetni

### Soft IP Core

* szintetizálható RTL leírás
	* titkosított forrás, vagy generikkus netlista
* tetszőleges technológiára szintetizálható
* nehézség: olyannak kell a fizikai tervezést és optimalizálást megcsinálni, aki nem ismeri pontosan a belsejét
	* nem lehet garantálni a méretet, késleltetést, fogyasztást

### Hard IP Core

* fizikai tervezés végeredménye &rarr; azaz layout
* adott félvezető gyártó adott technológiájához kötődik
	* méret, késleltetés és fogyasztás garantált!