# IT eszközök technológiája - 5. előadás</br>Memóriák

<a class="md-button" style="text-align: center" href="../5_ea.pdf" download>Letöltés PDF-ként</a>

## Áttekintés

* Félvezető memóriák alapfogalmai
	* M x N memória
		* M db N bit széles memóriaszó
		* M általában 2 hatvány, N = 1, 2, 4, 8, ...
	* Tradicionális felosztás
		* **ROM** (csak olvasható) vs **RAM** (írható-olvasható)
		* mostanra elmosódtak a határok
	* Új fajta csoportosítás
		* <u>írhatóság és adatmegőrzési idő szerint</u> (így jobb!)
	* <img src="/src/iteszk/image-20201012103028923.png" alt="image-20201012103028923" style="zoom:40%;" />
	* Tipikus memória struktúra
		* memória mátrix
		* 1 sorban &rarr; **szóvonallal** aktiváljuk
		* aktivált cellák &rarr; **bitvonalra** másolják tartalmukat
		* cím másik részével a bitvonalak közül választunk
	* néhány 100mV a logikai szint távolsága &rarr; érzékelő erősítő állítja helyre a rail-to-rail jelet
	* még 2 hierarchiai szint &rarr; **blokk csoport** és azon belül **blokk**
		* hozzáférés hierarchikus
		* kívülről **banknak** látszik

## RAM memóriák

### Statikus RAM

<img src="/src/iteszk/image-20201012105544238.png" alt="image-20201012105544238" style="zoom:35%;" />

* cellát 6 tranzisztor alkotja
* 2 bitvonal van, ellentétes logikával (differenciális logika)
* 2 keresztbe csatolt inverter &rarr; tárolásért felel
* beírás/olvasás 1-1 tranzisztoron keresztül (mint az SR latch-nél)
* M3 és M4 tranzisztorok &rarr; <u>elérési tranzisztorok</u>

#### Dual port SRAM

* 2 szóvonal van (wordA, wordB)
* olvasásnál egy időben két cellából is lehetséges
* íráshoz viszont mindkettő szóvonal kell (továbbra is csak 1 írás lehet egyszerre)

### Dinamikus RAM

<img src="/src/iteszk/image-20201012110500274.png" alt="image-20201012110500274" style="zoom:35%;" />

* nagyobb kapacitású, jóval lassabb a statikus ramnál
* 3 dimenziós struktúra
	* **Stack** kapacitás &rarr; tranzisztor felett készül el egy vékonyréteg kapacitás
	* **Árok** kapacitás &rarr; tranzisztor mellett árkot marnak a szilíciumba, ebbe alakítják ki a tároló kapacitást
* információt a C~s~ kapacitás tárolja, M~1~ kapcsol a bitvonalra
* tároló kapacitás nagyon pici
* **írás**
	* bitvonalra logikai értéket rakunk
	* szóvonallal aktiváljuk &rarr; tároló kapacitás vagy kisül vagy feltölt
* **olvasás**
	* bitvonalat a tápfeszültség felére előtöltjük
	* szóvonal aktiválódik &rarr; rákapcsolódik a bitvonalra a tároló kapacitás töltése
	* <img src="/src/iteszk/image-20201012111504107.png" alt="image-20201012111504107" style="zoom:35%;" />
	* az érzékelő ezt állítja helyre
	* olvasás <u>destruktív</u>, a kiolvasott értéket vissza kell írni!
* **frissítés** &rarr; erre van szükség, mert a <u>töltés szivárog</u>
	* +30 fok hőmérséklet &rarr; 10x-es szivárgás
	* egyszerre 1 sort frissítenek &rarr; t~RC~ = 100-200ns
	* **burst refresh**: összes sort egyszerre frissíti (csökkenti a memória sávszélességét :( )
	* **distributed (hidden) refresh**: számláló figyeli az utolsó frissített sort, mindig a soron következő kerül frissítésre

#### Beágyazott DRAM

<img src="/src/iteszk/image-20201012120403084.png" alt="image-20201012120403084" style="zoom:35%;" />

* 3 tranzisztorod dinamikus RAM technológia
	* M1-M2 tranzisztor szórt kapacitása tárol
	* M1 szolgál a beírásra

## Tartalommal címezhető memóriák (CAM)

<img src="/src/iteszk/image-20201012120824754.png" alt="image-20201012120824754" style="zoom:38%;" />

* fordított feladat &rarr; <u>adathoz</u> milyen <u>cím</u> tartozik
* 1 órajel alatt a keresett információ címének előállítása
	* *search data register* -t hasonlítja össze a párhuzamosan tárolt információval
	* 1 match vonal lesz csak aktív &rarr; innen van meg a cím
* használat pl: virtuális page cím - fizikai page cím párosítása
* <img src="/src/iteszk/image-20201012121935323.png" alt="image-20201012121935323" style="zoom:35%;" />
* megvalósítás
	* SRAM kiegészítése 10 tranzisztoros CAM cellává
	* keresett bit a bitvonalra
	* ha megegyezik a tárolt bittel &rarr; nincs áramút a match line és a föld között
	* ha nem egyezik meg a tárolt bittel &rarr; áramút alakul ki
		* 
		* keresés teljes soron egyszerre
	* negatívum: nagy fogyasztású

## ROM memóriák

### A maszk programozott ROM

* információ a gyártáskor belekerül (maszk programozott)
* nagy sorozatú gyártásnál éri csak meg &rarr; egy bit kis területet foglal &rarr; bitre vetített ár kedvező

#### Pszeudo NMOS kapu

* pMOS mindig nyitott &rarr; egy ellenállással modellezhető
* így is működik, csak az alacsony szint nem 0 hanem csak ahhoz közeli kicsi érték
* negatívum: statikus fogyasztása van, ha a kimenet 0
* pozitívum: 2n helyett n+1 tranzisztor elég

#### Maszk programozott NOR ROM

<img src="/src/iteszk/image-20201012154444418.png" alt="image-20201012154444418" style="zoom:40%;" />

* elemi cella egy nMOS tranzisztor
* információ = jelen van e egy adott tranzisztor elektromos szempontból vagy sem
* aktivált tranzisztor &rarr; bitvonalat a földre köti
* adott bitvonalra nézve = sokbemenetű pszeudo nMOS NOR kapu
* bemenetek közül egy lehet csak aktív
	* tranzisztot vezet = 0
	* egyébként = 1

#### Maszk programozott NAND ROM

<img src="/src/iteszk/image-20201012154501255.png" alt="image-20201012154501255" style="zoom:40%;" />

* tranzisztorokat sorba kapcsoljuk
* információ = adott helyen a tranzisztort rövidre zártuk e fémezéssel vagy sem
* kiolvasáskor &rarr; minden szóvonalat aktiválunk, kivéve a kérdéses sort
* adott helyen nincs tranzisztor &rarr; kimenet = 0 (NAND összes további tranzisztora vagy vezet vagy rövidzárt)
* adott helyen van tranzisztor &rarr; kimenet = 1

## OTP ROM

* firmware
* on-chip konfiguráció kialakítása
	* pl kalibrálási konstansok, titkosítási kulcsok, chip azonosítók
* programozható logikai eszközök
* információtároló elem &rarr; fuse vagy antifuse
	* **fuse**: rövidzár, kiégetés után nem vezet
	* **antifuse**: kiégetés után vezet, égetés nélkül szakadást okoz.

## EEPROM

* információt tárolja &rarr; MOS tranzisztor <u>küszöbfeszültsége</u>
	* küszöbfeszültség változtatásával programozható
	* kiolvasás
		* adott tranzisztor vezet vagy nem vezet (SLC)
		* adott feszültség mellett jól megkülönböztethető áramok folynak (MLC, TLC, QLC)
* **küszöbfeszültség**: az a gate-source közé kapcsolt feszültség, amikor a vezetőképesség inverziós csatorna létrejön
	* függ a szigetelőben lévő töltésektől
* konstrukciók
	* **lebegő gate** &rarr; sehova nem kötött poli-Si
	* **többrétegű szigetelő anyagok határfelülete**, töltéscsapdákat tartalmaz
* programozás fizikai elve &rarr; elektronok mozgatása töltés-tároló eszközre ami egy vékony szigetelővel van elválasztva
	* **lavina letörés**: nagymennyiségű, nagyenergiájú forró elektron jelenik meg, aminek energiája elég hogy áthaladjon a szigetelőn
	* **alagút jelenség**: megfelelő térerősség hatására egy keskeny szigetelőn biztos valószínűséggel keresztülhalad az elektron
* negatívum: ha az elektron a <u>szigetelőben ragad</u> &rarr; nehezebben változtatható küszöbfeszültség &rarr; tranzisztor elhasználódik
* régi technológiák:
	* **EPROM**: programozás lavinaletöréssel, törlés UV fény segítségével
	* **EEPROM**: programozás-törlés alagútjelenséggel

## FLASH EEPROM

* legsikeresebb memóriatípus jelenleg
* programozás: lavinaletöréssel vagy alagútjelenség segítségével
* törlés: alagútjelenség segítségével (nagyobb blokkonként történik)
* <img src="/src/iteszk/image-20201012161113967.png" alt="image-20201012161113967" style="zoom:45%;" />
* lebegő gate-et vékony oxid választja el a szubsztráttól
* 2 elrendezés
	* NOR elrendezés
		* nagy teljesítménnyel kell programozni
		* lassú a programozás és törlés
		* gyors az olvasás
		* <u>program memória</u>
	* NAND elrendezés
		* kis cellaméret, nagy sűrűség, kis teljesítménnyel programozható
		* gyorsabb törlés
		* <u>háttértárolás</u>

## Új memória architektúrák