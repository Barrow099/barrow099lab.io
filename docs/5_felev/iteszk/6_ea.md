# IT eszközök technológiája - 6. előadás</br>ASIC

<a class="md-button" style="text-align: center" href="../6_ea.pdf" download>Letöltés PDF-ként</a>

## Integrált áramkörök csoportosítása

### Katalógus áramkörök (COTS)

* széleskörű használat
* felhasználó ≠ tervező
* nagyon sokat gyártanak, pl 555 időzítő
* probléma:
	* ebből összeállított rendszer nem optimális (mert ezek általános célúak), késleltetése és fogyasztása nem lesz optimális
	* nagyméretű lesz a rendszer

### ASIC

* adott speciális célra készülnek
* ok: katalógus áramkörrel nehezen megoldható, másolást nehezítik ezzel
* ha kevés az egyedi lépés &rarr; jobb az ár
* ASIC áramkörök <u>részben előre gyártottak, részben előre tervezettek</u>

#### SoC (System on a chip)

* egy teljes rendszer megvalósítása 1 chipen
* digitális, analóg részek, tápellátás
* előny: kisebb késleltetés és fogyasztás, kisebb fizikai méret, olcsóbb gyártás
* hátrány: bonyolult, nagyméretű nagyobb eséllyel hibás
* gyakran használt megoldás (pl mobilokban)

## ASIC kategóriák

* **Custom ASIC** (full custom)
	* tervezés és költséges, drágák a maszkot, csak nagy példányszámban éri meg
* **Semi-custom ASIC**
	* részben vagy teljesen előre gyártottak (konfiguráló réteg maszktervezése elég vagy teljes mértékben sw úton)
	* vagy előre tervezettek (áramkört struktúrált, maszk minták nagy része előre tervezett)
	* előre tervezett &rarr; Standard cell
	* részben előre gyártott &rarr; Gate array
	* előre programozott &rarr; PLD
		* CPLD, FPGA

### Standard cellás ASIC

* maszk minták előre tervezettek &rarr; cellák (magassága adott, szélesség változó)

* táp és föld rögzített

* standard könyvtárat a félvezető gyár fejleszti és bocsátja rendelkezésre

* **tervezés** &rarr; teljes mértékben automatizált

	* cellák sorban, majd huzalozva
	* probléma: összes maszkot le kell gyártani &rarr; nagyon drága!

	* félvezetőgyár adja:
		* standard cellakészletet, logikai leírás és absztrakt fizikai terv
		* cellakészlet időzítési és fogyasztási adatai &rarr; logikai szimulációhoz
		* fizikai és elektromos tervezési szabályok
		* visszafejtési szabályok (kapu kimenetét mekkora kapacitás terheli)
		* áramköri modellek az áramköri szimulációkhoz
	* minden maszkot le kell gyártani, viszont a cellakönyvtár elemei előre tervezettek, kipróbáltak és karakterizáltak

### Gate array

* fémezés kivételével előre gyártott
	* 2 elrendezés:
		* Sea of gates: n ps pMOS tranzisztorok előre meghatározott mintában
		* Struktúrált ASIC esetén FPGA-hoz hasonló logikai blokkok
* áramkör végleges funkciójának kialakítása
	* tranzisztor összeköttetésével kapuk, majd kapuk összeköttetésével végleges funkciók
	* logikai kapuk összeköttetése előre tervezett
	* tervezés itt is főleg automatikus
* előny: olcsóbb &rarr; maszkok száma kevesebb, 
* hátrány: terület kihasználás nem lesz optimális, huzalozás nem optimális &rarr; nagyobb késleltetés

### Programozható logikai eszközök

* teljes mértékben előre gyártott (logikai funkciók és összeköttetések)
* konfigurálás elektromosan
	* Volatile &rarr; konfigurálás statikus RAM végzi, indításkor újra be kell tölteni, működés közben újrakonfigurálható
	* Non-volatile &rarr; maszk programozott, flash EEPROM vagy Antifuse tárolja (PLICE vagy ViaLink)
		* Antifuse &rarr; kisebb késleltetés, de nem újrakonfigurálható

<img src="/src/iteszk/image-20201020182552170.png" alt="image-20201020182552170" style="zoom:40%;" />

#### PLA/PAL

* PLA - logikai függvények megvalósítása &rarr; igazságtáblázat megadásával lehetett "programozni" (első hardver leíró nyelvek)
* PAL - már regisztereket is tartalmazott &rarr; sorrendi hálózat is megvalósítható

#### CPLD

* PLA/PAL utódja
* feladata: segéd logika előállítása (busz illesztése, segéd logikai jelek generálása)
* makrocellákból állnak &rarr;  ÉS mátrix (logikai fv-ek előállítására)
	* programozható típusú flip-flip

#### FPGA

* Field programmable gate array
* általános célú, újrakonfigurálható eszközök &rarr; tetszőleges logikai funkció megvalósítható velük!
* erőforrások
	* **konfigurálható logikai blokkok**
		* granularitás &rarr; mennyire összetett funkciót valósít meg (modern FPGA finom granularitású - egyszerű funkció de sok BLE)
	* **konfigurálható I/O blokkok**
	* **konfigurálható huzalozási erőforrások**
	* **konfigurációs memória** (általában SRAM), **speciális célú erőforrások** (blokk RAM - nagymennyiségű statikus RAM)
* terület 90-95% konfigurációs erőforrások
* kapcsolómátrixok &rarr; csak a lehetséges összeköttetések egy részét valósítja meg

##### DSP slice

* mini szorzóáramkört tartalmaznak
* gyakran szorzó akumlátort is

##### SoPC - System on a Programmable Chip

* egy vagy több mikroprocesszor mag, hard IP formájában
* DRAM vezérlő
* külső, nagysebességű kommunikáció

#### Struktúrális ASIC

* átmenet FPGA és standard cellás ASIC között
* modern FPGA-hoz hasonló hard IP blokkok, köztük maszk konfigurálható logikai blokkok és összeköttetések
* nincs konfigurációs RAM
* terület spórolás &rarr; késleltetés is csökken
* struktúrális ASIC szolgáltatás
	* FPGA tervből ASIC (35% költségmegtakarítás, de nem újraprogramozható)

