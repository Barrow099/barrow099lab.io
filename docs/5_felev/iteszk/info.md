# IT eszközök technológiája (VIEEAC00)

**Honlap:** VIK Moodle

!!! warning "Távoktatás"
	Távoktatás során az előadások aszinkron módon Streams-ről lesznek elérhetőek. A laborok jelenléti formában lesznek megtartva, és az 5 laborból csak 4-et kötelező teljesíteni, illetve 1 pótolható. Összesen 10 db kisházi lesz a tárgyból a 3. héttől kezdve, ezek határideje mindig a következő hét hétfő 4:00. A kisházik 40%-nak kitöltése az aláírás feltétele, és a házikkal szerzett pontszám 20%-ban beleszámít a félév végi jegybe!

## Adatok

* Kredit: 4
* Tanszék: EET
* ZH: 1db
* Házi: 10 kisházi
* Vizsga: nincs

## Tárgy teljesítése

* A kisházik 40%-nak kitöltése!
* Félév végi jegy = 0.8 * Zh pont + 0.2 * kisházi pontok
	* 0 - 0 pont: **1**-es
	* 0 - 0 pont: **2**-es
	* 0 - 0 pont: **3**-as
	* 0 - 0 pont: **4**-es
	* 0 - 0 pont: **5**-ös

## Előadás

| Hét  | Dátum      | Előadás                                                      | Jelenlét | Tudom |
| ---- | ---------- | ------------------------------------------------------------ | -------- | ----- |
| 1    | 2020.09.07 | IC és MOS tranzisztor                                        |          |       |
| 2    | 2020.09.14 | IC és MOS tranzisztor, Digitális logika megvalósítása        |          |       |
| 3    | 2020.09.21 | Digitális logika megvalósítása                               |          |       |
| 4    | 2020.09.28 | Digitális rendszertervezés                                   |          |       |
| 5    | 2020.10.5  | Memória technológiák                                         |          |       |
| 6    | 2020.10.12 | ASIC és Programozható logikai eszközök                       |          |       |
| 7    | 2020.10.19 | Hogyan illesztjük a digitális IC-t egy rendszerbe?           |          |       |
| 8    | 2020.10.26 | Érzékelés minden szinten                                     |          |       |
| 9    | 2020.11.02 | Megjelenítő eszközök technológiájával                        |          |       |
| 10   | 2020.11.09 | Átjárás analóg és a digitális világ közt (AD/DA átalakítók)  |          |       |
| 11   | 2020.11.16 | Teljesítmény és hőmérsékleti problémák                       |          |       |
| 12   | 2020.11.23 | Mágneses adattárolás (2020 - számonkérésnek nem lesz része)  |          |       |
| 13   | 2020.11.30 | Nyomtatott huzalozású lemez készítése, milyen alkatrészek vannak még, és hogy vannak azok rögzítve |          |       |

## Labor

1. CMOS inverter és kapu szimulációja
2. Modellezés hardver leíró nyelven (VHDL)
3. Egyszerű digitális modul elkészítése FPGA-n
4. Soft processzoros számítógép FPGA-n
5. Hőmérsékletmérés FPGA-val, esettanulmány és kísérletezés

## Zárthelyi

* 1 darab ZH és 1 darab pótlási lehetőség a félév során
* elektronikus zh
* teszt feladatok, rövid példák és összetett példák (egyszerűbb számolás)

## Házi feladat

* 10 db kisházi a félév során.