# IT eszközök technológiája - 3. előadás

<a class="md-button" style="text-align: center" href="../3_ea.pdf" download>Letöltés PDF-ként</a>

## Komplex kapuk

* Tranzisztor szinten tudunk bonyolultabb logikai függvényeket megvalósítani
* Általában max 4 bemenettel, az ÉS illetve VAGY függvényeket kombináljuk
* pl:
	* OAI21 &rarr; Y = komplementer( (A + B) C )
	* AOI22 &rarr; Y = komplementer( AB + CD )
* Általában <u>n bemenet és/vagy kombinációja 2n tranzisztor segítségével megvalósítható</u>
	* bemenet ponált és negált változata is rendelkezésre áll
* Többszintű realizációhoz képest
	* késleltetése kedvezőbb
	* kevesebb tranzisztort tartalmaz
	* hazárdmentes

### Példa: Y = komplementer( (A + B) C )

* pull-down network tervezése:
	* minden 0 értékhez a <u>kimenet és föld</u> között kell kapcsolat
	* függvényben szereplő <u>összegeknek párhuzamosan</u>, <u>szorzatoknak sorba kapcsoltan</u> kell szerepelniük
	* <img src="/src/iteszk/image-20200928090734580.png" alt="image-20200928090734580" style="zoom:25%;" />
* pull-up network tervezése:
	* minden 1 értékhez a <u>kimenet és a távfeszültség</u> között kell kapcsolat
	* Bool algebrával kiszámolt érték: <img src="/src/iteszk/image-20200928091031035.png" alt="image-20200928091031035" style="zoom:18%;" />
	* Reciprok hálózat megközelítésből: pull-down ellenkezőjére kötünk mindent (sorosból párhuzamos, párhuzamosból soros)
* megoldás:
	* <img src="/src/iteszk/image-20200928091439387.png" alt="image-20200928091439387" style="zoom:40%;" />

* ha kapuból csinálnánk:
	* nagyobb kérleltetése lenne, több tranzisztor kéne

### Példa: teljes összeadó

* kritikus út &rarr; **carry** (az átvitt érték mindig kell a következő helyiértékhez)
	* megodás pl: előbb számítunk átvitelt, aztán összeget
	* C~OUT~ = AB + C(A+B) &rarr; így a C-t a lehető legkevesebb helyre kell bekötni
	* S = ABC + (A + B + C) kompl(C~OUT~)

## CMOS transzfer kapu

* jelfolyam útjába helyezett kapcsoló
* n és p típusú tranzisztor kapcsolásából áll, amikre ellentétes jelet kötünk
	* C = 0 &rarr; szakadás
	* C = 1 &rarr; vezetés
* kiválasztók-ra nagyon jó (kétbemenetű multiplexer)
	* <img src="/src/iteszk/image-20200928092502711.png" alt="image-20200928092502711" style="zoom:40%;" />
	* pozitívum: komplex kapus 8 szükséges tranzisztor helyett 4 is elég!

## Órajel vezérelt CMOS

* Háromállapotú (tri-state kapu)
	* EN = 0 &rarr; a kimenet lebeg (nagyimpedanciás)
	* EN = 1 &rarr; a kimenet a bemenet inverze
	* <img src="/src/iteszk/image-20200928093452859.png" alt="image-20200928093452859" style="zoom:35%;" />

## CMOS tárolók

* **Latch** &rarr; <u>szintvezérelt</u>
	* EN = 1 &rarr; átlátszó
	* bemeneti változás a kimenetre jut (kis késleltetés után)
* **Flip-flip** &rarr; <u>élvezérelt</u>
	* beírás az órajel fel vagy lefutó élére történik

### Tárolás alapelve

* két állapotú (bistabli) áramkör alapja: <u>két gyűrűbe kapcsolt inverter</u>
	* <img src="/src/iteszk/image-20200928100318718.png" alt="image-20200928100318718" style="zoom:45%;" />
* **D-latch**
	* komplex kapukkal, 12 tranzisztorból
		<img src="/src/iteszk/image-20200928100618424.png" alt="image-20200928100618424" style="zoom:45%;" />
	* transzfer kapuval, 8 tranzisztorból
		<img src="/src/iteszk/image-20200928100859652.png" alt="image-20200928100859652" style="zoom:45%;" />
		* EN = 1, bal kapu nyitva, D = Q, visszacsatoló (lenti) kapu zár
		* EN = 0, bal kapu csukva (nincs írás), visszacsatoló ág él &rarr; létrejön a két gyűrűbe kapcsolt inverter
* **D-flipflop**
	* master-slave flip-flop két sorbakötött, ellenütemű órajellel vezérelt latch
	* CP = alacsony szint, első tároló átlátszó
	* CP = felfutó él, master nem átlátszó, tartalom slave-be íródik
	* <img src="/src/iteszk/image-20200928102947584.png" alt="image-20200928102947584" style="zoom:40%;" />
	* **t~setup~** &rarr; órajel **aktív éle előtt** a mintavételezett adatnak <u>stabilnak kell lennie</u>
		* t~setup~ > t~PGTG~ + 2t~PGINV~ (1 transzfer kapu és 2 inverter)
	* **t~hold~** &rarr; órajel **aktív éle után** ennyi ideig <u>nem szabad megváltoznia</u>
		* t~HOLD~ > t~PGTG~ (míg a master transzfer kapuja elzáródik)
	* aszinkron *clear* és *set* is megvalósítható &rarr; kétbemenetű NAND kapukkal

## Nagysebességű CMOS logika

* statikus CMOS logika késleltetée: **t~pd~ ~ CV / I**
	* gyorsítás <u>kapacitás csökkentésével</u>, vagy az <u>áram növelésével</u> (közben méret is csökkenjen)
	* logikai szint távolságának csökkentése &rarr; SCL = source-coupled logic
	* differenciális logikai &rarr; két feszültségszint különbsgégének előjele adja a logikai értéket
	* **szórt kapacitás** kihasználása logikai szint **ideiglenes tárolására**
		* területet nyerünk, kapacitást csökkentünk

### CMOS dominó logika

* csak pull-down network van
* kimenetet terhelő szórt kapacitást előtöltjük
* <img src="/src/iteszk/image-20200928105320870.png" alt="image-20200928105320870" style="zoom:33%;" />
	* Φ = 0 &rarr; előtöltés tápfeszültségre
	* Φ = 1 &rarr; kiértékelés (pull-down network az input alapján vagy lehúzza a kimenetet, vagy békén hagyja)
* előnyök:
	* N bemenetű függvényhez N+2 tranzisztor szükséges
	* gyorsabb &rarr; előző fokozatot kisebb kapacitás terheli
* hátrányok &rarr; kapuk összekapcsolásakor vigyázni kell
	* kiértékeléskor egy rövid ideig 1 van akkor is, ha 0-nak kellene lennie (ez kinyitja a következő kapu tranzisztorát &rarr; töltés sérülés lesz)
	* megoldás: inverteren keresztül kötjük a kimenetet a következő kapu bemenetére
		<img src="/src/iteszk/image-20200928111145364.png" alt="image-20200928111145364" style="zoom:45%;" />
* **dominó logika** 1,5x gyorsabb a CMOS-nál (csak pull-down network dolgozik benne)

### Dinamikus D flip-flop

* 2db órajellel vezérelt invertert összekapcsolunk, ezeket ellentétesen vezéreljük
* info &rarr; master és slave közti szórt kapacitásban tárolódik
* következmény &rarr; van egy minimális órajel, amit használni kell (ha annál kisebb használunk, akkor elszivárog a kapacitás annyi idő alatt)

## Az adatút elemei (*már nem zh anyag!*)

### Összeadó

#### Ripple carry adder

* több bites számok összeadása &rarr; sorba kapcsolt teljes összeadók
* nem lesz túl gyors (egymásra várnak) &rarr; utolsó carry elkészülése + utolsó összeg elkészülése
* kritikus út = carry terjedése
* egy bizonyos bitszám felett túl lassú lesz
* <img src="/src/iteszk/image-20200929085802972.png" alt="image-20200929085802972" style="zoom:33%;" />
* gyorsítása:
	* generate &rarr; G = AB
	* propagate &rarr; P = A ⊕ B

#### Carry skip adder

* számítsuk ki egy blokkra a propagate-t és ezzel vezéreljünk egy multiplexert, ami vagy az előző fokozat átvitelét vagy a generált átvitelt teszi a kimenetre
* propagate = és kapu &rarr; könnyen kiszámítható
* késleltetés: O(N/K)

#### Carry-look-ahead

* fokozatonként történik a carry gyors kiszámítása, minden fokozatra P és G számítása
* késleltetés: P és G kiszámítása (részben párhuzamosan történik)

#### Carry-select

* fokozatonként kiszámolja az összeget átvitel és átvitel nélkül, amiből az előző fokozat átvitele választja ki a végeredményt egy multiplexer segítségével

### Funnel shifter

### Kombinációs szorzó