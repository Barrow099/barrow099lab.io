# Mobil- és webes szoftverfejlesztés - 2. előadás

<a class="md-button" style="text-align: center" href="../2_ea.pdf" download>Letöltés PDF-ként</a>

## Kotlinos tudnivalók

* JVM byte kódra fordul a Kotlin is
* Java és Kotlin kód keverhető (automatikus konverzió van)
* Java API-k, keretrendszerek és könyvtárak ugyan úgy használhatóak
* null-safety &rarr; kevesebb NullPointerException
* szépen olvasható a kódja

### Kotlin (csak címszavak szintjén)

* val & var

* stringek ($ jel használata)

* null érték (Int? &rarr; null is lehet)

	* `var x: List<String?> = listof(null, null, null)` &rarr; *egy lista, aminek String elemei lehetnek null-ok*
	* `var x: List<String>? = null` &rarr; *egy lista, ami lehet null, de elemei nem*

* null tesztelés

	* ```kotlin
		var nullTest : Int? = null
		nullTest?.inc() //inc csak akkor hívódik meg, ha nullTest NEM null
		```

	* Elvis operátor:

		```kotlin
		var x: Int? = 4
		var y = x?.toString) ?: "" //ha x értéke null, akkor y értéke egy üres string lesz
		```

	* Double bang &rarr; NullPointerException kikényszerítése

		```kotlin
		var x: Int? = null
		x!!.toString() //itt kotlin.KotlinNullPointerException lesz dobva
		```

* függvények

	* függvény szintaxis

		```kotlin
		fun add(a: Int, b: Int): Int {
			return a + b
		}
		```

	* kifejezés törzs, visszatérési típus se kell ha egyértelmű

		```kotlin
		fun add(a: Int, b: Int) = a + b
		```

	* érték nélküli visszatérés &rarr; `Unit`, de ez elhagyható

* osztályok

	* ```kotlin
		//constructor szó elhagyható
		class Car constructor(val type: String) { //zárójelben primary constructor paraméterekkel
		    val typeUpper = type.toUpperCase() //primary constructor tagváltozóira lehet hivatkozni
		    init { //primary constructor inicializáló blokkja
		        Log.d("TAG_DEMO", "Car created: ${type}")
		    }
		    
		    constructor(type: String, model: String): this(type) { //secondary constructor
		        Log.d("TAG_DEMO!", "Car model: ${model}")
		    }
		}
		val car = Car("Toyota") //példányosítás, NEM kell new kulcsszó!
		```

		* az elsődleges konstruktor paraméterei bekerülnek property-ként az osztályba (ha előttük van, hogy var vagy val)

	* leszármaztatás:

		```kotlin
		open class Item(price: Int) { //open -> így írható felül (alapból final)
			open fun calculatePrice() {}
			fun load() {}
		}
		
		class SpecialItem(price: Int) : Item(price) { //öröklés kettősponttal
			final override fun calculatePrice() {} //final -> azaz később nem lehet már felülírni
		}
		```

* property-k

	```kotlin
	class Car {
		var type: String? = null
			set(type) {
				Log.d("TAG_CAR", "type SET")
				field = type
			}
	}
	```

	

## Android projekt felépítése, fordítása

### Felépítés

* **MainActivity.kt** &rarr; kotlin forrásfájl
* **erőforrások** &rarr; res mappában, R osztály ebből generálódik
	* *drawable* &rarr; képek
	* *layout* &rarr; elrendezések
	* *mipmap* &rarr; ikonok
	* *values* &rarr; pl színek, szövegek, stílusok
	* (new &rarr; Android Resources Directory &rarr; itt van sokféle erőforrás típus)
	* könyvtárak után <u>minősítők írhatók</u> (kötőjel után, pl values-hu)
		* mely tulajdonság esetén vegye ebből a könyvtárból az erőforrásokat
	* FONTOS: res mappába NE legyen hiba (mert a teljes R.java-t elrontja :( )
* **manifest fájl** (manifests/AndroidManifes.xml) &rarr; alkalmazás metainformációit tartalmazza
	* ikonja, neve, verziója, jogosultságok
	* alkalmazás komponenseinek felsorolása, plusz ki implementálja
	* alkalmazás telepítésekor ellenőrzi a rendszer
	* LANCHER kategória &rarr; bekerült az alkalmazások közé
	* MAIN kategória &rarr; a fő activity-t jelöli

### Fordítás (*zh-n szeretik kérdezni*)

* végeredménye *.apk*
	* tömörített állomány
* <img src="/src/mobweb/image-20200929113229408.png" alt="image-20200929113229408" style="zoom:45%;" />

## Android alkalmazás komponensek

* 4 komponensből építkezhetünk
	* **Activity** &rarr; egy képernyő (nézet, UI), `android.app.Activity` osztályból származik le
	* **Service** &rarr; kódot futtat a háttérben, magában nincs felhasználói felülete, `android.app.Service` osztályból származik le
	* **Content Provider** &rarr; alkalmazás egymás közti adatmegosztását támogatja (névjegyzék, naptár hozzáférés, akár fájlrendszerből, adatbázisból, webről), `android.content.ContentProvider` osztályból származik le
	* **Broadcast Receiver** &rarr; rendszer meg egyéb broadcast-ekre feliratkozás (pl kikapcsolt a képernyő, hívás jött be, elkészült egy fotó, stb), alkalmazás is indíthat saját broadcast-et, nincs saját felületük, `android.content.BroadcastReceiver` osztályból származik le
* komponensek külön belépési ponttal rendelkeznek
	* ikonra nyomva, vagy broadcast-en keresztül is pl megindulhat
	* több main activity is lehet &rarr; 2 ikon lesz, 2 belépési ponttal

## Activity életciklus

* tipikusan egy képernyő (UI), ablak
* egy alkalmazás = több <u>lazán csatolt</u> activity (de létezik egy <u>fő activity</u>, ahonnan indul)

### Életciklus részei

<img src="/src/mobweb/image-20200929113328504.png" alt="image-20200929113328504" style="zoom:50%;" />

* Életciklus:
	* Activity elindul &rarr; `onCreate()`
	* Láthatóvá válik &rarr; `onStrart()` &rarr; *pl BroadcastReceiver-re feliratkozás*
	* Fókuszt kap (kattinthatóvá válnak rajta a dolgok) &rarr; `onResume()` &rarr; *pl játék zene itt indítható*
	* Másik activity kerül előtérbe &rarr; `onPause()` &rarr; *pl zene itt állítandó le*
	* Activity egyáltalán nem látszik &rarr; `onStop()`
	* Teljesen bezáródik &rarr; `onDestroy()` &rarr; *minden maradék erőforrás felszabadítása*
	* A felhasználó az activity-re navikgál &rarr; `onRestart()` &rarr; Stop-ból Start-ba kerül
* `Paused` vagy `Stopped` állapotban <u>bármikor leállíthatja</u> a rendszer az activity-t memória-felszabadítás céljából
* mindegyik metódus felüldefiniálásakor meg kell hívni az őst (`super.onCreate()`)
* FONTOS: A képernyő <u>elforgatásával</u> az adott activity <u>megszűnik,</u> majd egy <u>új activity indul</u>
	* azaz ha `var score = 10` értékben akarjuk a rekordot tárolni, egy képernyő elforgatáskor elveszik az

### Activity-ből másik activity indítása

* ```kotlin
	fun runSecondActivity() {
	    val myIntent: Intent = Intent()
	    myIntent.setClass(this@MainActivity, SecondActivity::class.java)
	   	//adat átadása
	    myIntent.putEctra("KEY_DATA", "Hi there!")
	    startActivity(myIntent)
	}
	```

	