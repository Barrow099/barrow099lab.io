# Mobil- és webes szoftverek - 1. labor

## Android alapok

### Fordítás menete Android platformon

* /src &rarr; forráskód
* /res &rarr; UI XML kódja
* **R.java** állomány (ez generált, bele se tudunk nyúlni) &rarr; összeköti az erőforrás állományt a forráskóddal
* fordítás eredménye &rarr; **APK állomány**
* **Manifest állomány** &rarr; hozzáférési jogosultságokat tartalmazza (Internet elérés, szenzorok, stb)
* forráskód + erőforrások + külső könyvtárak &rarr; *fordító* &rarr; **ART (Dalvik) virtuális gép** <u>gépi kódja</u>
* gépi kód + erőforrások &rarr; nem aláírt APK állomány
* aláírás után telepíthető a készülékre az APK
* Gradle &rarr; build rendszer, Android Studio ezt használja a lépések végrehajtásához

### Billentyűkombinációk

* CTRL + ALT + L: Kódformázás
* CTRL + SPACE: Kódkiegészítés
* SHIFT + F6 Átnevezés (Mindenhol)
* F2: A következő error-ra ugrik. Ha nincs error, akkor warningra.
* CTRL + P: Paraméterek mutatása
* ALT + INSERT: Metódus generálása
* CTRL + O: Metódus felüldefiniálása
* CTRL + F9: Fordítás
* SHIFT + F10: Fordítás és futtatás
* SHIFT SHIFT: Keresés mindenhol
* CTRL + N: Keresés osztályokban
* CTRL + SHIFT + N: Keresés fájlokban
* CTRL + ALT + SHIFT + N: Keresés szimbólumokban (például függvények, property-k)
* CTRL + SHIFT + A: Keresés a beállításokban, kiadható parancsokban.