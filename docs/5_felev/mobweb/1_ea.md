# Mobil- és webes szoftverfejlesztés - 1. előadás

<a class="md-button" style="text-align: center" href="../1_ea.pdf" download>Letöltés PDF-ként</a>

## Bevezetés

### Mobil piac szereplői

* Hálózati operátorok
	* kiépíti, karbantartja a hálózatot
	* biztosítja a készülékek közötti kommunikációt
	* pl: T-mobil, telenor, vodafon
* Szolgáltatók
	* pl hanghívás, videóhívás
* Készülékgyártól
	* pl: Apple, Samsung, Huawei, Xaomi, ...
* Felhasználók

### Mobilkészülék funkciói

* kb mindent tud

### Platformok

* **Symbian OS**
	* első okostelefon platform (kifejezetten mobilra készült)
	* nem volt store
	* c++ban lehetett fejleszteni
	* nokiák futtatták főleg
* **Java ME**
* **Python**
* **Windows Mobile**
	* Windows CE-re épült
* **Windows Phone**
	* háttérbe került, leállították
* **Maemo**, **Meego**, **Tizen**, stb
	* nem futottak be
* **iOS**
	* Objective C, Swift
	* csak OS X-en lehet rá fejleszteni
	* picit prémium
	* tableten, órán is elérhető
* **Android**
	* nyílt forráskódú, ingyenes hardver

### Android

* google brand-et épített rá (többen ismerik az OS-t mint a telefon márkát)
* egyik legelterjedtebb
* Google áll mögötte
* Android eszközök
	* sok helyen elérhető &rarr; telefonon, tableten, autóba, okosórán, stb

### Android története

* 2005-ben kezdték fejleszteni, 2008-ban jött ki, közben Google megvette

## Android platform szerkezete

<img src="https://developer.android.com/guide/platform/images/android-stack_2x.png" alt="Platform Architecture | Android Developers" style="zoom:45%;" />

* **Linux Kernel** (ez az alapja)
	* Driver-ek
	* Memória kezelés, folyamatok ütemezése
	* Alacsony fogyasztást elősegítő teljesítmény-kezelés
* **Hardware Abstraction Layer** (HAL)
	* kommunikációt biztosítja
* **Alkalmazás réteg**
	* itt futnak az alkalmazások
	* **ART:** *Android RunTime* (virtuális gép)
		* minden Android alkalmazás külön virtuális gép példányban fut (így nem egybe fagynak le)
	* c, c++ban írt kód is futtatható, de az nem a virtuális gépen &rarr; veszélyes tud lenni
* **Java API Framework**
	* Managers API-kat biztosít (pl Notification, Location)
* **System Apps**
	* alap elérhető alkalmazások (tárcsázó, kamera, naptár)
	* könnyű lecserélni ezen alap alkalmazásokat
		* így mások ezek az alap alkalmazások különböző eszközökön

## Androidos alkalmazások fejlesztése

* **Android SDK** (*Software Development Kit*)
	* fejlesztői eszköz, emulátor (AVD Manager), Java, Kotlin
* **Android NDK** (*Native Development Kit*)
	* C, C++ natív kód futtatásának lehetősége
* **Android ADK** (*Accessory Development Kit*)
	* Android kiegészítő eszközök támogatása (dokkoló, egészségügyi eszközök, stb)
	* Android Open Accessory protocol (USB és Bluetooth)
* **Android Studio**
	* IntelliJ alapú fejlesztőkörnyezet
* Emulátor

## Első program

* Sima Java alapú verzió:

	```java
	public class HelloAndroid extends Activity  { //Activity őszosztályból származunk le
		public void inCreate(Bundle savedInstanveState) {
			super.onCreate(savedInstanceState); //Ős implementáció meghívása
	        //TextView megjelenítése:
			TextView tv = new TextView(this);
	         tv.setText("Hello BME!");
	         setContentView(tv);
		}
	}
	```


* Java + XML UI verzió:

	* ```xml
		layout/main.xml
		...
		<TextView
		          android:id="@+id/tvHello"
		          android:layout_width="match_parent"
		          android:layout_height="wrap_content"
		          android:text="@string/hello" />
		...
		```

	* ```java
		public class HelloWorldActivity extends Activity {
		    @Override
		    public void onCreate(Bundle savedInstanceState) {
		        super.onCreate(savedInstanceState);
		        setContentView(R.layout.main); //XML alapú layout
		        TextView myTextView = (TextView) findViewById(R.id.tvHello); //UI komponens kikeresése ID alapján
		        myTextView.append("\n--MODIFIED--");
		    }
		}
		```

* Egyszerű esemény kezelés

	* ```java
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
		    setContentView(R.layout.main);
		    final TextView myTextView = (TextView) findViewById(R.id.tvHello); //final, mivel anonim osztályból férünk hozzá
		    myTextView.appen("\n--MODIFIED--");
		    //egyszerű érintés esemény kezelése:
		    myTextView.setOnClickListener(new OnClickListener() {
		        public void onClick(View v) {
		            myTextView.append("\n--CLICKED---");
		        }
		    })
		}
		```

## Kotlin

* Az előbbi kódrész Kotlin-ban:

  ```kotlin
  class MainActivity: AppCompatActivity() { //leszármaztatás kettősponttal
  	override fun onCreate(savedInstanceState: Bundle?) {
  		super.onCreate(savedInstanceState)
  		myTextView.append("#") //kotlin bővítmény miatt használható
  		
  		myTextView.setOnClickListener{ //lambda hívás
              myTextView.append("\n--CLICKED--")
          }
  	}
  }
  ```

* függvény átadása paraméterként

	```kotlin
	class MainActivity : AppCompatActivity() {
	    override fun onCreate(savedInstanceState: Bundle?) {
	        super.onCreate(savedInstanceState)
	        setContentView(R.layout.activity_main)
	        btnTime.setOnClickListener(::click) //ez itt a paraméterként átadott függvény
	    }
	    private fun click(view: View) {
	        Toast.makeText(this, timeText, Toast.LENGTH_LONG).show() //felugró ablak
	    }
	}
	```
