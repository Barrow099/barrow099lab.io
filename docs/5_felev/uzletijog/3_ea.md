# Üzleti jog - 3. előadás

<a class="md-button" style="text-align: center" href="../3_ea.pdf" download>Letöltés PDF-ként</a>

## A jogrendszer fogalma és típusai

* **jogrendszer**: az adott állam jogszabályainak rendezett összessége
	* jog: fogalmilag több jogszabályt tételez fel
	* jogszabály: tükrözi a valóságot, igazodik a többi jogszabályhoz
	* az egész több mint a részek csoportja!
* **felosztás**
	* történelmi materializmus szerint jogtípusok:
		* ókori, középkori, újkori, legújabb kori
	* modernizációelméleti megközelítés: premodern és modern jogok (polgári átalakulás előtti és utáni jogok)
	* R. David: jogrendszer-csoportok
		* kontinentális: európai kontinens jogai (ilyen a miénk is!)
		* szocialista: "szocialista országok" joga (ma már a kontinentális egy részének tekinthető inkább)
		* Common Low: angolszász országok &rarr; precedens jog!
		* vallási, tradicionális: pl iszlám, indiai, távol-keleti, afrikai (nagyon más mint a miénk, szent könyvön alapul, teljesen más a logikája)
* **különbség**: kontinentális = <u>törvényi jog</u> ↔ Common Law = <u>precedensjog</u>
	* **Common Law**-ban nem nagyon van jogalkotás, csak jogalkamazás, és abból lesz jogalkotás
		* nincs írott büntető törvénykönyv, polgári törvénykönyv, stb
		* kötik saját korábbi döntései
		* rengeteg precedens van &rarr; *Harvard Law School Library*
	* más a jogászképzés és a jogászi előrejutás &rarr; ott sokkal bonyolultabb és hosszabb
		* angol bíró = öregember
		* **Statute Law**: írott jog is

## A jogrendszer tagozódása

* két felosztás
  * közjog ↔ magánjog
  	* **közjogban** - állammal kapcsolatos, "közhaszon", vertikális alá-fölé rendeltségi jelleg
  		* alkotmányjog, közigazhatási jog, büntetőjog, eljárásjog, egyházjog, nemzetközi jog
  	* **magánjogban** - civilekre és gazdaságra vonatkozik, "magánhaszon", horizontális mellérendeltségi jelleg
  		* vagyonjog, családjog, öröklési jog, kereskedelmi jog, szerzői jog, szabadalmi jog
  	* liberálkapitalizmus (régen) ↔ monopolkapitalizmus (most)
  		* régen kevésbé volt beavatkozása az államnak a gazdaságba
  		* keveredett jogok: gazdasági jog, munkajog, versenyjog, pénzügyi jog
  * anyagi jog  ↔ alaki jog (eljárásjog)
  	* **anyagi jog** &rarr; jogokat, kötelességeket állapít meg
  	* **alaki jog - eljárásjog** &rarr; az eljárás módját, formai lehetőségeit határozza meg
  		* büntető eljárásjog
  		* polgári eljárásjog
  		* közigazgatási eljárás

## Jogágak

* jogág: azonos típusú társadalmi viszonyokat azonos módszerrel szabályozó jogszabályok összessége
* felosztás:
	* **alkotmányjog** (régen államjog)
		* államszervezet felépítésének és működésének elve
		* állam és állampolgári jog viszonya
		* mik az állampolgári jogok
			* tiszta alkotmányjogi norma &rarr; pl választójog, állampolgárság
			* áttételes alkotmányjogi norma &rarr; pl nők hátrányát megelőző dolgok
	* **közigazgatási jog** (államigazgatási jog) &rarr; pl piroson átmentünk, stb 
	* **büntetőjog** &rarr; bűnről és bűnhődésről szól
		* általános rész: bűncselekményekre vonatkozó közös szabályok (miért van joga büntetni az államnak?)
		* különös rész: definíciók a bűncselekményekre
	* **büntető eljárásjog** &rarr; büntetőjog eljárásának alkalmazása
		* <u>állam</u> áll szemben a <u>terhelt</u>-tel (a sértett csak tanú)
		* 3 főszakasz
			* nyomozás (felderítés) &rarr; gyanúsított van
			* bírósági eljárás (kiszabás) &rarr; vádlott van
			* végrehajtás &rarr; elítélt van
	* **polgári jog** &rarr; ez az üzleti jog gerince
		* vagyonjog
			* *tulajdonjog* (statikus) &rarr; a tulajdonos jogai, kötelességei (telefon az enyém)
			* *kötelmi jog* (dinamikus) &rarr; szerződés, kártérítés (eladom, ellopják)
			* *öröklési jog* (dinamikus) &rarr; törvényes, végintézkedésen alapuló (ki örököli meg)
		* személyi jog, jogi személy, gazdasági társaság
			* személyiség polgári jogi védelme
			* szellemi alkotás joga &rarr; szerzői jog, iparjogvédelem
	* **polgári eljárásjog**
		* felek állnak szemben &rarr; felperes-alperes
		* pl válóperek
	* **munkajog**
		* vegyes szakjog, szerződési jogból önállósult (mert túl gyakori)
		* kollektív &rarr; hogy szervezhetünk sztrájkot, alapíthatunk szakszervezetet
		* individuális &rarr; munkaszerződés, munkaügyi eljárás (én hogyan kell hogy melózzak)
		* 2 speciális ág &rarr; közalkalmazotti törvény, köztisztviselői-kormánytisztiviselői törvény
	* **családjog** &rarr; most nem külön álló, már a polgáriba tartozik
	* **pénzügyi jog** &rarr;  pl adójog
	* **mezőgazdasági jog** &rarr; tulajdonjogból lett forma, termőföld egy speciális dolog
		* földtulajdon, földhasználat, földvédelem, földigazgatás, stb
	* **nemzetközi magánjog** &rarr;  nem igazán jogág
		* globalizációval összefüggésben jelent meg!
		* nemzetközi konfliktusban mi a kapcsoló elv (melyik nemzet jogát vegyük?)
	* **nemzetközi jog** &rarr; nem igazán jogág, inkább politikai kérdés
		* mi teszi a jogot joggá?
			* szuverenitás (van egy szuverén állam), szankció (ez büntetést szab ki)
		* nemzetközi jognál ezek nem teljesülnek
			* sok állam van, nem egy &rarr; megegyezés
			* szankció &rarr; együttműködési készség csökkentése
		* lehetőségek: külügyminiszter behívatása, bojkott, háború 