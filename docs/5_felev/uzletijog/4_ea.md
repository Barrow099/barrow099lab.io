# Üzleti jog - 4. előadás</br>Európai jog

<a class="md-button" style="text-align: center" href="../4_ea.pdf" download>Letöltés PDF-ként</a>

## Bevezetés: Az EU ma

* kialakulása: közös célok mentén (béke, térségbiztosítás, gazdasági növekedés, stb)
* fontosabb értékek:
	* emberi méltóság
	* szabad mozgás joga
	* demokrácia
	* egyenlőség elve (nők = férfiak, munkához való jog)
	* jogállamiság (igazságszolgáltatás függetlensége)
	* emberi jogok (faji, nemi, etnikai, vallási, semmilyen megkülönböztetés tilos)
* jelenleg: 27 ország, 24 hivatalos nyelv (nyelvi sokszínűség érték)
* Magyarország 2004-ben csatlakozott
* 5milliós lakosság
* Jelképek:
	* EU zászló (12 csillag kék alapon - szolidaritás, harmónia, egység)
	* EU himnusz &rarr; Beethoven, Örömóda
	* Európa nap &rarr; május 9
	* jelmondat (2000): "Egység a sokféleségben", "Egység a sokszínűségben", "Egyesülve a sokféleségben"
		* béke és jólét megteremtése
* jelentős költségvetési források, többéves pénzügyi keret &rarr; EU-s költségvetéssel segítik az egyenlőség fele törekvést (Magyarországnak 2011: 4,73 milliárd euró)
	* EU parlament felügyeli, hogy mi lesz a pénzzel

## Az EU története: az integráció mint gazdasági és jogi folyamat

* meghatározó személyiségek
	* Winston Churchill (brit) - béke európa szintű egyesüléssel, Európai Egyesült Államok
	* Conrad Adenauer (német) - 
	* Alcide De Gasperi (olasz) - franciákal együttműködés
	* Robert Schuman  - hadiipar, szén és acéltermelés, háborúellenesség
	* Jean Monnet - szülőatyja az európai integrációnak
* kialakulás okai
	* két nagy nem európai világrend &rarr; USA és Szovjetek
	* Európát kezdték szétszakítani (Kelet-EU Szovjet lett), Nyugat ezért elkezdett összefogni
	* **Európai Szén- és Acélközösség** létrejön
* intézményi keretek
	* Benelux Unió (1948), OEEC - európai újáépítés szervezete, NATO (1949) - katonai szerep
* 1957 - **Római szerződések**: EURATOM & EKG aláírása (Belg, Fr, Holl, Lux, NSZK, Olasz)
	* vámok megszűntetése
	* közös kereskedelempolitika a kívülállókkal
	* szabad áru, szolgáltatás, tőke és munkaerő áramlás
	* jogharmonizáció
* lépésenként bővült (1958 - alapítók, 73, 81, 86, 95, 04 (mi is), 07, 13)
* 85 óta Shangen-i övezet
* Euró övezet - 19 országban
* 1985 - Fehér Könyv &rarr; egységes piac megvalósuljon
* **Maastrichti Szerződés** &rarr; gazdasági és monetáris unió, közös valuta legyen bevezetve
* **Amszterdami szerződés** (97) - alapító szerződések átláthatóbbá tétele, de darabos lett
* **Nizzai Szerződés** (2000) - Amszterdami maradék elkészítése
* **Liszaboni szerződés** (2007) &rarr; legfontosabb érték az emberi szabadság, egyenlő jogok, emberi jogok, stb 

## Az EU intézményei

* fő szervek:
	* **Tanács**: kormányközi alapokon működik, legfőbb törvényhozó és jogalkotó szerv
	* **Bizottság**: javaslattevő, döntés-előkészítő, jogkezdeményező
	* **Parlament**: társdöntéshozó, társjogalkotó, konzultatív és ellenőrző testület
	* **Bíróság**: ügyel a közösségi jog alkalmazására, betartására, érvényesülésére
	* **Számvevőszék**: EU pénzügyeit ellenőrzi
* szervek bővebben:
	* **Miniszterek Tanácsa**: változó összetételben ülésező, elsődleges de nem kizárólagos kormányközi alapokon működő döntéshozó és jogalkotó szerv
		* tagjai: tagállamok kormányának képviselői, általában adott témában jártas <u>felelős miniszter</u>
		* feladatai: jogalkotás, tagállamok gazdaságpolitikáinak összehangolása, nemzetközi egyezmények létrehozása, EU költségvetés jóváhagyása, EU Közös Kül- és Biztonságpolitikájának kialakítása, együttműködés összehangolása büntetőügyekben (nemzeti bíróságok és rendőrségek között)
		* szavazás: több lakos &rarr; több szavazattal rendelkeznek, de a szavazatuk súlya kisebb
	* soros elnök = Tanács elnöke: felváltva fél évente egy-egy tagállam vállalja (3 tagállam összesen)
	* **Európai Tanács**: EU állam- és kormányfőinek testülete, csúcsszerv, nem a tanács része!
		* EU politikai programjának meghatározása, irányok kijelölése, de nem jogalkotók!
		* jelöltek állítása, Európai Bizottság felkérése
		* 2.5 évre szól az elnöki pozíció
		* évente 4x ülésezik
	* **Európai Tanács elnöke**: folytonos stabilitást adnak
	* **Európai Bizottság**: jogszabályjavaslatok kidolgozása, végrehajtó szerv, EU nemzetközi képviselése, szerződések őre, 28 független tag (országonként egy)
		* bizottság kinevezése: 5 évente, Tanács jelöli ki (minősített többséggel), Parlament ezt jóváhagyja
		* tagok függetlenek a nemzeti kormányoktól és a Tanácstól
	* Unió **kül- és biztonságpolitikai főképviselője**: Federica Mogherini
		* elnököl a Külügyek Tanácsa és Európai Bizottság alelnöke
	* **Európai Parlament**: unió polgárai választják 5 évente (705 fő összesen)
		* 2 nagy párt - néppárt, szocialisták
		* döntéshozó, jogalkotó intézmény
	* **Európai Bíróság**: 27 bíró, ítélkezés uniós jog alapján, gondoskodik a jogszabályok egységes alkalmazásáról az EU egész területén
	* **Európai Számvevőszék**: 27 tag
	* Európai Gazdasági és Szociális Bizottság
	* Régiós Bizottság:
	* Európai Központi Bank
	* EU köztisztviselői
* Jogalkotási szintek
	* Polgárok, érdekcsoportok, szakértők: megbeszélések, konzultációk
	* Európai Bizottság: hivatalos javaslat
	* Európai Parlament és a Miniszterek Tanácsa: közös döntés
	* Országos vagy helyi hatóságok: végrehajtás
	* Európai Bizottság és Európai Bíróság: végrehajtás nyomon követése