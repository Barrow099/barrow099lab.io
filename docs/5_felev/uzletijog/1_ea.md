# Üzleti jog - 1. előadás

<a class="md-button" style="text-align: center" href="../1_ea.pdf" download>Letöltés PDF-ként</a>

## Jogtani alapismeretek

### Jogfogalom

* a **társadalom magatartásirányítási rendszere**
    * **normativitás fogalma**: *<u>előírásrendszer, amely egy adott szituációban előírja a megfelelő magatartást</u>*
        * pl: tilalmak, tabuk, követendő magatartási minták, rögzült szokások összessége
    * eszközei: jogszabályok, erkölcsi normák, vallási normák, szubkulturális normák
* **jog sajátosságai és fogalma**
    * legfontosabb viszonyokat szabályozza &rarr; <u>jog kevesebbet szabályoz mint az erkölcs</u>!
    * kapcsolatban van az állammal
        * állam jogalkotó szervei <u>hozzák létre</u> ÉS jogalkalmazó szervei alkalmazzák és kényszerítik ki
* **jog fogalma**
    * magatartásirányitási rendszer része
    * hipotetikus szabályokból áll (mikor mit kell csinálni)
    * szabályai általánosan vannak megfogalmazva &rarr; minden normára igaz (ezért nem parancs)
    * ismétlődően kell őket követni &rarr; minden normára igaz (ezért nem parancs)
    * állam hozza létre (*jogalkotás*)
    * állam működteti (*jogalkalmazás*)
    * csak a legfontosabb viszonyokat szabályozza
    * belső ellentmondásmentességre törekszik
        * olyan intézményeket fejleszt ki, amik ezt biztosítják
        * <u>érvényesség</u>: alacsonyabb és magasabb szintű nem lehet ellentmondásos (jogszabályok **hierarchikus rendszert** alkotnak)
        * <u>időbeli hatály</u>: később és korábban születettek nem lehetnek ellentmondásosak

### A jogi norma szerkezete

* **jogi norma** = jog legkisebb értelmes része (1-2 mondat egy jogszabályban)
* **regulatív norma** &rarr; <u>magatartási formákat</u> magukba foglaló normák
* azonos <u>logikai szerkezet</u>
    * **hipotézis**: feltétel, ami leírja a társadalmi szituációt
    * **diszpozíció**: rendelkező rész, amely a meghatározott magatartást írja elő
    * **jogkövetelmény**:
        * szankció (=hátrányos jogkövetkezmény), jutalom, egyéb joghatás
* diszpozíció alapján **3 típusú norma**:
    * **imperatív**: *közjogban* a jogalanyra <u>kötelező</u>
    * **kógens**: *magánjogban* a jogalanyra <u>kötelező</u>
    * **diszpozitív**: *magánjogban* <u>nem kötelező</u>, csak <u>ajánló</u> (jogalanyok együttes akarattal eltérhetnek tőle)

### Érvényesség és hatályosság

* <u>nem szinonímák</u>!
* **érvényesség**: tágabb, elméleti kategória
    * lehető legtágabb kategória &rarr; <u>jog elkülönítése a nem-jogtól</u>
* **hatályosság**: szűkebb, gyakorlati kategória
    * érvényes jogon belül jelöl ki <u>ténylegesen működő</u> szférát

#### Érvényesség

* 3 feltétel:
    * **megalkossák**: állami jogalkotó szerv megfelelő eljárás keretében létrehozza
    * **kihirdessék**: érdekeltek tudomására hozzák
        * országos hatályú jogszabály &rarr; Magyar Közlöny-ben
        * helyi hatályú jogszabály &rarr; egyéb kihirdetési mód
    * **nem mond ellent magasabb szintű jogszabály előírásainak**
        * ha ellentmond &rarr; <u>érvénytelenség</u>
            * Alkotmánybíróság szerepe &rarr; normakontroll (Alaptörvénnyel ellent mond e)

#### Hatályosság

* 3 megnyilvánulási terület: időbeli, személyi, területi hatály (mettől-meddig, kikre, hol)
* **időbeli hatály**:
    * *kezdete*:
        * kihirdetésével egy időben
        * kihirdetése után &rarr; "kódex"-ek túl nagy terjedelműek (pl polgári törvénykönyv módosulásárakor 1 év volt a határba lépésre)
        * kihirdetés előtt &rarr; visszamenőleges hatály (jogállami szempontból aggályos, CSAK ha jogalanyra vonatkozóan kedvezőbb helyzetet teremt)
    * *vége*:
        * később született jogszabály vele ellentétes rendelkezést hoz &rarr; **hatályon kívül helyezi** a korábbit
        * maga a jogszabály rendelkezik saját hatályának végéről &rarr; ritka
* **személyi**: 
    * *általános*: mindenkire
    * *különös*: szűkebb személyi körre (pl hivatalos személy, házastárs)
* **területi hatály**:
    * *országos*: központi jogalkotó szervek által alkotott jogszabályok
        * magyar és nem-magyar állampolgárokra egyaránt
        * magyar állampolgárokra külföldön is (ellentmondást is eredményezhet)
        * diplomáciai mentességet élvező személyekre az országon belül sem
    * *helyi*: önkormányzati rendeletek (csak saját területükön)

### Jogviszonyszemlélet

* **jogviszony fogalma**
    * jog magatartást szabályoz &rarr; viszonyt is szabályoz
    * *jogviszony*: jogilag szabályozott társadalmi viszony, amelyben a jog az emberi magatartást szabályozza
    * pl: együtt járás = társadalmi viszony, regisztrált élettársi kapcsolat = picit már jogviszony, összeházasodnak = jogviszony
* **jogviszony elemei**
    * **alanya**: ha kenyeret veszek &rarr; ÉN és a PÉK
        * *természetes személy*: ember, általános és egyenlő jogalanyiság
        * *állam*: általános jogalanyiság, bizonyos viszonyokban kizárólagos jogalanyiság
        * *jogi személy*: szervezet (cél, vagyon, szervezet, felelősség)
        * *jogképességgel fölruházott nem jogi személy szervezet*: kivételes
    * tárgya:
        * *közvetlen tárgy*: magatartás (pl más bántalmazása)
        * *közvetett tárgy*: konkrét dolog (pl kenyér)
* **jogi tények**
    * *események*:
        * születés, halál, életkor betöltése, stb
    * *magatartások*:
        * tiltott
        * megengedett: jogügyletek &rarr; egyoldalú - kétoldalú (pl szerződés = kétoldalú jogügylet)
* **jogviszony szerkezete**
    * megkülönböztetés alapja:
        * ki van megemlítve a jogviszonyban
        * mire köteles a kötelezett (tevésre vagy nem tevésre?)
    * *abszolút szerkezetű* (pl tulajdonjog):
        * csak a jogosult van konkrétan megemlítve (a kötelezett nincs)
            * pl rajtam kívül mindenki nemtevésre kötelezett a telómmal kapcsolatban (ne vegyék el a telóm)
        * kötelezett csak nem-tevésre (cselekvéstől való tartózkodásra köteles)
    * *relatív szerkezetű* (pl szerződési jog):
        * jogosult mellett a kötelezett is név szerint szerepel (eladom Pisti-nek a telómat)
        * kötelezett cselekvésre köteles (fizesse ki a telóm)
