# Üzleti jog - 2. előadás<br>Államtam, államszervezet

<a class="md-button" style="text-align: center" href="../2_ea.pdf" download>Letöltés PDF-ként</a>

## A magyar államszervezet

* <img src="/src/uzletijog/image-20200922095613181.png" alt="image-20200922095613181" style="zoom:50%;" />
* redukált: nincsenek rajta társadalmi szervek, vállalati-intézményi szféra, hivatali jellegű szervek
* jogi ábra: csak jogi alá-fölérendeltségi viszonyokat ábrázolja
* **horizontális** tagoltság
  * törvényhozás
  * végrehajtás
  * igazságszolgáltatás
  * +ügyészi szervtípus (Magyarországon)
* **vertikális** tagoltság
	* országos
	* regionális &rarr; csak bírósági és ügyésszégi szinten
	* megyei
	* járási
	* helyi &rarr; városi és községi
* (alapvető jogok biztosa (országgyűlési biztos) = ombudsman)

## Az egyes szervtípusok

* **államhatalmi-képviselet és önkormányzati szervek** &rarr; törvényhozás
	* Országgyűlés, önkormányzatok (megyei, fővárosi, helyi)
	* közvetlenül választjuk őket, döntéshozatallal foglalkoznak
* **államigazgatási szervek** &rarr; végrehajtás
	* kormányzás és közigazgatás
		* ezek egyrészt <u>általános hatáskörű szervek</u>, illetve <u>szakigazgatási szervek</u>
	* jellemzőik: jogalkotás, jogalkalmazás, szervezés
* **bírósági szervezetrendszer** &rarr; igazságszolgáltatás
	* szervezetrendszer (Országos Bírói Hivatal)
		* Kúria (felülvizsgálati kérelmek elbírálása, jogegységi határozatok)
		* ítélőtáblák (másodfok)
		* törvényszékek (megyénként, első + másodfok)
		* járásbíróságok (első fok)
	* jogalkalmazó szerv (nem alkotja a jogot, hanem csak alkalmazza!)
	* bírók függetlenek
	* vannak választott bíróságok
* **ügyészségi szervezetrendszer** (orosz múlt miatt)
	* szervezetrendszer
		* Legfőbb Ügyészség
		* fellebbviteli főügyészségek
		* megyei ügyészségek
		* járási ügyészségek
	* nem független, hanem hierarchikus!
* plusz
	* **köztársasági elnök**
		* funkciói:
			* nemzeti egység kifejezése
			* államszervezet demokratikus működése feletti őrködés
			* fegyveres erők főparancsnoka
		* 5 évre választja a parlament (direkt van elcsúsztatva a parlamenti választáshoz képest)
		* feladatai (de <u>csak miniszteri ellenjegyzéssel</u>)
			* az állam képviselete &rarr; nemzetközi dolgok megkötése
			* kezdeményezési jogok &rarr; választás kezdeményezése
			* személyi jogok &rarr; mindenkit ő nevez ki (minisztereket, államtitkárokat, nemzeti bank, egyetemi tanárok, stb)
			* kegyelmi jogok &rarr; (királyi jogokból örökölt)
			* döntés állampolgársági ügyekben &rarr; (királyi jogokból örökölt)
	* **Alkotmánybíróság**
		* 2/3 többséggel választva
		* lényeg: <u>normakontroll</u>
	* **alapvető jogok biztosa** (**Ombudsman**)
		* svédországból származik
		* gyermekek jogai, jövő nemzedék érdekei, nemzetiségek jogai, veszélyeztetett társadalmi csoportok jogainak védelme
		* nincs önálló hatásköre, csak parlamenti ellenőrző szerv
		* állampolgári alapjogokat védi
	* **Állami Számvevőszék**
		* pénzügyi szerv
		* költségvetési kiadások utólagos ellenőrzése
	* **Költségvetési Tanács**
		* költségvetés megalapozottságának vizsgálata
		* vétójoga is lehet
	* egyéb rendeletalkotási joggal rendelkező állami szerv
		* **Magyar Nemzeti Bank**
		* **Nemzeti Média és Hírközlési Hatóság**

## A jogforrási hierarchia

* jogforrási **hierarchia**
	* *Országgyűlés*: Alaptörvény, sarkalatos (kétharmados) törvény, törvény
	* *kormány*: kormányrendelet
	* *minisztériumok*: miniszteri rendelet
	* *önkormányzatok*: önkormányzati rendelet (csak helyi határú rendeletek)
* **speciális jogforrások**
	* nemzetközi egyezmény
	* EU jogforrás (rendelet vagy irányelv)
	* állami irányítás egyéb jogi eszközei:
		* határozat, utasítás, irányelv, tájékoztató