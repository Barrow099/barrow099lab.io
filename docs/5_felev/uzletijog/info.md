# Üzleti jog (GT55A001)

**Honlap:** GTK Moodle

!!! warning "Távoktatás"
	Távoktatás során az előadások valós időben Teams-en lesznek megtartva (ha valaki elindítja a felvételt, akkor vissza is lehet majd nézni). Fontos, hogy minden héten ki kell tölteni a Moodle rendszeren egy önellenőrzést segítő kérdéssort.

## Adatok

* Kredit: 2
* Tanszék: GTK ÜTI
* ZH: 2db

## Tárgy teljesítése

* két évközi zh sikeresre megírása
* Félév végi jegy = ZH1 (max 25p) + ZH2 (max 25p)
    * 0 - 24 pont: **1**-es
    * 25 - 30 pont: **2**-es
    * 31 - 37 pont: **3**-as
    * 38 - 43 pont: **4**-es
    * 44 - 50 pont: **5**-ös

## Előadás

| Hét  | Dátum      | Előadás                                       | Jelenlét | Tudom |
| ---- | ---------- | --------------------------------------------- | -------- | ----- |
| 1    | 2020.09.11 | Bevezetés, jogtan                             |          |       |
| 2    | 2020.09.18 | Államtan, államszervezet, jogforrási rendszer |          |       |
| 3    | 2020.09.25 | Jogrendszer, jogágak                          |          |       |
| 4    | 2020.10.02 | EU-jog *(Dr. Steixner Zsófia)*                |          |       |
| 5    | 2020.10.09 | Szerződési jog 1.                             |          |       |
| 6    | 2020.10.16 | Szerződési jog 2.                             |          |       |
| 7    | 2020.10.20 | **ZH. I.**                                    |          |       |
| 7    | 2020.10.23 | -                                             |          |       |
| 8    | 2020.10.30 | Társasági jog 1.                              |          |       |
| 9    | 2020.11.06 | Társasági jog 2.                              |          |       |
| 10   | 2020.11.13 | Társasági jog 3.                              |          |       |
| 11   | 2020.11.20 | Munkajog *(Dr. Steixner Zsófia)*              |          |       |
| 12   | 2020.11.27 | -                                             |          |       |
| 13   | 2020.12.04 | Versenyjog                                    |          |       |
| 13   | 2020.12.08 | **ZH. II.**                                   |          |       |
| 14   | 2020.12.11 | Összefoglalás                                 |          |       |

## Zárthelyi

2 darab ZH, mindkettőn max 25 pont szerezhető