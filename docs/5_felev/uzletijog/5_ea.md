# Üzleti jog - 5. előadás</br>Szerződési jog I.

## Bevezetés: a polgári jog és szerkezete

* **vagyonjog**
	* *tulajdonjog* (statikus) &rarr; tulajdonos jogai, kötelességei 
	* *kötelmi jog* (dinamikus) &rarr; szerződés, kártérítés
	* *öröklési jog* (dinamikus) &rarr; törvényes, végintézkedésen alapuló
* **személyi jog**
	* *általános*: személy, a személyiség polgári jogi védelme
	* *különös*: a személyi alkotás joga (szerzői jog, iparjogvédelem)

## Kötelem - szerződés

* **kötelem**: relatív szerkezetű jogviszony
	* kötelezettség a szolgáltatás teljesítésére és jogosultság a szolgáltatás teljesítésének követelésére
	* *kötelezett*: vagyoni értékű szolgáltatás teljesítése
	* *jogosult*: vagyoni értékű szolgáltatás követelése
	* kötelem irányulhat: dolog adására, tevékenységre, tevékenységtől való tartózkodásra vagy más magatartásra
* szerződés, károkozás, jogalap nélküli gazdagodás (vízművek visszafizeti a többletet), megbízás nélküli ügyvétel, egyoldalú jognyilatkozat (pl díjkitűzés), stb

## A szerződés fogalma

* **áruviszony** elemei
	* elkülönültség: szeélyek + tulajdon
	* összekapcsolódás: szerződés
	* értékviszony
* **szerződés fogalma**
	* gazdasági tartalom
	* jogi forma
		* *a felek szabad, kölcsönös és egybehangzó jognyilatkozata amelyből kötelezettség keletkezik a szolgáltatás teljesítésére és jogosultság a szolgáltatás követelésére*
		* felek &rarr; nem egy személy
		* szabad, kölcsönös és egybehangzó &rarr; konszenzusra kell jutni!
		* jognyilatkozat &rarr; joghatás kiváltására induló akaratnyilatkozat
			* lehet <u>írásban, szóban vagy ráutaló magatartással</u>
		* szükséglet &rarr; érdek (akkor lesz, ha konfliktus van a szükségletnél) &rarr; akarat &rarr; nyilatkozat
			* ok: szükségletek kielégítése konfliktusos zónában van
	* állami elismerés
		* 3féleképp viszonyulhat
			* *ellenségesen*: joghatás megtagadása (pl bérgyilkos felbérlése)
			* *közömbösen*: elismeri, de nem segíti (pl kártyaadósság) 
			* *támogatóan*: kikényszeríti a teljesítést, pl kártérítés sújtja a szerződésszegőt

## Polgári jog - kereskedelmi jog

* *polgári jogi szerződés*: mindennapi életben, laikus-laikus között
* *kereskedelmim ügylet*: élethivatás szerűen kereskedelemmel foglalkozók között, profi-profi
* **szabályozás**: kétféle európai szerződési rendszer
	* *dualista*: külön törvénykönyvben a polgári jog és a kereskedelmi jog
	* *monista*: a polgári törvénykönyvben a kereskedelmi jog is (új 2013)

## A szerződési jog alapelvei

* <u>szerződés szabadsága</u> &rarr; nagyon fontos
	* *akarati autonómia*: akarok e vagy nem akarok szerződést kötni?
	* *partnerválasztás szabadsága*: kivel akarok szerződést kötni?
	* *szerződési típusszabadság*: szabadon választhatom a Ptk szerződéstípusait de atipikus vagy vegyes szerződés is szabadon választható
	* *diszpozitivitás*: egységes akarattal el lehet térni a szerződés szabályaitól
* <u>jogszabály által meghatározott szerződési tartalom</u> &rarr; amikor nem a diszpozitivitás uralkodik
* <u>visszterhesség vélelme</u>
	* piaci viszonyok &rarr; érték - ellenérték egyensúlyba legyen (ha nincs így akkor megtámadható)
	* nincs ajándékozási szándék
* <u>együttműködési és tájékoztatási kötelezettség</u>

## A szerződési jogviszony elemei

* **alanyai**: a felek
	* többen vannak
	* jogosult - kötelezett (vannak speckó nevek)
	* természetes személy, vagy törvényes képviselő
	* jogosultként
		* osztható szolgáltatás &rarr; saját részét kapja mindenki
		* oszthatatlan szolgáltatás
			* együttesség &rarr; oszthatatlanból is mindegyik kapjon
			* egyetemlegesség &rarr; elég egyiknek teljesíteni
	* kötelezettként
		* osztott kötelezettség &rarr; mindenkitől a ráeső rész követelhető
		* egyetemleges kötelezettség &rarr; a teljes szolgáltatást igényelheti a jogosult
	* **alanyváltozás**
		* természetes személy halála &rarr; örökös átveszi vagy nem veszi át
		* szervezet &rarr; jogutód átveszi mindet
		* jogosult pozíció átengedése: **engedményezés**
		* kötelezett pozíció átengedése: **tartozásátvállalás** (jogosult beleegyezése szükséges)
* **tárgya**: a szolgáltatás és a dolog
	* közvetlen &rarr; **szolgáltatás**
	* közvetett &rarr; **dolog** (pl: pénz, értékpapír)
	* szolgáltatások felosztása
		* tevőleges - nem tevőleges
		* egyszeri, tartós, visszatérően teljesíthető
		* személyhez kötött - forgalmi jellegű (megbízás - adásvétel)
		* egyedi (festmény) - fajlagos (kölcsönadott 100ezer) - zártfajú (pl fajlagos szolgáltatás szűkítésével meghatározott)
		* osztható - oszthatatlan
* **tartalma**: a jogok és a kötelességek
	* *főkötelezettségek*: pl ki kell fizetnem a házat
	* *mellékkötelezettségek*: pl foglalót adok
	* felek határozzák meg
	* jogszabály
		* közvetlenül: hatósági eszközökkel (márpedig ez és ez a tartalma a szerződésnek)
		* közvetetten: a szerződés "minimális tartalmának" meghatározása

## A szerződésértelmezés

* szerződés = akaratnyilatkozat &rarr; két elv:
	* *nyilatkozati elv* &rarr; objektív, a másik fél hogyan értelmezheti, általános nyelvi jelentéstartalom
	* *akarati elv* &rarr; szubjektív, a nyilatkozó fél szándéka, akarata
* magyar Ptk ötvözi ezt a két elvet

