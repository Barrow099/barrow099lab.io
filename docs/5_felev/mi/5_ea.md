# Mesterséges intelligencia - 5. előadás</br>Logikai ágens, szabályalapú rendszerek

<a class="md-button" style="text-align: center" href="../5_ea.pdf" download>Letöltés PDF-ként</a>

## Történelem

* Mycin &rarr; szakorvosi szintű első szabályalapú rendszer &rarr; tünetek alapján betegség és kezelés javaslat

## Logika szerepe a komplex MI alkalmazásokban

* logikai szabályalapú következtetés és beavatkozás
	* egy vagy több feltétel logikai állításként történő megfogalmazása
	* reakciók, beavatkozások megadása logikai szabályok formájában
* IBM Watson (2011): Jeopardy kvízjáték &rarr; beszédértés, következtetés, játék

## Logika

* 2 része van
	* **tudás bázis**: feladatspecifikus információ (elméletek + észrevételek)
	* **következtető gép**: feladatfüggetlen algoritmusok
* <img src="/src/mi/image-20201022192034689.png" alt="image-20201022192034689" style="zoom:50%;" />
* példák ilyen rendszerekre: jogi szakértői rendszerek, ügyfélszolgálat, csetbot, stb
* előnyei:
	* <u>látjuk a levezetést</u> is! (hogy jutottunk pl a diagnózisig?)

### Reprezentáció és manipuláció eszköze

* **szintaktika**: legálisan létrehozható szimbolikus mondatok

* **szemantika**: a világ tényei, amikre a szimbolikus mondatok vonatkoznak

* **tények**: valós dolgok a világban, amik egymással kapcsolatban vannak

	* **vonzat**: <u>logikai konzekvencia dolgok között</u>
	* vonzatreláció &rarr; α vonzata TB-nek: **TB |= α**, ha α minden olyan világban igaz, ahol TB is igaz.

* **következtetés**: a vonzat kiszámítása a mondatok formális manipulálásával

* **bizonyítás**: a következtetési algoritmus lépéssorozata

	* α bizonyítható TB-ből: **TB |- α**

* szintaktika és szemantika összekapcsolódása &rarr; **interpretáció** kell hozzá

	* az igazság függ mind a <u>mondat interpretációjától</u>, mind a <u>világ aktuális állapotától</u>

* **modell**

	* bármely világ, ahol a mondat igaz egy bizonyos interpretációban

	* minél több információt tudunk &rarr; annál kevesebb modellünk lesz

	* **egy α mondat vonzata a TB tudásnak, ha a TB modelljei mind modelljei az α-nak is**

		* ekkor ha TB igaz, akkor α is igaz, azaz
		* **TB |= α, akkor és csak akkor, ha M(TB) ⊆ M(α)**
		* <img src="/src/mi/image-20201023124053619.png" alt="image-20201023124053619" style="zoom:33%;" />

	* példa:

		<img src="/src/mi/image-20201023124343060.png" alt="image-20201023124343060" style="zoom:40%;" />

	* Ha adott egy TB, egy a következtetés

		1. létrehozhat új α mondatot, amely vonzata a TB-nak (azaz ha kikövetkeztetem, hogy [1;2] egy biztonságos mező, akkor abból létrejön egy új α mondat, ami az adott tudásbázisból következik, tehát annak vonzata)
		2. ha adott α mondat, eldöntheti hogy α vonzata-e a TB-nak (azaz ha kikövetkeztetem, hogy [1;2] egy biztonságos mező, akkor abból egy adott α mondatról eltudom dönteni, hogy vonzata-e a tudásbázisomnak)

	* **igazságtartó következtetés** (*extenzionális*), ha <u>csak igazságfüggvényekkel dolgozik</u>

		* (igazságfüggvény logikai értéke &rarr; csak az igazságfüggvény részeitől és logikai műveletek definíciójától függ, <u>az interpretációtól</u> nem!)

	* **formális bizonyítás** &rarr; igazságtartó következtetési eljárás lépései

	* **következtetési eljárás**

		* **teljes**: ha minden vonzatmondathoz talál egy bizonyítást (azaz ami igaz az bebizonyítható)
			* **A |= B-ból A |- B**
		* **helyes**: ha minden bizonyított mondat vonzatrelációban áll a felhasznált tényekkel (azaz ami bebizonyított, az igaz is)
			* **A |- B-ból A |= B**

	* **érvényes** (analitikus mondat, tautológia): ha minden világban minden lehetséges interpretációja igaz, függetlenül attól, hogy mit akar jelenteni és mi a világ állapota (pl van fal vagy nincs fal)

	* **kiegészíthető**: ha létezik olyan interpretáció, hogy valamely világban igaz. Különben **kiegészíthetetlen**.

* példa:

	* <img src="/src/mi/image-20201024230605156.png" alt="image-20201024230605156" style="zoom:33%;" />
	* itt az első érvényes (minden világban 1), a második érvénytelen (B és Bnegált miatt), a harmadik kielégíthető (néhány világban teljesül, pl ahol A=1 és B=1)

* **Ítéletkalkulus**

	* igaz és hamis logikai konstansok, precedencia: ¬, ∧, ∨, →, ⇔
	* szemantika: minden modell i/h értéket rendel minden ítélet szimbólumhoz
	* <img src="/src/mi/image-20201024233247533.png" alt="image-20201024233247533" style="zoom:33%;" />
	* N argumentumon értelmezett logikai függvény 2 a 2^N^-ediken &rarr; igazságtáblás kiértékelés nem nagyon lesz használható
	* <img src="/src/mi/image-20201024234147628.png" alt="image-20201024234147628" style="zoom:40%;" />
	* Két állítás logikailag ekvivalens, ha ugyanazokban a modellekben igaz, azaz **A ⇔ B akkor és csak akkor, ha A |= B és B |= A**
	* **TB |= α akkor és csak akkor, ha (TB → α) érvényes**
	* **TB |= α akkor és csak akkor, ha  (TB ∧ ¬α) kielégíthetetlen**

* **következtetés módjai**

	* modell-ellenőrzés (a mondat kielégíthető-e)
		* igazságtábla listázás
		* visszalépéses keresés
		* heurisztikus keresés modellek terében (helyes de nem teljes)
	* következtetési szabályok alkalmazása &rarr; új mondatok legális generálása régebbi mondatokból
	* bizonyításkor **speciális operátorok**
		* keresés általános operátorokkal (természetes dedukció)
		* teljes keresés teljes operátorral általános logikában (rezolúció, exponenciális!)
		* teljes keresés teljes operátorral redukált logikában (Horn-klózok, Modus Ponens, redukált komplexitás!)

* **ítéletlogika következtetési mintái**

	* **modus ponens** (implikáció eliminálása): A → B helyett B
	* AND eliminálása: A~1~ ∧ A~2~ ∧ A~3~ ∧ ... helyett A~1~, A~2~, A~3~, ...
	* AND bevezetése: A~1~, A~2~, A~3~, ... helyett A~1~ ∧ A~2~ ∧ A~3~ ∧ ... (TB = 1 állítás)
	* OR bevezetése:  A~1~, A~2~, A~3~, ... helyett A~1~ ∨ A~2~ ∨ A~3~ ∨ ... 
	* Dupla negálás eliminálása: ¬¬A helyett A
	* elemi rezolúció: A ∨ B helyett ha Bről tudjuk, hogy negált, akkor A biztosan ponált
	* rezolúció: Ha szerepel B ∨ A és ¬B ∨ G, akkor a kettő helyett A ∨ B használható (ha van 2 diszjunktív állításunk, és pontosan az egyik elemre igaz, hogy az egyikben ponáltan a másikban negáltan szerepel, akkor kikövetkeztethetjük, hogy a két állításban a rezolúcióban nem érintett részek maradnak csak meg)

* **logikai következtetések fajtái**

	* **dedukció**: formálisan érvényes következtetés, <u>tudás átalakítás modellje</u>	
		* pl feltétel: ha a kutya nagy, akkor sokat eszik. tapasztalat: a kutya nagy. következtetés: sokat eszik.
	* **indukció**: ezek nem formálisan igazak, <u>tömörítés, általánosítás: példákból tanulás modellje</u>
		* pl tapasztalat: kutya1 nagy, kutya2 nagy, ..., kutya1000 nagy. induktív általánosítás: minden kutya nagy
		* formálisan NEM igaz!
	* **addukció**: belátás folyamata, <u>diagnózis, magyarázatadás modellje</u>
		* pl feltétel: ha a kutya nagy, sokat eszik. tapasztalat: a kutya sokat eszik. hipotézis: a ktuya nagy
		* formálisan NEM igaz!

* **ítéletlogika eldönthető és teljes**

	* beláthatóak véges algoritmussal &rarr; következtetés igazságtábla módszere teljes, DE a számítási idő exponenciális
	* egy mondathalmaz kielégíthetőségi vizsgálata NP-teljes

* **ítéletlogika monoton**

	* új mondat TB-hoz hozzáadásakor az eredeti TB mondatok továbbra is vonzatai maradnak az új nagyobb TB-nak
		* **ha TB~1~ |= a, akkor (TB~1~ ∪ TB~2~) |= a**
	* statikus világ &rarr; akkor ez nagyon jó
	* dinamikus világ &rarr; valahogy frissíteni kell, hogy továbbra is igazak e a megszerzett tudások

* **Horn klózok**

	* ha ilyen formájú: **P~1~ ∧ P~2~ ∧ P~3~ ∧ ... P~n~&rarr; Q** vagy **¬P~1~ ∨ ¬P~2~ ∨ ¬P~3~ ∨ ... ¬P~n~ ∨ Q**
	* ekkor <u>létezik a TB méretében **lineáris idejű** következtetési eljárás</u>
	* nem minden TB-beli állítás írható fel így, de ami igen &rarr; Modus Ponens-t tudunk alkalmazni
		* Modus Ponens **teljes** bizonyítási lépés a **Horn klózok** tudásbázisában

* **előrecsatolt következtetés**

	* minden olyan szabályt elsütünk, aminek premisszája teljesül, a következményt a TB-hez adjuk, amíg a kérdéses változó értéket nem kap

* **hátracsatolt következtetés**

	* kérdéses változótól visszafele lépkedünk, amíg nem találunk olyat amik igazak, és onnan mehetünk felfele.

* **Wumpus következtetés**

	* TODO feladta házinak a jófej bácsi :) 36. diától

