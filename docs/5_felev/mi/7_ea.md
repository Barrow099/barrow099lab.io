# Mesterséges intelligencia - 7. előadás</br>Bizonytalanság kezelése

<a class="md-button" style="text-align: center" href="../7_ea.pdf" download>Letöltés PDF-ként</a>

## Bizonytalan tudás

### Bizonytalan tudás lehetséges okai

* "lazaság, lustaság": a részletes kapcsolatok megfogalmazása túl nehéz, a használatuk szintén nehézkes
* **elméleti ismeretek hiánya**: adott problématerület elméleti feltárása még nem zárult le, vagy sose lehet lezárni
* **gyakorlati ismeretek hiánya**: nem minden, a szabályokban hivatkozott feltétel ismert a szabályok alkalmazásakor

### Valószínűségi axiómák

* 0 &le; P(A) &le; 1
* P(Igaz) = 1, P(Hamis) = 0
* P(A &or; B) = P(A) + P(B) - P(A &and; B)
* P(&not;A) = 1 - P(A)
* P(A) = P(A &and; B) + P(A &and; &not;B)

### Valószínűségi állítások

* bináris &rarr; P(Lyuk = Igaz)
* többértékű &rarr; P(Időjárás = Esős)
* folytonos &rarr; P(Hőmérséklet < 22 °C)

### Pár szabály, átalakítás

* **Feltételes valószínűség** &rarr; P(A | B) = P(A B) / P(B)
* **Lánc szabály** &rarr; együttes valószínűség felírható feltételes valószínűségek szorzataként
* **Bayes-tétel**
	* <img src="/src/mi/image-20201025130731889.png" alt="image-20201025130731889" style="zoom:40%;" />
	* miért fontos?
		* sokszor rendelkezünk kauzális (ok-okozati) tudással
		* pl1: mi az adott tünet valószínűsége egy adott betegség függvényében
		* pl2: mi a valószínűsége a riasztásnak tűz esetén (adott riasztónál pl)
	* <u>sokszor kell az evidencia (következmény) felől az ok-ra következtetni</u>
		* pl1: tünetet látom, lehetett e adott betegség a kiváltó ok
		* pl2: riasztás van, lehetett e tűzeset miatt
	* **okság irány**: könnyebb, betegségről tünetre következtetni
	* **diagnosztikai irány**: nehezebb, tünetről betegségre következtetni
	* <img src="/src/mi/image-20201025131257606.png" alt="image-20201025131257606" style="zoom:30%;" />
	* <img src="/src/mi/image-20201025131401502.png" alt="image-20201025131401502" style="zoom:40%;" />
	* Bayes-tétel műveletek:
		* <img src="/src/mi/image-20201025135921230.png" alt="image-20201025135921230" style="zoom:43%;" />
		* <img src="/src/mi/image-20201025140032032.png" alt="image-20201025140032032" style="zoom:43%;" />

### Valószínűség értelmezése

* **frekventista nézőpont**: a valószínűség objektív, események gyakoriságából számítható
	* **valószínűség (frekventista definíciója)**: A esemény valószínűsége az a számérték, amely körül az esemény relatív gyakorisága (f~A~) ingadozik, ha egyre több kísérletet végzünk
	* P(A) = lim <sub>n&rarr;&infin;</sub> f~A~
* **bayesi nézőpont**: a valószínűség egy eseménybe vetett hiedelem mértéke
	* **valószínűség (bayesi definíciója)**: A esemény valószínűsége az adott esemény bekövetkezési esélye
	* a jelenlegi helyzettől megkülönböztethetetlen esetek mekkora hányadában fog bekövetkezni az adott esemény
	* Priori valószínűségekből indulunk ki, új evidencia érkezésekor azokat frissítjük

### Együttes valószínűség-eloszlás

* P(X~1~, ..., X~n~) &rarr; minden egyes elemi eseményhez valószínűséget rendel
* táblázat &rarr; cellája az adott állapot valószínűsége, pl P(időjárás, hőmérséklet) &rarr; bazinagy táblázat lesz
	* elemi események, mert nem lehet egyszerre felhős-meleg és napos-hideg
	* táblázat elemeinek összege 1
* **marginális elosztások** &rarr; egy változóra nézett vetület &rarr; adott értékbek tartozó sorban/oszlopban levő értékek összege
* pozitívum: együttes eloszlás birtokában &rarr; mindenre választ kapunk
* negatívum: nem megy 10-nél több változóra &rarr; P(X~1~, ..., X~n~) esetén &rarr; 2^n^-1 független valószínűségérték
	* egy diagnózishoz &rarr; exponenciális számú valószínűség kellene :(
	* ha 1 is megváltozik, akkor csomót újra kell számolni! :(

### Bayesi frissítés

* egyesével gyűjtjük a tényeket, majd módosítjuk az ismeretlen változóval kapcsolatos korábbi hiedelmi mértéket
* TODO itt hirtelen vége a felvételnek :O





