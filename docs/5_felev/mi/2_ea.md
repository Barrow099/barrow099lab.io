# Mesterséges intelligencia - 2. előadás <br>Problémamegoldás kereséssel

<a class="md-button" style="text-align: center" href="../2_ea.pdf" download>Letöltés PDF-ként</a>

## Ismétlés

### Ágens

* **szenzorokon** keresztül <u>érzékeli a környezetet</u>
* **beavatkozó szerveken** keresztül <u>cselekszik</u>, azaz befolyásolja a környezetét

### Racionális ágens

* úgy választ a lehetséges cselekvések közül, hogy általuk <u>maximalizálja</u> a **várható hasznosságot**
	* *egyszerű eset* &rarr; rendelkezik céllal, ismeri a költségeket
	* *komplex eset* &rarr; hasznossági függvénnyel rendelkezik, várható jutalmakat ismeri (cél a jutalom maximalizálása)

### Környezet

* fontos figyelembe venni az ágens tervezésének szempontjából
* **teljesen** / **részlegesen megfigyelhető** &rarr; *memória* kell a belső állapot nyilvántartására
* **diszkrét** / **folytonos** &rarr; folytonosnál nem lehet az összes állapotot megkülönböztetni
* **sztochasztikus** / **determinisztikus** &rarr; több lehetséges *forgatókönyvvel* is kell dolgozni sztochasztikus esetben
* **egyedüli ágens** / **több ágens** &rarr; lehet, hogy *véletlenszerűen* kell viselkednie

### Reflex ágens

* aktuális érzékelés vagy memória alapján választ cselekvést
* környezetet nyilvántarthatja memóriában (belső modell a világról)
* <u>nem veszi figyelembe a cselekvések jövőbeli következményeit</u>

### Tervkészítő ágens

* döntéshozatalnál a lehetséges következményekkel is számol
* belső modell a világról (hogy hogyan változik a cselekvése hatására)
* kell neki egy <u>cél</u>, ami tesztelhető **célfüggvénnyel**

## Keresési problémák

* keresési probléma elemei:
	1. **állapottér**
	2. **állapotátmenet-függvény**, formája: (cselekvése, költségek)
	3. **kiindulási állapot**
	4. **célállapot teszt**
* <u>megoldás</u> &rarr; cselekvések egy sorozata (terv), kiindulási állapotból célállapotba juttat
* Példa - Útvonaltervezés
	1. **állapottér** &rarr;  városok
	2. **állapotátmenet-függvény** &rarr; utak, költség a távolság
	3. kiindulási állapot &rarr; pl Arad
	4. **célállapot tesztelés** &rarr; állapot == Bukarest?
* állapottér nagysága &rarr; könnyen <u>túl nagy lehet</u>
	* elég csak a nagy valószínűséggel bekövetkező lehetőségeket számba venni

#### Keresési eljárás tulajdonságai

* **Teljesség** (*completeness*) &rarr; ha <u>van</u> megoldás, <u>biztosan</u> megtalálja
* **Időigény** (*time complexity*) &rarr; mennyi idő a megoldás megtalálása
* **Tárigény** (*space complexity*) &rarr; mennyi tárhely kell
* **Optimalitás** (*optimality*) &rarr; ha több megoldás van, megtaláltuk e a <u>legjobbat</u>?

## Állapottér reprezentáció

### Állapottérgráf

* csomópontok a világ állapotának egy egy absztrakt reprezentációi
* élek &rarr; állapot átmenetek
* célteszt &rarr; célállapot elérésének vizsgálata
* minden állapot 1x fordulhat elő
* probléma: nagyon nagy lesz, teljes egészében <u>nem lesz felépíthető a memóriában</u>

### Keresési fa

* start: gyökér csomópont
* levelek: lehetséges jövők
* csomópontok &rarr; olyan állapot, amit tartalmaz egy adott terv
* probléma: teljes egészében <u>nem lesz felépíthető a memóriában</u>

### Állapottérgráf vs keresési fa

* keresési fa 1 csomópontja = egy útvonal az állapottérgráfban
* kört tartalmazó állapottérgráfhoz &rarr; végtelen fa kellene!

## Keresés keresési fával (általánosan)

* perem = potenciális terv folytatások
* <img src="/src/mi/image-20200916230329294.png" alt="image-20200916230329294" style="zoom:54%;" />
* a különböző algoritmusok a **stratégiában** fognak különbözni, amivel a következő vizsgált perem pont lesz kijelölve

## Keresési stratégiák

* **nem informált keresések** (***gyenge***, ***vak*** keresés)
	* <u>tudjuk</u>: hogyan néz ki a célállapot
	* <u>nem tudjuk</u>: merre a cél, milyen költségű oda a legrövidebb út
* **informált keresések** (***heurisztikus keresések***)
	* <u>tudjuk</u>: hogyan néz ki a célállapot
	* <u>jó becslésünk van rá:</u> merre lehet a célállapot, milyen költségű a célállapotba vezető út

### Mélységi keresés (DFS)

* stratégia: **legmélyebb csomópont kifejtése** (legbaloldalibb ág a fában)
* megvalósítás: a perem egy LIFO stack
* <u>legrosszabb eset</u> &rarr; teljes fa feldolgozása
* **elágazási faktor** = ***b*** (egy szinten hány gyerek van)
* **maximum mélység** = ***m***
* jellemzése:
	1. **Teljesség** &rarr; bal oldallel kezd, teljes fát is feldolgozhatja
		* feltétel: ***m*** legyen véges &rarr; **O(b^m^)** időt vesz igénybe
	2. **Időkomplexitás**
	3. **Tárkomplexitás** &rarr; csak a gyökércsomóponthoz vezető úton levő elágazások számítanak &rarr; O(bm) :)
	4. **Optimalitás** &rarr; nem, <u>legbaloldalibb megoldást</u> találja meg, költségtől és mélységtől függetlenül :(
* baj: ha jobb oldalt van a megoldás

### Szélességi keresés (BFS)

* strarégia: a **legkevésbé mélyen levő csomópont kifejtése** (szintenként)
* megvalósítás: a perem egy FIFO sor
* **jelenlegi mélység** = ***s***
* jellemzése:
	1. **Teljesség** &rarr; legfelső szinttel kezd, teljes fát is feldolgozhatja (***s*** véges, ha létezik megoldás)
		* feltétel: **O(b^s^)** időt vesz igénybe
	2. **Időkomplexitás**
	3. **Tárkomplexitás** &rarr; kb a legutolsó szinttel arányos: O(b^s^) :(
	4. **Optimalitás** &rarr; igen, ha <u>minden egységesen 1 költségű</u> :)
* baj: ha mélyen van a megoldás

### Mélységkorlátozott keresés

* utak maximális mélységére (m) vágási korlátot ad
	* ha ezt eléri a keresés, akkor visszalép
* **Teljesség** &rarr; ha a vágás felett van a megoldás (ha nem, akkor gáz van)
* **Optimalitás** &rarr; nem optimális (nem garantálja a legkisebb költségű utat)
* baj: honnan tudjuk a korlátot?

### Iteratívan mélyülő keresés

* ötlet: jó ez a mélységkorlátozós dolog, de nehéz belőni a jó korlátot
* megoldás: <u>kipróbáljuk az összes lehetséges mélységkorlátot</u>
* baj: redundáns lesz, olyat kell feldolgozni amit már egyszer feldolgoztunk
* **Optimális** és **teljes** &rarr; mint a szélességi keresésé
* **Tárhelykomplexitása** alacsony &rarr; mint a mélységi keresésé
* kifejtések száma, ha **d** mélységben **b** elágazási tényező mellett
	* szélességi keresésnél &rarr; 1 + b + b^2^ + ... + b^d-2^ + b^d-1^ + b^d^ 
	* iteratívan mélyülő keresésnél &rarr; (d+1)*1 + (d)*b + (d-1)b^2^ + ... 3b^d-2^ + 2b^d-1^ + 1b^d^ 
		* mivel a fa felsőbb részeit többször kifejti
	* minél <u>nagyobb az elágazási tényező</u>, annál <u>kisebb</u> lesz a <u>többletmunka</u>

## Költségérzékeny keresés

### Egyenletes költségű keresés (UCS)

* stratégia: **legkisebb költségű csomópont kifejtése**
* módszer: perem egy <u>prioritásos sor</u> lesz, ahol a prioritás a **kumulált költségnek** felel meg
* <img src="/src/mi/image-20200917095235198.png" alt="image-20200917095235198" style="zoom:50%;" />
* elv: kifejti az összes olyan csomópontot, ami <u>kevesebb költséggel elérhető</u>, mint a legkisebb költségű megoldás
* ***C**** = megoldás költsége, ***ε*** = élek legalább ε költségűek (**minimum élköltség**)
* **effektív mélység** = ***C*/ε***
* Teljesség &rarr; igen, ha a legjobb megoldás <u>véges költségű</u> és a <u>minimum élköltség pozitív</u>!
* Optimális &rarr; IGEN :)
* Tárhelykomplexitás &rarr; kb az utolsó szinttel arányos **O(b^C*/ε^)**
* Időkomplexitás &rarr; **O(b^C*/ε^)**
* baj: minden irányba feltár, fogalma sincs a célállapot helyzetéről

### Eddigi algoritmusok közös tulajdonságia

* Csak a perem tárolásának módjában különböznek
	* mind valamilyen **prioritásos sort** használnak
* mélységi &rarr; verem
* szélességi &rarr; sor

### Kétirányú keresés

* egyszerre indítunk keresést <u>előre a kiinduló</u> állapotból és <u>hátra a célállapotból</u>
* keresés véget ér, ha találkozik a két keresés
	* O(2 * b^d/2^) = **O(b^d/2^)**
* implementálása nem triviális
	* hogy keresünk hátrafele? &rarr; több célállapot esetén nehéz

## Eddigi keresések összefoglalója

<img src="/src/mi/image-20200918091451726.png" alt="image-20200918091451726" style="zoom:45%;" />

## Informált keresés

### Heurisztika, h(n)

* ötlet: büntessük, ha a céltól távolodunk = jutalmazzuk, ha közeledünk
* egy függvény, ami **becslést ad** arra, hogy az **adott állapot mennyire van közel a célállapothoz**
* mindig az adott keresési problémához tervezett (pl: Manhattan távolság, euklidészi távolság)
* minden n állapotra ki kell számítani
* ha teljesen pontos lenne, akkor felesleges (egyértelmű a megoldás)
* teljes útköltség = eddig megtett út költsége + hátralevő út költsége &rarr; f(n) = g(n) + h(n)

### Mohó keresés

* stratégia: **fejtsük ki azt a csomópontot, amiről azt gondoljuk, hogy a legközelebb van a célállapothoz**
	* heurisztika: a legközelebbi célállapottól való becsült távolság
* probléma: nem biztos, hogy az aktuálisan legjobb állapot lesz a legjobb globálisan
* legrosszabb esetben egy rosszul irányított mélységi keresést kapunk
* Teljesség &rarr; nem teljes (elindul végtelen úton, nem tér vissza mást kipróbálni)
* Optimális &rarr; nem optimális
* Idő és Tárigény &rarr; nagyon rossz, az összes csomópontot a memóriában tartja &rarr; **O(b^m^)**

### A* keresés

* ötlet: egyenletes költségű és mohó keresést kombináljuk
	* egyenletes költségű &rarr; g(n) minimalizálása a célja
	* mohó &rarr; h(n) minimalizálása a célja
	* <img src="/src/mi/image-20200918094811839.png" alt="image-20200918094811839" style="zoom:40%;" />
* vegyük figyelembe az eddig megtett út költségét és a hátralevő út költségét is &rarr; **f(n) = g(n) + h(n)**
* csak akkor <u>állhatunk meg</u>, ha már <u>kifejtésre került a célállapot</u> (nem elég, ha a kifejtendők közt van, mert az utolsó út is számít)
* Teljes és Optimális &rarr; igen, ha jó a heurisztika
* A határvonala a cél fele van (az egyenletes költségűé egyenletes minden irányba)

### Elfogadható heurisztika

* **nem elfogadható** (pesszimista) heurisztika megtöri az optimalitást azzal, hogy jó tervek a peremben maradnak
* **elfogadható** (optimista) heurisztika "lelassítja" a rossz terveket, de sosem lép túl a valós költségeken
	* 0 ≤ h(n) ≤ h*(n), ahol h*(n) a valós költség a legközelebbi célállapothoz
* A* kereséshez <u>elengedhetetlen</u> az **elfogadható heurisztika** kialakítása!
* heurisztika készítése &rarr; **probléma relaxálásával** (egyszerűsítésével) &rarr; pl légvonal, manhattan távolság stb

