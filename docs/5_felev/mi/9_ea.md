# Mesterséges intelligencia - 9. előadás</br>Valószínűségi hálók

<a class="md-button" style="text-align: center" href="../9_ea.pdf" download>Letöltés PDF-ként</a>

## Bayes szabály nehézségei

* priori feltételes és együttes valószínűségek begyűjtése nehéz és költséges
* ember rossz valószínűségbecslő
* Bayes-szabály sok számítást igényel

## Valószínűségi hálók

* más néven Bayes-háló &rarr; egy gráf

	* csomópontok &rarr; valószínűségi változók egy halmaza
	* csomópontok között &rarr; irányított élek halmaza (Y &rarr; X = Y-nak közvetlen befolyása van az X-re)
	* minden csomópont &rarr; feltételes valószínűségi tábla &rarr; P(X | Szülők(X))
	* a gráf nem tartalmaz irányított kört &rarr; DAG
	* valószínűségi változó = egy állítás a problémáról

* **feltételes valószínűségi tábla** - **FVT**

	* egyes csomóponti értékek feltételes valószínűsége, az adott sorhoz tartozó <u>szülő feltétel</u> esetén

	* ahol egy <u>szülői feltétel</u> &rarr; szülői csomópontok értékeinek egy lehetséges kombinációja

	* pl Betörés &rarr; Riasztás és Földrengés &rarr; Riasztás, akkor

		| B    | F    | P(R) = P(R\|B &and; F) |
		| ---- | ---- | ---------------------- |
		| i    | i    | 0.95                   |
		| i    | h    | 0.95                   |
		| h    | i    | 0.28                   |
		| h    | h    | 0.001                  |

	* Így &rarr; kevesebb dolgot kell számolni, mint ha az összes együttes valószínűséget néznénk (2^n^-1)

		* HA minden mindennel összefügg &rarr; akkor ez sem segít :(

* Bayes-hálók &rarr; White box modell (belső működése is értelmezhető)

	* a Neurális hálók pl általában nem ilyenek, hanem black box modellek

* együttes valószínűségi eloszlás dekomponált leírása

	* <img src="/src/mi/image-20201026164056957.png" alt="image-20201026164056957" style="zoom:33%;" />

	* P(J M R &not;B &not;F) =
		= P(J | M R &not;B &not;F) * P(M R &not;B &not;F) =

		= **P(J | R)** * P(M | R &not;B &not;F) * P(R &not;B &not;F) =
		= P(J | R) * **P(M | R)** * P(R | &not;B &not;F) * **P(&not;B)** * **P(&not;F)**

* amit tudunk:

	1. minden csomópont <u>feltételesen független a nem leszármazottaitól, ha a **szülői adottak**</u>
	2. minden csomópont <u>feltételesen független minden mástól, ha a **markov-takarója adott**</u>
		* markov-takaró: a szülei, a gyerekei, és a gyerekeinek a szülei.
		* ez elszigeteli a modell többi részétől!

* háló építésének lépései:

	1. határozzuk meg a problémát leíró változókat
	2. határozzunk meg egy sorrendet
	3. amíg marad érintett változó
		1. válasszuk a következő X~i~-t, adjuk a hálóhoz
		2. legyenek X~i~ szülei a már gráfba levő csomópontok azon minimális halmaza, amiktől feltételes függetlenségben áll
		3. definiáljuk X~i~ feltételes valószínűségi tábláját

* pozitívum az együttes valószínűségi eloszlásfüggvényekhez képest

	* tömörebb, lokálisan strukturált (egy komponens csak korlátos másikkal van kapcsolatban)
	* inkább lineáris komplexitás növekedés (mint exponenciális)

### Naív Bayes-hálók

* egy szülőhöz tartozik több gyermek csomópont

