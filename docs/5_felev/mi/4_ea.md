# Mesterséges intelligencia - 4. előadás</br>Keresés ellenséges környezetben

<a class="md-button" style="text-align: center" href="../4_ea.pdf" download>Letöltés PDF-ként</a>

### Játékok típusai

* sokféle létezik
* tulajdonságok:
	* determinisztikus vagy sztochasztikus? (van benne véletlen?)
	* egy, kettő vagy több játékos?
	* zéró összegű játék? (egy nyertes van, ha az egyik játékos jobb helyzetbe kerül a másik rosszabba)
	* teljes információ (láthatjuk az állapotot)? (pl kártyajáték NEM ilyen)
* algoritmus = **stratégiát** ad &rarr; optimális/jó választ javasol minden egyes állapotban
* **determinisztikus játékok** formalizálása:
	* állapotok: S
	* játékosok: P={1..N}
	* cselekvések: A
	* állapotátmenet függvény: SxA &rarr; S
	* célállapot teszt: S &rarr; {igaz, hamis}
	* célállapot hasznosság: SxP &rarr; R
	* megoldás egy játékos számára a **stratégia** ami S &rarr; A
* **zéró összegű játék**
	* ágensek hasznossága ellenkező, ellenséges játékosok tiszta versengéssel
* **nem zéró összegű játék**
	* ágensek hasznossága független
	* kooperáció, közömbösség, versengés mind egyszerre megjelenhetnek
* egy állapot értéke &rarr; az adott állapotból elérhető legjobb kimenetel (hasznosság)
	* nem végállapotok: <img src="/src/mi/image-20201022165134043.png" alt="image-20201022165134043" style="zoom:45%;" />
	* végállapotok: V(s) = ismert

## Minimax

* ágens és ellenfele ellentétes célra törekednek

* egyikük minimalizálni, másikuk maximalizálni akar egy adott értéket &rarr; minimax

* **minimax keresés**

	* keresés állapottér gráfban

	* játékosok felváltva lépnek

	* minden csomópontra **minimax érték** számítása: <u>legnagyobb elérhető hasznosság egy racionális (ügyesen játszó) ellenséggel szemben</u>

	* végállapotok értéke <u>kiszámítva a játék szabályaiból</u>, és ez alapján születik döntés!

	* algoritmus:

		<img src="/src/mi/image-20201022171910376.png" alt="image-20201022171910376" style="zoom:50%;" />

	* milyen hatékony?

		* gyakorlatilag egy kimerítő DFS &rarr; optimális, de nagy esetben nem lesz hozzá erőforrás
		* idő: O(b^m^)
		* tár: O(bm)

	* hogy lehet rajta javítani? &rarr; nyesések

		* <img src="/src/mi/image-20201022170914767.png" alt="image-20201022170914767" style="zoom:30%;" />
		* ne fejtsük ki azt az ágat, amit biztos nem fog választani (a min ág tuti 2-t rak oda, vagy kisebbet, így a max ágnak nem fogja megérni azt választani)

* **alfa-béta nyesés**

	* ötlet: feleslegesen kiértékelt ágak lenyesése, a játékosok ezeket biztosan nem választják, valamint gyerek csomóópontok sorrendezése ami növeli a hatékonyságot
	* éppen az *n* csomópont MIN-ÉRTÉKét számítjuk ki, végig lépkedünk *n* gyerekein
	* *n* gyerekeinek minimum értéke egyre csökken(het), mert MIN erre törekszik
	* legyen *m* és *m'* két másik már megvizsgált csomópont, amelyeket MAX választhat
	* ha *n* rosszabbá válik mint *m* vagy *m'*, akkor MAX el fogja kerülni *n*-t, így nem kell megvizsgálni *n* további gyerekeit
	* algoritmus:
		<img src="/src/mi/image-20201022171942176.png" alt="image-20201022171942176" style="zoom:50%;" />
	* hatás:
		* nyesés nem befolyásolja a gyökér csomópont minmax értékét!
		* köztes csomópontok értéke eltérhet a valóságtól
		* gyerek csomópontok jó sorrendezése növeli a nyesés hatékonyságát
		* tökéletes sorrendezéssel:
			* idő: O(b^m^)-ről O(b^m/2^), mert az elágazási tényező b = gyök b-re csökkent
			* így dupla keresési mélység valósítható meg!
	* példa:
	* <img src="/src/mi/image-20201022180230909.png" alt="image-20201022180230909" style="zoom:28%;" />

## Erőforrások limitációja

* probléma: keresés nem tud eljutni a levélcsomópontokig!
* megoldás: **mélységkorlátozott keresés**
	* végállapotok hasznossága helyett egy kiértékelő függvény általi becslést fogunk adni
* probléma: optimális játék nem garantált
* ha bármikor le kell tudni állni &rarr; **iteratívan mélyülő keresés** kell
* kiértékelő függvény
	* sakknál &rarr; jegyek súlyozott lineáris kombinációja f~1~(s) = (világos vezérek száma - sötét vezérek száma)
		* ha sok értékes bábunk van még, az többet ér!
	* kiértékelő függvények <u>mindig pontatlanok</u>
	* számít, hogy milyen mélységben vagyunk &rarr; a sakk játék vége fele már érdemes kiszámítani az optimális lépést!

## Bizonytalan kimenetelek

### Legrosszabb eset vs átlagos eset

* ötlet: bizonytalan kimeneteleket a <u>véletlen irányítja</u>, nem egy ellenfél

### Expectimax keresés 

* explicit véletlen &rarr; kockadobás
* véletlenszerűen viselkedő ellenfelek &rarr; PacMan szellemek véletlen mozgása
* cselekvés nem sikerül &rarr; robot mozgásánál megcsúszik a kerék
* fontos: az értékeknek az <u>átlagos kimenetelt</u> kellene tükrözniük (expectimax), <u>nem</u> pedig a <u>lehető legrosszabb</u> (minimax) kimenetelt
* **expectimax keresés**
	* ötlet: számítsuk ki az <u>átlagos pontszámot</u> (hasznosságot) optimális játékot feltételezve
	* max csomópontok &rarr; mint minmax keresésnél
	* min csomópontok &rarr; véletlenszerűek, bizonytalan a kimenetel
	* kérdés: várható érték = <u>gyerekcsomópontok súlyozott átlaga</u>
	* algoritmus:
		<img src="/src/mi/image-20201022183811669.png" alt="image-20201022183811669" style="zoom:50%;" />
	* hasznosságot súlyozzuk a bekövetkezésük valószínűségével!
* itt is <u>lehet nyesni</u>

### valószínűségek

* véletlen változó &rarr; eseményt reprezentél, melynek kimenetele nem ismert
* valószínűségi eloszlás &rarr; súlyok hozzárendelése kimenetelekhez
* valószínűségi axiómák: valószínűségek nem negatívak, összes lehetséges kimenetel valószínűségeinek összege 1
* keresésnél használt <u>valószínűség kiszámítása</u>
	* lehet egyszerű egyenletes eloszlás, vagy bonyolultabb nagy számítási igényű számolás
	* fontos a modellezésnél: ne legyen se túl pesszimista, se túl optimista!

|                   | támadó szellem          | random szellem                                           |
| ----------------- | ----------------------- | -------------------------------------------------------- |
| minmax pacman     | erre lett kitalálva     | ezt is jól kezeli                                        |
| expectimax pacman | **:(** ez nagyon nem jó | ez is jól kezeli, és jobban is teljesít itt a minmax-nál |

## Kevert rétegű játék

* **expectiminimax**
	* környezet egy plusz véletlen ágens, aki a minmax játékos után lép

## Monte Carlo Tree Search

* <u>ismételt véletlen mintavétel</u>, ezen minták gyakorisága alapján közelítjük a nehezen számítható értékeket
* **MCTS**
	* Kiválasztás &rarr; fa mely részét bontsuk ki tovább (már exploration - már feltárttal foglalkozunk, exploitation- újat tárunk fel)
	* Terjeszkedés &rarr; következő csomópont kiválasztása (már bevált ág folytatása VAGY további lehetséges lépés felfedezése)
	* Szimuláció &rarr; véletlenszerű módon leszimuláljuk a játék további részét
	* Visszaterjesztés &rarr; ennek eredményét beírjuk a fába
	* ![image-20201022190022649](/src/mi/image-20201022190022649.png)
	* fontos: kihasználás és feltárás közti arány &rarr; azt kell jól csinálni!
	* <img src="/src/mi/image-20201022190211682.png" alt="image-20201022190211682" style="zoom:25%;" />