# Mesterséges intelligencia - 6. előadás</br>Logikai ágens, elsőrendű logika

<a class="md-button" style="text-align: center" href="../6_ea.pdf" download>Letöltés PDF-ként</a>

## Rezolúció

* Egy Q-ról akarjuk eldönteni, hogy igaz e vagy hamis.
* lépései:
	* TB átírása klóz formába
	* Q kérdés negálása, negált Q kérdés átírása klóz formára
	* kiterjesztés TB' = TB ∪ ¬Q
	* rezolúciós lépés ciklikus elvégzése &rarr; üres rezolvenst kapunk
	* tehát TB' egy ellentmondást tartalmaz, és mivel TB igaz volt biztosan ¬Q miatt romlott el, azaz Q igaz kell, hogy legyen!
* **klóz formára hozás**
	1. ekvivalencia elhagyása: A ⇔ B = (A &rarr; B) &and; (B &rarr; A)
	2. implikáció elhagyása: A &rarr; B = &not;A &or; B
	3. negálás atomi formulák szintjére (de Morgan): &not;(A &or; B) = &not;A &and; &not;B
	4. diszjunkciók literálok szintjére (disztributivitás): (A &and; B) &or; C = (A &or; C) &and; (B &or; C)
	5. konjunkciók elhagyása (diszjunktív klózokra bontás): csak &not; és &or; maradhat, azaz az és-ek helyett külön mondattá szedjük őket
* probléma: túl sok ítéletszimbólumot kell kezelni, nehézkes a változások kezelése &rarr; lassú a következtetési eljárás
	* ahol jó: formális modell-ell, MI tervkészítés, auto tesztminta-generálás, szoftver-verifikáció, stb

## Elsőrendű logika

* **objektumok**: más obj-tól megkülönböztethető identitású, **tulajdonságokkal** rendelkező dolog
* **relációk**: kapcsolatok, lehet n-elemű vagy egyelemű (ezek a tulajdonságok)
* **függvény jellegű relációk**: egy érték egy adott bemenetre (pl: apja, egyel több mint)
* szintaktika és szemantika
	* **konstans szimbólumok**: X, János, K21
	* **predikátum szimbólumok**: kerek(X), bátyja(József, Anna)
	* **függvény szimbólumok**: János = apja(Béla), apja(János, Béla) = Igaz
* építőelemek:
	* **term**: egy objektumre vonatkozó kifejezés (János, bal-lába(apja(János)), ...)
	* **atom**: bátyja(Géza, Árpád)
	* **atomi mondat**: predikátum szimbólum és az argumentumait jelentő termek **tényeket fejez ki** (igazságértéke van)
	* **összetett mondat**: bátyja(Misi, János) &rarr; házas(apja(Misi), anyja(János))
	* **kvantorok**:
		* **univerzális kvantor**: ∀ (&forall;x. macska(x) &rarr; emlés(x))
		* **egzisztenciális kvantor**: ∃ (&exist;x. magyka(x) &and; úszik(x))
	* egyenlősség: (term1 = term2) adott implementáció mellett és akkor és csak akkor, ha term1 és term2 ugyanarra az objektumra vonatkozik
	* &forall; és &exist; kapcsolata:
		* &forall;x &not;szeret(x, Répa) = &not;&exist;x szeret(x, Répa)
		* &forall;x szeret(x, Fagylalt) = &not;&exist;x &not;szeret(x, Fagylalt)
	* fontos: mivel mi adjuk a konstansok, predikátumok és függvényszimbólumok neveit &rarr; fontos, hogy ha kettő kettő ugyan azt jelenti, azt logikailag is leírjuk! (ne csak mi tudjuk, hogy a nagytestű és dögnagy ugyanaz!) 

#### Modell alapú következtetés?

* probléma:

	* összes modell felsorolása nagyon sok, DE megszámlálható
	* függvények egymásba ágyazhatók végtelen mélységben! (apja-fia végtelen ismétlése)
		* megoldás1: <u>redukció ítéletlogikai következtetésre</u>
		* megoldás2: <u>következtetési lépések kiterjesztése</u>

* **redukció ítéletlogikai következtetésre** &rarr; grounding

	* változó nélküli term &rarr; **alapterm (grounding)**
	* változó nélküli predikátum kalkulus &rarr; **ítélet kalkulus**
	* új TB = minden univerzális mondat minden lehetséges példányosítása
		* egy alapmondat vonzata az új TB-nek, ha vonzata volt az eredeti TB-nek is
		* függvény = végtelen sok alapterm &rarr; határt kell húzni nekik
	* **teljes**: <u>minden igaz állítás belátható</u>
	* **vonzat csak félig eldönthető**: <u>állítás hamis volta nem mutatható ki</u>
		* bizonyítási eljárásnak <u>igaz állítás</u> esetén **van kilépési pontja**
		* <u>hamis állítás</u> esetén **nincs**, közben **nem dönthető el**
		* AZAZ gépen <u>véges erőforrások miatt</u> elvben nem mutatható ki egy állításhalmaz konzisztenciája

* **következtetési lépések kiterjesztése**

	* unizerzális kvantor eliminálása

		* <img src="/src/mi/image-20201025114116986.png" alt="image-20201025114116986" style="zoom:35%;" />
		* illesztés = unifikálás (mit mivel ) + behelyettesítés (konkrét érték)

	* egzisztenciális kvantor elminimálása &rarr; skolemizálás

		* <img src="/src/mi/image-20201025114601535.png" alt="image-20201025114601535" style="zoom:35%;" />

		* B~S~ = Skolem konstans, a <u>feladatban önálló léttel NEM rendelkező objektum</u>

			* lehet függvény is! (ami minden x-hez egy külön skolem konstanst rendel hozzá)

		* <img src="/src/mi/image-20201025114620929.png" alt="image-20201025114620929" style="zoom:35%;" />

			* szükség van az f~SZÍV~ Skolem függvényre, ami megadja az adott ember szívét

		* egyesítés megy, ha:

			| x          | y          | eredmény |
			| ---------- | ---------- | -------- |
			| változó-1  | változó-2  | ✔        |
			| változó    | konstans   | ✔        |
			| konstans   | változó    | ✔        |
			| konstans-1 | konstans-2 | ✖        |
			| változó    | f(változó) | ✖        |

			

* gépi bizonyítás

	* Modus Ponens alapú biz. &rarr; NEM teljes
	* elsőrendű logikai biz. &rarr; teljes
	* algoritmizált "hogyan" &rarr; **rezolúció** &rarr; DE a félig eldönthetőség megmarad
	* **transzformáció klóz formára**
		1. ekvivalencia elhagyása: A ⇔ B = (A &rarr; B) &and; (B &rarr; A)
		2. implikáció elhagyása: A &rarr; B = &not;A &or; B
		3. negálás atomi formulák szintjére (de Morgan): &not;(A &or; B) = &not;A &and; &not;B, **&not;&forall;x p(x) = &exist;x &not;p(x)**
		4. **Egzisztenciális kvantorokat eltüntetni: Skolemizálás**
		5. **Ha szükséges, a változókat átnevezni**: **&forall;x p(x) &or; &forall;x q(x) helyett &forall;x p(x) &or; &forall;y q(y)**
		6. **Univerzális kvantorokat balra kihejezni: ...&forall;x...&forall;y... = &forall;x&forall;y...x...y**
		7. diszjunkciók literálok szintjére (disztributivitás): (A &and; B) &or; C = (A &or; C) &and; (B &or; C)
		8. konjunkciók elhagyása (diszjunktív klózokra bontás): csak &not; és &or; maradhat, plusz a kvantorok
		9. **ha szükséges, a változókat átnevezni**
		10. **univerzális kvantorokat elhagyni &rarr; csak &not; és &or; maradhat, azaz az és-ek helyett külön mondattá szedjük őket**
	* Rezolúció bizonyítás procedúrája &rarr; adott állítás halmaza F, a bizonyítandó állítás Q
		1. Az F halmaz összes állítását <u>konvertáljuk klóz formába</u> &rarr; F'
		2. <u>Negáljuk a Q-t</u>, és <u>konvertáljuk klóz formába</u>, adjuk hozzá F'-hez
		3. Ismételjük a ciklust (3-4-5 pont):
			1. ellentmondásra rá nem futunk
			2. előrehaladást nem tapasztaljuk
			3. az erőforrások előre meghatározott mennyiségét ki nem használjuk
		4. Válasszunk meg két klózt és alkalmazzunk rezolúciós lépést
		5. Ha a <u>rezolvens üres</u>, megvan az ellentmondás. Ha nem, adjuk hozzá a többi klózhoz és folytatjuk a 3. pontot.
	* Rezolúciós stratégiák = <u>klózok kiválasztási heurisztikái</u>
		1. Egység rezolúció &rarr; <u>Horn-klóz alakú TB-ban az eljárás teljes</u>, különben nem!
		2. Set of Support: 
		3. MI A LÓFASZ EZ A 31. DIA? KONKRÉTAN EGY SZÓT NEM MOND EL BELŐLE ÉRTELMESEN... 1:01:00 előtt FUCKING TODO
	* logikai prorgamozás - Prolog
		* **nem teljes** &rarr; időkorlát van!

* **Ontológia**

	* cél: a tudás megosztása (érthetően). Adott tárgyterület részletes leírását valósítja meg.
	* összetevők: fogalmak (osztály), tulajdonságok és relációk a fogalmak közt (taxonómia (isA, partOf), reláció, attribútum, szerep)
	* **tárgyterület részletes leírása**:
		* megkötések &rarr; típus megkötés, számosság (1 alatti), értéktartomány (0 és 50 közti), X nagyobb mint Y, X és Y különböző
		* konkrét érték megadása
	* ontológiák típusai
		* szemantika szerinti csoportosítás
			* taxonomy - relációs modell szerinti
			* thesaurus - ER
			* conceptual model - RDF/S, XTM, UML
			* logical theory - OWL, ...
			* Elsőrendű logika
	* Ontológia vs Relációs séma
		* ontológia &rarr; formálisan definiált kapcsolatok az entitáok közt, ember és gép is érti
		* relációs séma &rarr; implicit kapcsolatok az entitások között, interpretáció szűkséges, adatbázis szemantikájának ismerete nélkül nem használható

* festő ágens problémája

	* amiket nem kezelünk: idő múlása, ágens cselekvéseinek leírása, környezet változása
	* idő bevezetése &rarr; **szituáció kalkulus**
		* idő múlása = szituációk sorozata
		* egy szituációban 1 tény igaz vagy hamis, de a következő szituációban lehet más
		* fluent = folyékony esemény
		* **hatás axiómák**: egy adott cselekvésnek mi lesz az eredménye
		* **keret axiómák**: a környezet miatt mi marad változatlan

* TODO itt még volt valami a diasorban, de arról nem beszélt (?)

