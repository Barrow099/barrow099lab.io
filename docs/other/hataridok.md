| Időpont         | Esemény                            | Típus |
| --------------- | ---------------------------------- | ----- |
| 2020-09-24, Cs  | IT eszközök labor1                 | lab   |
| 2020-10-8, Cs   | IT eszközök labor2                 | lab   |
| 2020-10-20, K   | Üzleti jog, zh1                    | zh    |
| 2020-10-21, Sze | Mesterséges intelligencia, házi1   | hf    |
| 2020-10-22, Cs  | IT eszközök labor3                 | lab   |
| 2020-10-27, K   | Mesterséges intelligencia, zh1     | zh    |
| 2020-10-28, Sze | Mikro-és makroökonomia, zh         | zh    |
| 2020-11-05, Cs  | IT eszközök labor4                 | lab   |
| 2020-11-06, P   | Mesterséges intelligencia, pót zh1 | pzh   |
| 2020-11-11, Sze | Mikro-és makroökonomia, pót zh     | pzh   |
| 2020-11-17, K   | Mobil és webes szoftverek, zh      | zh    |
| 2020-11-18, Sze | Mesterséges intelligencia, házi2   | hf    |
| 2020-11-19, Cs  | IT eszközök labor5                 | lab   |
| 2020-12-01, K   | Mobil és webes szoftverek, pót zh  | pzh   |
| 2020-12-04, P   | Mesterséges intelligencia, zh2     | zh    |
| 2020-12-08, K   | Üzleti jog, zh2                    | zh    |
| 2020-12-11, P   | IT eszközök technológiája, zh      | zh    |
| 2020-12-15, K   | Üzleti jog, pót zh                 | pzh   |
| 2020-12-16, Sze | Mikro-és makroökonomia, pótpót zh  | pzh   |
| 2020-12-17, Cs  | IT eszközök technológiája, pót zh  | pzh   |
| 2020-12-18, P   | Mesterséges intelligencia, pót zh2 | pzh   |
| 2020-12-18, P   | Mesterséges intelligencia, házi3   | hf    |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |

<script language="javascript">
	onload = function a() {
	var t = window.document.getElementsByClassName("md-typeset__table")[0].firstElementChild;
	var rowCount = t.rows.length;
	for(var i = 1; i < rowCount-1; i++) {
		var monday = new Date();
		var date = t.rows[i].cells[0].innerText.split(',')[0];
		var year = date.split('-')[0];
		var month = date.split('-')[1];
		var day = date.split('-')[2];
		if(year <= monday.getUTCFullYear() && month <= monday.getUTCMonth() + 1 && day <= monday.getUTCDate()) {
			//legyen zöld -> teljesítve
			for(var j = 0; j < 3; j++) {
				t.rows[i].cells[j].bgColor = "lightgreen"
			}
		}
	}
	console.log(t);
	}
</script>

