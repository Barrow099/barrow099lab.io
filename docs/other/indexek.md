??? question Beállítások
	<p>Régi események elrejtése<input onclick="refresh()" id="hide_old"  type="checkbox"></p>
	<p>Laborok elrejtése<input onclick="refresh()"  id="hide_lab" type="checkbox"></p>
	<p>Házik elrejtése<input onclick="refresh()"  id="hide_hf" type="checkbox"></p>
	<p>Zh-k elrejtése<input onclick="refresh()"  id="hide_zh" type="checkbox"></p>
	<p>PótZh-k elrejtése<input onclick="refresh()"  id="hide_pzh" type="checkbox"></p>


| Időpont         | Esemény                            | Típus |
| --------------- | ---------------------------------- | ----- |
| 2020-09-24, Cs  | IT eszközök labor1                 | lab   |
| 2020-10-8, Cs   | IT eszközök labor2                 | lab   |
| 2020-10-20, K   | Üzleti jog, zh1                    | zh    |
| 2020-10-21, Sze | Mesterséges intelligencia, házi1   | hf    |
| 2020-10-22, Cs  | IT eszközök labor3                 | lab   |
| 2020-10-27, K   | Mesterséges intelligencia, zh1     | zh    |
| 2020-10-28, Sze | Mikro-és makroökonomia, zh         | zh    |
| 2020-11-05, Cs  | IT eszközök labor4                 | lab   |
| 2020-11-06, P   | Mesterséges intelligencia, pót zh1 | pzh   |
| 2020-11-11, Sze | Mikro-és makroökonomia, pót zh     | pzh   |
| 2020-11-17, K   | Mobil és webes szoftverek, zh      | zh    |
| 2020-11-18, Sze | Mesterséges intelligencia, házi2   | hf    |
| 2020-11-19, Cs  | IT eszközök labor5                 | lab   |
| 2020-12-01, K   | Mobil és webes szoftverek, pót zh  | pzh   |
| 2020-12-04, P   | Mesterséges intelligencia, zh2     | zh    |
| 2020-12-08, K   | Üzleti jog, zh2                    | zh    |
| 2020-12-11, P   | IT eszközök technológiája, zh      | zh    |
| 2020-12-15, K   | Üzleti jog, pót zh                 | pzh   |
| 2020-12-16, Sze | Mikro-és makroökonomia, pótpót zh  | pzh   |
| 2020-12-17, Cs  | IT eszközök technológiája, pót zh  | pzh   |
| 2020-12-18, P   | Mesterséges intelligencia, pót zh2 | pzh   |
| 2020-12-18, P   | Mesterséges intelligencia, házi3   | hf    |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |
|                 |                                    |       |

<script language="javascript">
	onload = function a() {
	var t = window.document.getElementsByClassName("md-typeset__table")[0].firstElementChild;
	var rowCount = t.rows.length;
	for(var i = 1; i < rowCount-1; i++) {
		var monday = new Date(); //aznap
		//var monday = new Date("2020/12/01"); //teszt
		monday.setHours(6, 0, 0, 0);
		var deadline_threeday = new Date(monday.getTime() + 3 * 24 * 60 * 60 * 1000); //plusz 3 nap
		var deadline_oneweek = new Date(monday.getTime() + 7 * 24 * 60 * 60 * 1000); //plusz 1 hét
		var deadline_twoweek = new Date(monday.getTime() + 14 * 24 * 60 * 60 * 1000); //plusz 2 hét
		var date = t.rows[i].cells[0].innerText.split(',')[0];
		var year = date.split('-')[0];
		var month = date.split('-')[1];
		var day = date.split('-')[2];
		var newDate = new Date(year + "/" + month + "/" + day);
		newDate.setHours(6, 0, 0, 0);
		if(deadline_oneweek < newDate && newDate <= deadline_twoweek) {
			//legyen világos piros -> nagyon közel
			for(var j = 0; j < 3; j++) {
				t.rows[i].cells[j].style.backgroundColor = "rgb(255 46 46 / 35%)";
			}
			console.log("vilagospiros" + "  " + newDate);
		}
		if(deadline_threeday < newDate && newDate <= deadline_oneweek) {
			//legyen közepes piros -> nagyon közel
			for(var j = 0; j < 3; j++) {
				t.rows[i].cells[j].style.backgroundColor = "rgb(255 46 46 / 55%)";
			}
			console.log("kozepespiros" + "  " + newDate);
		}
		if(monday < newDate && newDate <= deadline_threeday) {
			//legyen sötét piros -> közel
			for(var j = 0; j < 3; j++) {
				t.rows[i].cells[j].style.backgroundColor = "rgb(255 46 46 / 69%)";
			}
			console.log("sotetpiros" + "  " + newDate);
		}
		if(monday > newDate) {
			//legyen zöld -> teljesítve
			for(var j = 0; j < 3; j++) {
				t.rows[i].cells[j].style.backgroundColor = "rgb(76 174 79 / 57%)";
			}
			console.log("zold" + "  " + newDate);
		}
		if(!(newDate < monday) && !(monday < newDate)) {
			//legyen sárga -> mai
			for(var j = 0; j < 3; j++) {
				t.rows[i].cells[j].style.backgroundColor = "rgb(230 157 0 / 54%)";
			}
			console.log("sarga" + "  " + newDate);
		}
	}
	}
	
	
	function refresh() {
		var t = window.document.getElementsByClassName("md-typeset__table")[0].firstElementChild;
		var rowCount = t.rows.length;
		for(var i = 1; i < rowCount-1; i++) {
			var monday = new Date(); //aznap
			//var monday = new Date("2020/12/01"); //teszt
			monday.setHours(6, 0, 0, 0);
			var date = t.rows[i].cells[0].innerText.split(',')[0];
			var year = date.split('-')[0];
			var month = date.split('-')[1];
			var day = date.split('-')[2];
			var newDate = new Date(year + "/" + month + "/" + day);
			newDate.setHours(6, 0, 0, 0);
			
			
			if(document.getElementById("hide_old").checked == true) {
				if(newDate < monday) {
					t.rows[i].style.display = "none";
				}
			} else {
				if(newDate < monday) {
					t.rows[i].style.display = "display";
				}
			}
			
			if(document.getElementById("hide_lab").checked == true) {
				if(t.rows[i].cells[2].innerText == "lab") {
					t.rows[i].style.display = "none";
				}
			} else {
				if(newDate < monday) {
					t.rows[i].style.display = "display";
				}
			}
		}
	}
</script>

